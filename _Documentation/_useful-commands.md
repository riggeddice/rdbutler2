# Useful commands

## Docker

docker build -t rd_api .
docker run -v C:/Temp/db/:/app/db -p 5452:80 -d rd_api        //port 80 on app -> port 5452 on VPS
docker ps
docker logs 90c88be0a75c
docker exec -it 90c88be0a75c /bin/sh
docker stop 90c88be0a75c
docker run -it --entrypoint sh webapi

## Graphviz

dot input -Ooutput -Tsvg

digraph D {

  A [shape=diamond]
  B [shape=box]
  C [shape=circle]
  Cat [shape=pentagon color="green" penwidth=5]


  A -> B [style=dashed, color=grey]
  A -> C [color="black:invis:black"]
  A -> Aardvark [penwidth=5, arrowhead=none]
  A -> Cat

  subgraph cluster_0{
    bgcolor="#ffddaad4"
    label="Animal cluster"
    Dog [shape=hexagon style=filled fillcolor="hotpink"]

    Cat -> Dog [label="  friends" dir=both color="red:blue"]
  }
  
}

links:

* https://renenyffenegger.ch/notes/tools/Graphviz/attributes/_color/index
* https://graphviz.org/doc/info/attrs.html
* https://www.graphviz.org/doc/info/lang.html
* https://graphs.grevian.org/ --> https://graphs.grevian.org/reference
* https://www.graphviz.org/doc/info/shapes.html


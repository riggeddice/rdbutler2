# Architecture Design Record
## Descriptor

* Number: 0003
* Title: How should a visualisation of story sequence look like

## Context - Value neutral, describe forces at play

Concerns: Exporter as a whole - subset: visualisation of stories

Look at diagram:

![Visualisation of story chain](mats/2112/211202-visualisation-of-story-chain.png)

This is how, more or less, we want to output it as a graphviz diagram. Eventually we can add some more things:

* rank by year-month (this will be useful)
* ..?

To achieve that, we need to add a new component to StoryMetadata:

* Previous Story

And it might be _useful_ to change "Kolejność kampanijna" to "Poprzednia opowieść" in all the reports to make sure this is explicit. NOT YET.

## Decision - 1 sentence, “We will ....”

We will make a graphviz diagram as close as possible to the above vision.

## Status - Proposed, Accepted, Deprecated, Superseded

Accepted.

## Consequences

New functionality.

## Idea

Feed Motives from ez-public instead of rpg repo. Also, feed dark-agendas in place of Motives **AS** Motives.

### Domain Explanation
#### X.1 - File Loading, Parsing differences

This one is a doozy.

So, we have two repositories. 

* 'rpg/cybermagic' on gitlab, representing dynamic EZ information
* 'ez-public' on gitlab, representing EZ as it is, as world and RPG system

Initially, we were loading only data from 'rpg/cybermagic' and it was sufficient; 'ez-public' didn't really exist. But the situation changed with us trying to actually publish the system. And now we have data duplication.

Like:

* This: `/home/ljzolw/Riggeddice/rpg/cybermagic/opowiesci/motives/body-horror.md` is a motive
* This: `/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/story-creation/motives/body-horror/index.md` is the same motive

Note the difference between the way they are written; Eleventy uses a very different way of pathing than I am used to from gitlab. Also the contents differ:

Gitlab:

```
    # Motive
    ## Metadane

    * Nazwa: AMZ - Oczy Zaczęstwa

    ## Krótki opis

    AMZ posiada dziesiątki dron monitorujących wszystko co się dzieje w Zaczęstwie. I TAI AMZ dyskretnie dba o to, by wszystko na terenie Zaczęstwa działało dobrze.

    ## Opis

    **Ten motyw działa TYLKO w Zaczęstwie i okolicy.** AMZ posiada dziesiątki dron monitorujących wszystko co się dzieje w Zaczęstwie. I TAI AMZ dyskretnie dba o to, by wszystko na terenie Zaczęstwa działało dobrze. Ten motyw skupia się na ...
```

Eleventy:

```
    ---
    title: AMZ - Oczy Zaczęstwa
    layout: layouts/component.njk
    eleventyNavigation:
    key: amz-oczy-zaczestwa
    parent: components
    tags: [story_motive, component]
    description: 'AMZ posiada dziesiątki dron monitorujących wszystko co się dzieje w Zaczęstwie. I TAI AMZ dyskretnie dba o to, by wszystko na terenie Zaczęstwa działało dobrze.'
    ---

    # {{ title }}

    ## Opis

    **Ten motyw działa TYLKO w Zaczęstwie i okolicy.** AMZ posiada dziesiątki dron monitorujących wszystko co się dzieje w Zaczęstwie. I TAI AMZ dyskretnie dba o to, by wszystko na terenie Zaczęstwa działało dobrze. Ten motyw skupia się na...
```

So we have to deal with front matter / files.

#### X.2 - Motives and Agenda Drives

But that is not all.

There are some special Motives which double as Motives and as Agendas for opponents. Say, "the-corruptor" or "the-protector".

* They are MOTIVES, as in, they are used to construct a Story and every Story should have at least one Agenda Drive.
* They are much more exhaustive and they use very different tags and different locations. 

As in, Agenda Drive looks more or less like this:

```
    ---
    title: The Aspirant
    layout: layouts/component.njk
    eleventyNavigation:
    key: the-aspirant
    parent: components
    tags: [agenda_drive, component]
    description: '"Zasługuję na więcej!". The Aspirant uosabia kombatywną merytokrację - najlepszy winien być na szczycie.'
    ---

    # {{ title }}
    ## Krótki opis

    {{ description }}

    ## Opis

    ### 1. Przykładowe warianty

    The Aspirant jest skupiony na osiągnięciu odpowiedniego poziomu kompetencji
```

And they are located in a different path: `/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/story-creation/agenda-drives/the-aspirant/index.md` .

### Solution
#### X.1 - Current state

1. I have a MAJOR duplication. Importer requires me to have every Motive in `rpg/cybermagic`, or it will fail to work (as I wanted it to).
2. Agenda Drives are currently stored as Motives in `rpg/cybermagic`. One Agenda Drive is an hour of work, Motive can be mere 5 minutes.
3. If I create a new Story with a new Motive, I need to add that Motive and move it to `rpg/cybermagic`. It is how it is, it's how it currently works.
4. In the same time, `ez-public` has high-quality list of Agenda Drives. All 5 of them (while we have 20+ as Motives).
5. `ez-public` NEEDS to display Motives. And it requires complete overhaul of some lower quality Motives.
6. AND to top it all, `browser.rd.pl` requires Motives to function; it's one of my favourite and most useful functions.

#### X.2 - Desired state

* I have only one place for Motives.
* I do not have any duplication.
* I have Agenda Drives which are excellent and work.
* Both Motives and Agenda Drives work as Motives in terms of Story construction.
* Both Motives and Agenda Drives are visible on ez-public.
* Neither Motives nor Agenda Drives present low quality records for public use.

#### X.3 - How to get there
##### Overall

I need to deal with two areas:

1. Motives
2. Agenda Drives

##### Variant 1

For Motives (this will be exhausting, but non-destructive work):

1. Rebuild three Motives exhaustively and into excellence from `rpg/cybermagic` to `ez-public`.
2. Create a prompt which will take a "cybermagic Motive" and convert it to "ez-public Motive", while increasing its quality.
3. **Manually** convert **ALL** "cybermagic Motives" to new format.
4. Result -> we have high quality Motives in `ez-public`.

For Agenda Drives:

1. Move "bad" Agenda Drives to Agenda Drives, but display them differently.
2. Result -> we have high quality Agenda Drives in `ez-public` and low quality ones as 'pending'.

Having the above:

1. Importer shall change the path to read Motives from `rpg/cybermagic` to `ez-public`, treating **both** Motives and Agenda Drives as Motives.
2. Result -> we have achieved parity.

Long term plan:

1. Convert bad Agenda Drives into good Agenda Drives.
2. Add new Motives as needed to ez-public.
3. Stop supporting `rpg/cybermagic` in terms of Motives and eventually, delete it.

##### Variant 2

1. Rebuild [1, 3] Motives exhaustively and into excellence from `rpg/cybermagic` to `ez-public`.
2. Delete those Motives from `ez-public`. This will make Importer fail.
3. Parallel work on Motives
    1. Create a prompt which will take a "cybermagic Motive" and convert it to "ez-public Motive", while increasing its quality.
    2. (Ignore Agenda Drives, Ignore Magical Energies)
    3. Slowly move and convert Motives to `ez-public`
4. Parallel work on Importer
    1. Add `ez-public` as repository and give it priority (if the same Motive exists in `ez-public` and `rpg/cybermagic`, ignore `rpg/cybermagic`)
    2. For now, integrate Motives from `ez-public` and `rpg/cybermagic` programistically; Riggeddice.db will accept that. 
5. When only Energies and Agenda Drives will be left, it will be much easier to convert those and only those.
    1. Importer shall treat both Motives and Agenda Drives as Motives.
    2. There will be special Energies-based Motives too.

##### Conclusion

I will go for Variant 2. Easier and less turbulent. Also, I can focus on 'best' Motives, not on 'all' Motives, which will lead to better results irl as well.

### Possible expansion

* Parity achieved, expansion will be simple.

## Idea

Current world map visualisation is very flat. Not much is visible there.

There are several questions about it:

* Should we display it Left-to-Right or like a mind map, with the root at center?
* What should we do to make it more readable

It has been suggested (by T) to do the following:

* Tooltip of graphviz nodes: stories in which LocationRecord was present
* The more the node is present, the more "visible" it is on the map
    * Very Rare: 1-2 times: grey background, grey circumference
    * Rare: 3-5 times: no change, grey circumference
    * Seldom: 6-10 times: no change
    * Visited: 11-20 times: white background, blue circumference, size of circumference: 2
    * Typical: 21-50 times: yellow background, blue circumference, size of circumference: 2
    * Common: 51-100 times: yellow background, orange circumference, size of circumference: 5
    * Hotspot: 101-200 times: yellow background, red circumference, size of circumference: 5
    * Core: 201-500 times: red background, red circumference, size of circumference: 5

Also, one more thing which is necessary:

* Correct the nodes; change the node NAME (not Label) to Path from Name, to deduplicate nodes
    * So for X->Y->Z and A->B->Z we do not generate: (X - Y - Z) and (A - B - Z) which is interpreted as the same Z being linked to Y and B
        * Instead make it: (X - X_Y - X_Y_Z) and (A - A_B - A_B_Z) do they are distinct nodes

Also, think on ATLRs:

* Actor-to-Location Record with dates.

## Result

Seems Graphviz library I am using **CANNOT** function with the background filled, however stupid it sounds. So plan B:

We are using _penwidth_ 5 (to see the, well, pen width of a node) with color schemes: https://graphviz.org/doc/info/colors.html#brewer . 

* Using either 
    * ylorrd6 (yellow -> red), where lower values are yellow, higher are red.
    * spectral6 (red -> yellow -> green -> blue) where we'd start from BLUE as coldest -> RED as hottest.

For now selecting spectral6; this change is easy to make later


## Idea

### Problem

* There needs to exist a useful view on an Actor, one which allows one to see whole history of the character. 
* However, having something like that on every actor sheet would make everything too heavy and unreadable. This can't appear on an actor sheet.

Usecases:

* Actor: 'Izabela Zarantel'. I want to see her story from conception to death and to see what dates I have available for next Story.
* Actor: 'Izabela Zarantel'. I want to know if she had ever visited Pustogor or not and if so, WHEN.
* Actor: 'Izabela Zarantel'. I want to know during which Stories she met Martyn Hiwasser and what happened in those Stories.
* kluczowe zasoby z momentu chronologii? I mean, tak jak my mieliśmy key zasób działo zniszczenia, albo mamy pandorę, albo tą przynętę na kryptę. Żebym mogła sprawdzić, jakimi key zasobami dysponuje w danym momencie chronologii
    * z perspektywy TEJ POSTACI albo FRAKCJI TEJ POSTACI albo ODPOWIEDNIEJ POSTACI
* key npc - czy dostępne? Mam na myśli, informacje o stanie danej postaci w danym okresie czasu. Np. xx.xx.xxxx Martyn Hiwasser - szpital.
    * z perspektywy - określę dla mojej postaci datę i sprawdzę tamtą postać by określić jej stan

### Solution

Therefore create an actorstory-actor-name.md file with those things:

* We want the following information:
    * Actor Name (whom we refer to)
    * Enumerated Threads the Actor is connected to
    * BLOCK OF HISTORY
        * Story uid, Story name
        * Dates
        * Story Summary
        * Actor Deed
        * Actor Progression & Plan
        * Enumerated Actors who were met
    * BLOCK OF DERIVED DATA
        * Actor to Actor Relation Map (AtARM)
        * Subset of World Map the character is connected to with heatmap of location and date of last visit

Do it only for (threshold) amount of Stories per Actor. Like, if an Actor has 5+ Stories, create a actorstory for the Profile

### How to do it

A new High Level Command.

### Possible expansion

* Add World Map Subset to every Actor Profile which is generated
* Add threadstory view (similar to actorstory but per thread)

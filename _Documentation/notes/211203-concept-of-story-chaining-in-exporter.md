This below will render in graphviz what we want to see (in terms of story chaining visualisation, Exporter):

digraph {
    
    
    node [ shape=Mrecord ]

    story1 [label="{211123-odbudowa-wedlug-justyniana|Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać \n do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął \n odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia \n skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, \n by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, \n że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka \n- prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...| 0111-02-26 - 0111-03-07 }"]
    story2 [label="{211102-satarail-pomaga-marysi|Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który\n kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę.\n Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną.\n Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony\n - nie był sobą. Ale polowanie na Torszeckiego czas zacząć.|0111-02-24 - 0111-02-25}"]

    story1 -> story2 [dir="back"]

}

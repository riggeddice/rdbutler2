## Idea

Add Actor Creation Components to Riggeddice Database for AI systems to be able to work with them.

### Domain Explanation

We have Actor Creation Components, as presented in ez-public repo:

* We have Archetypes: "Neuronauta", "Terminus" etc.
* We have Hearts: "Bohater Pozytywny", "Zbieracz Rozgwiazd"
* We have Aux: "Z Bogatej Rodziny", "Uzależniony"

We know we can make a character having parts of 1-2 Archetypes, 1 Heart and 0-2 Aux.

Having access to new AI tools based on LLMs, we can take a Story Concept and ask the AI Tool to generate us a character. We want to have a pure rng character creator and then an AI tool to make it one entity, fitting such a Story Concept.

So we need to have access to those Actor Creation Components. Conclusion: move them to DB.

### Solution

1. Create a ReadActorCreationComponent with fields:

```json
{
    title: "Arystokrata Aurum",
    identifier: "arystokrata-aurum",
    description: HERE COMES TEXT,
    content: WHOLE MEOWING DOCUMENT,
    type: "archetype" || "heart" || "aux",
}
```

2. Persist that in a database as three different tables: ActorCreationArchetypePersisted, ActorCreationHeartPersisted, ActorCreationAuxPersisted

That will allow me to create such a RNG-based generator. 

### Possible expansion

* It will be future-proof 
    * if we decide to go for something more intelligent (tags, modifiers), it's just a matter of adding new fields. 
    * if needed, I will split ReadActorCreationComponent into 3 as well; shouldn't be too hard.

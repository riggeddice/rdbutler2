## Idea

### StoryInfo

StoryInfo is an entity, which contains all the information about the story we need for Exporter / WebApi. So it models old TV magazines:

* https://www.tvguide.com/listings/
* https://www.tvguide.com/tvshows/star-trek-the-next-generation/1000296924/

So we have:

* Primary actors / characters
* Summary / blurb
* Metadata

This means that our StoryInfo should have some stuff appended to the data model. EVENTUALLY I can make a decision of splitting some strings to separate tables, but for now it is good enough to use _string.Join(", ", array)_ and then _stringArray.Split(", ")_ as long as there are tests responsible for it.

### Campaigns

Also, there is no such thing as campaign. We don't operate with _campaigns_, we use _threads_. One Story can be linked to 1..n threads.

Say, 210331-elena-z-rodu-verlen can have threads:

* Elena's past (secondary)
* Verlenland (secondary)
* Arianna, the Hero of Orbiter (primary)

So 1..n relation, not 1:n.

Therefore, we delete:

* campaignSeqNo

We will have to change Campaign to Threads. All in due time :-).


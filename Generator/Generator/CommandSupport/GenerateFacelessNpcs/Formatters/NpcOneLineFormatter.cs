﻿using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.Formatters
{
    public class NpcOneLineFormatter
    {
        public string NpcToMkdn(FacelessNpc npc)
        {
            string[] positiveValues = npc.PositiveValues.Select(s => s.Substring(0, s.IndexOf(':'))).ToArray();
            string[] antiValues = npc.AntiValues.Select(s => s.Substring(0, s.IndexOf(':'))).ToArray();
            string[] drives = npc.PositiveDrives.Select(s => s.Substring(0, s.IndexOf(':'))).ToArray();

            string faceless = $"* {npc.Name} || {npc.Encao} |" +
                $"{string.Join(";;",npc.PersonalityTraits)}| " +
                $"VALS: {string.Join(", ", positiveValues)} >> {string.Join(", ", antiValues)}| " +
                $"DRIVE: {string.Join(", ", drives)}" ;
            return faceless + "\n";
        }

    }
}

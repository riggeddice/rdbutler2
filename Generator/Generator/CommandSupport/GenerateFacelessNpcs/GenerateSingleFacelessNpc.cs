﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.GeneratingStrategies;
using Generator.CommandSupport.GenerateFacelessNpcs.JobGenStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.NameGenStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs
{
    public class GenerateSingleFacelessNpc
    {
        IRng _rng;
        INameGenStrat _nameGenStrat;
        IJobGenStrat _jobGenStrat;
        ISelector _valuesGenStrat;
        ISelector _drivesGenStrat;
        IPersonalityGenStrat _personalityGenStrat;

        public GenerateSingleFacelessNpc(IRng rng, 
            INameGenStrat nameGenStrat,
            IJobGenStrat jobGenStrat,
            ISelector valuesGenStrat,
            ISelector drivesGenStrat,
            IPersonalityGenStrat personalityGenStrat)
        {
            _nameGenStrat = nameGenStrat; 
            _rng = rng;
            _jobGenStrat = jobGenStrat;
            _valuesGenStrat = valuesGenStrat;
            _drivesGenStrat = drivesGenStrat;
            _personalityGenStrat = personalityGenStrat;
        }

        /// <summary>
        /// /// Faceless NPC
        /// has 1 name
        /// has 2-3 jobs, 
        /// has Values (2-3 Schwartz positive and 1-2 Schwartz negative)
        /// has Drives (1-3 positive and 0-1 negative).
        /// has Personality (2-4 active, with 3-5 traits)
        /// </summary>
        public FacelessNpc Execute(
            string[] maleNamesWithFrequencies,
            string[] femaleNamesWithFrequencies,
            string[] possibleJobs,
            string[] possibleDrives,
            string[] possibleValues,
            OceanComponent[] oceansWithAllTraits)
        {
            bool isMale = _rng.YesOrNo();
            string name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(
                _nameGenStrat.SelectName(isMale, maleNamesWithFrequencies, femaleNamesWithFrequencies).ToLower());
            string[] jobsSelected = _jobGenStrat.SelectJobs(2, 3, possibleJobs);
            (string[] positiveValues, string[] antiValues) = _valuesGenStrat.SelectPositiveAndNegative(2, 3, 2, 2, possibleValues);
            (string[] positiveDrives, string[] negativeDrives) = _drivesGenStrat.SelectPositiveAndNegative(2, 3, 1, 2, possibleDrives);
            (string encao, string[] personalityTraits) = _personalityGenStrat.SelectPersonality(1, 2, oceansWithAllTraits);

            return FacelessNpcFactory.CreateSingle(name,
                jobsSelected,
                positiveValues, antiValues,
                positiveDrives, negativeDrives,
                encao, personalityTraits);
        }
    }
}

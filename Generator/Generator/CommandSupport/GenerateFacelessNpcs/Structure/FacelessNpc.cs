﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.Structure
{
    /// <summary>
    /// /// Faceless NPC has, more or less:
    /// has 1 name
    /// has 2-3 jobs, 
    /// has Values (2-3 Schwartz positive and 1-2 Schwartz negative)
    /// has 1-2 Drives
    /// has Personality (2-4 active, with 3+ traits).
    /// </summary>
    public class FacelessNpc
    {
        public FacelessNpc(string name, 
            string[] jobsSelected, 
            string[] positiveValues, string[] antiValues, 
            string[] positiveDrives, string[] negativeDrives, 
            string encao, string[] personalityTraits)
        {
            Name = name;
            JobsSelected = jobsSelected;
            PositiveValues = positiveValues;
            AntiValues = antiValues;
            PositiveDrives = positiveDrives;
            NegativeDrives = negativeDrives;
            Encao = encao;
            PersonalityTraits = personalityTraits;
        }

        public string Name { get; }
        public string[] JobsSelected { get; }
        public string[] PositiveValues { get; }
        public string[] AntiValues { get; }
        public string[] PositiveDrives { get; }
        public string[] NegativeDrives { get; }
        public string Encao { get; }
        public string[] PersonalityTraits { get; }
    }

    public static class FacelessNpcFactory
    {
        public static FacelessNpc CreateSingle(string name, 
            string[] jobsSelected, 
            string[] positiveValues, string[] antiValues, 
            string[] positiveDrives, string[] negativeDrives, 
            string encao, string[] personalityTraits)
        {
            return new FacelessNpc(
                name: name,
                jobsSelected: jobsSelected,
                positiveValues: positiveValues,
                antiValues: antiValues,
                positiveDrives: positiveDrives,
                negativeDrives: negativeDrives,
                encao: encao,
                personalityTraits: personalityTraits);
        }
    }
}

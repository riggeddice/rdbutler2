﻿namespace Generator.CommandSupport.GenerateFacelessNpcs.GeneratingStrategies
{
    public interface ISelector
    {
        (string[] selectedPositive, string[] selectedNegative) SelectPositiveAndNegative(
            int minAmountPositive, int maxAmountPositive, int minAmountNegative, int maxAmountNegative, string[] allPossibleRecords);
    }
}
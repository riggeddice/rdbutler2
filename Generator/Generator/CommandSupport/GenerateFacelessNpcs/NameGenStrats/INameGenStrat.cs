﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.NameGenStrats
{
    public interface INameGenStrat
    {
        string SelectName(bool isMale, string[] maleNamesWithFrequencies, string[] femaleNamesWithFrequencies);
    }
}

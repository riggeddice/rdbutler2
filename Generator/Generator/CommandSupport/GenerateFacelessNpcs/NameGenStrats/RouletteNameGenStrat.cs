﻿using Commons.Tools.Rngs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.NameGenStrats
{
    public class RouletteNameGenStrat : INameGenStrat
    {
        private IRng _rng;

        public RouletteNameGenStrat(IRng rng)
        {
            _rng = rng;
        }

        public string SelectName(bool isMale, string[] maleNamesWithFrequencies, string[] femaleNamesWithFrequencies)
        {
            string[] selectedNamesFreqs = isMale ? maleNamesWithFrequencies : femaleNamesWithFrequencies;

            // We need to split namefreqs to ((string)name, (int)freq)[]
            string[][] namesFreqs = selectedNamesFreqs.Select(s => s.Split(" ")).ToArray();

            // Algorithm is simple: add 100 to all namesFreqs to make sure strange names can appear
            string[] names = namesFreqs.Select(nf => nf[0]).ToArray();
            int[] freqs = namesFreqs.Select(nf => int.Parse(nf[1])).Select(f => f + 100).ToArray();

            int maxPossibleIndex = freqs.Aggregate(0, (acc, x) => acc + x) - 1;
            int selectedNumber = _rng.Next(0, maxPossibleIndex);

            // Roulette
            int rouletteValue = 0;
            string name = string.Empty;
            for (int i = 0; i < names.Length; i++)
            {
                if (rouletteValue + freqs[i] > selectedNumber)
                {
                    name = names[i];
                    break;
                }
                else
                {
                    rouletteValue = rouletteValue + freqs[i];
                }
            }

            return name;
        }
    }
}

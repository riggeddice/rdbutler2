﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.JobGenStrats
{
    public interface IJobGenStrat
    {
        string[] SelectJobs(int minJobs, int maxJobs, string[] possibleJobs);
    }
}

﻿using Commons.Config;
using Commons.Tools;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Generator.Infrastructure
{
    public class GeneratorSource
    {
        AppConfigProvider _provider;

        public GeneratorSource()
        {
            _provider = new AppConfigProvider();
        }

        public (string[], string[]) ProvideBothMaleAndFemaleNamesWithFrequency()
        {
            string file = @"/1805-kategoryzowane-imiona.md";
            string path = _provider.GetPathToGeneratorInputFolder();
            string content = FileOps.ReadFile(path + file).Content;

            string maleNameSection = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Mężczyzna"), content).Trim();

            string femaleNameSection = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Kobieta"), content).Trim();

            string pattern = @"(\w+ \d+)";
            var regex = new Regex(pattern, RegexOptions.Multiline);

            MatchCollection maleMatches = regex.Matches(maleNameSection);
            string[] maleNameFreq = maleMatches.Select(m => m.Groups[1].Value.Trim()).ToArray();

            MatchCollection femaleMatches = regex.Matches(femaleNameSection);
            string[] femaleNameFreq = femaleMatches.Select(m => m.Groups[1].Value.Trim()).ToArray();

            return (maleNameFreq, femaleNameFreq);
        }

        public string[] ProvideDrives()
        {
            string pattern = @"^\* (.+?:.+)$";
            string file = @"/2204-drives.md";
            return ExtractEnumerationFromFile(pattern, file);
        }

        public string[] ProvideValues()
        {
            string pattern = @"^\* (.+?:.+)$";
            string file = @"/2204-wartosci-schwartz.md";
            return ExtractEnumerationFromFile(pattern, file);
        }

        public string[] ProvideJobs()
        {
            string pattern = @"^(\d{6} .+)$";
            string file = @"/1805-klasyfikacja-zawodow.md";
            return ExtractEnumerationFromFile(pattern, file);
        }

        public OceanComponent[] ProvideOceans()
        {
            // file
            string file = @"/2204-osobowosc-ocean.md";
            string path = _provider.GetPathToGeneratorInputFolder();
            string fileContent = FileOps.ReadFile(path + file).Content;

            // ocean names
            string personalitySection = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Personality Values"), fileContent).Trim();

            string[] oceanNames = personalitySection.Split("\n").Select(s => s[2..].Trim()).ToArray();
            OceanComponent[] oceans = oceanNames.Select(o => new OceanComponent(o)).ToArray();

            // traits
            string traitSection = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Traits"), fileContent).Trim();
            foreach (OceanComponent ocean in oceans)
            {
                string relevantTraits = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(3, 3, ocean.Name), traitSection).Trim();

                string highTraits = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(4, 4, "High"), relevantTraits).Trim();
                ocean.HighTraits = ExtractEnumerationUsingPattern(@"^\* (.+)$", highTraits);

                string lowTraits = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(4, 4, "Low"), relevantTraits).Trim();
                ocean.LowTraits = ExtractEnumerationUsingPattern(@"^\* (.+)$", lowTraits);
            }

            return oceans;
        }

        private static string[] ExtractEnumerationUsingPattern(string pattern, string content)
        {
            var regex = new Regex(pattern, RegexOptions.Multiline);
            MatchCollection matches = regex.Matches(content);
            return matches.Select(m => m.Groups[1].Value.Trim()).ToArray();
        }

        private string[] ExtractEnumerationFromFile(string pattern, string file)
        {
            string path = _provider.GetPathToGeneratorInputFolder();
            FileRecord drivesFile = FileOps.ReadFile(path + file);

            return ExtractEnumerationUsingPattern(pattern, drivesFile.Content);
        }

    }
}

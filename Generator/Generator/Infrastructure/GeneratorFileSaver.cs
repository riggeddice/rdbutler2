﻿using Commons.Config;
using Commons.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.Infrastructure
{
    public class GeneratorFileSaver
    {
        public void SaveFacelessNpcCards(string briefFacelessNpcsCard, string oneLineFacelessNpcsCard)
        {
            string directory = new AppConfigProvider().GetPathToGeneralOutputFolder();
            Directory.CreateDirectory(directory);   // if does exist, this line effectively does nothing
            FileOps.SaveFile(briefFacelessNpcsCard, directory + "/" + "generated_200_npcs_brief.md");
            FileOps.SaveFile(oneLineFacelessNpcsCard, directory + "/" + "generated_200_npcs_oneline.md");
        }
    }
}

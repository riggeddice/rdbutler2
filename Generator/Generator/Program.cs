﻿using Generator.Commands;
using Generator.Infrastructure;

var fileSaver = new GeneratorFileSaver();
var source = new GeneratorSource();

new GenerateFacelessNpcs().Execute(source, fileSaver);

Console.WriteLine("Required generation successful.");


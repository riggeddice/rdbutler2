﻿using Generator.CommandSupport.GenerateFacelessNpcs.Formatters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{
    public class BrieflyFormatValues
    {
        [Test]
        public void Displays_values_2_positive_1_negative()
        {
            // Given
            string[] positiveValues = ["Hedonism: pleasure above anything else", "Stimulation: feel alive, feel more"];
            string[] antiValues = ["Self-direction: autonomy, ability"];

            // When
            string actual = new NpcBriefFormatter().FormatValues(positiveValues, antiValues);

            // Then
            string expected = "    * Wartości:\n        * TAK: Hedonism, TAK: Stimulation, NIE: Self-direction\n";

            Assert.That(expected, Is.EqualTo(actual) );
        }
    }
}

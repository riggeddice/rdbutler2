﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd.GeneratePersonality
{
    public class CheckFlatRandom
    {
        [Test]
        public void For_rng_returning_zero_guarantees_specific_sequence_of_ENCAO_with_5L()
        {
            // Given
            OceanComponent[] oceanComponents =
            [
                new("Extraversion", [], []),
                new("Neuroticism", [], []),
                new("Conscientiousness", [], []),
                new("Agreeableness", [], []),
                new("Openness", [], [])
            ];

            // For generator always returning '0', as in, lowest possible value
            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(0);

            // When
            var (low, zero, high) = new LzhGenFlatRandom().GenerateLZH(oceanComponents, rngMock.Object);

            // Then
            Assert.That("Extraversion", Is.EqualTo(low.First().Name));
            Assert.That(5, Is.EqualTo(low.Count));
            Assert.That(0, Is.EqualTo(zero.Count));
            Assert.That(0, Is.EqualTo(high.Count));
        }
    }
}

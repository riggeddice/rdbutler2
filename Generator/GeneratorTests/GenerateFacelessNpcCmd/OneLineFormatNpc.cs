﻿using Generator.CommandSupport.GenerateFacelessNpcs.Formatters;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{
    public class OneLineFormatNpc
    {
        [Test]
        public void Converts_faceless_to_single_line_string_as_list_record()
        {
            // Given
            FacelessNpc faceless = new FacelessNpc(
                name: "Lena",
                jobsSelected: ["job1", "job2"],
                positiveValues: ["Self-direction: autonomy, ability to control your own way and direction", "Hedonism: pleasure above anything else"],
                antiValues: ["Power: more influence upon reality", "Benevolence: altruism, help others"],
                positiveDrives: ["Duma i pycha: pokazać swoją wyższość", "Uniesienia: szuka długotrwałej miłości"],
                negativeDrives: ["Szok i oburzenie: kontrkultura. Wzbudzać obrzydzenie", "Wanderlust: dowiedzieć się, co jest za TĄ GÓRĄ"],
                encao: "ENCAO:  +-00+",
                personalityTraits: ["Radosny, cieszy się sam i daje radość", "Bezbarwny, przezroczysty", "Popularny, rozpoznawany i szeroko znany"
                ]
            );

            // When
            string actual = new NpcOneLineFormatter().NpcToMkdn(faceless);

            // Then
            string expected = "* Lena || ENCAO:  +-00+ |Radosny, cieszy się sam i daje radość;;Bezbarwny, przezroczysty;;Popularny, rozpoznawany i szeroko znany| VALS: Self-direction, Hedonism >> Power, Benevolence| DRIVE: Duma i pycha, Uniesienia\n";

            Assert.That(expected, Is.EqualTo(actual) );
        }
    }
}

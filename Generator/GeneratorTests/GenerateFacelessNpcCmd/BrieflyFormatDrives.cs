﻿using Generator.CommandSupport.GenerateFacelessNpcs.Formatters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{
    public class BrieflyFormatDrives
    {
        [Test]
        public void Displays_drives_2_positive_1_negative()
        {
            // Given
            string[] positiveDrives = ["needs meat", "loves combat"];
            string[] negativeDrives = ["wants training"];

            // When
            string actual = new NpcBriefFormatter().FormatDrives(positiveDrives, negativeDrives);

            // Then
            string expected = "    * Silnik:\n        * TAK: needs meat\n        * TAK: loves combat\n        * NIE: wants training\n";

            Assert.That(expected, Is.EqualTo(actual) );
        }
    }
}

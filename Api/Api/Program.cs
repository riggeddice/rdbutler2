using Api.Endpoints;
using Commons.Infrastructure.Repos;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options => {
    options.AddPolicy("AllowSpecificOrigin", x => x
        //.WithOrigins("http://localhost:8080")
        .AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod());
});

// RD-specific services
builder.Services.AddSingleton<IRepository>(new SqliteRepository());

var app = builder.Build();

// Configure the HTTP request pipeline. ALWAYS use Swagger.
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseCors("AllowSpecificOrigin");

EndpointsMapping.MapEndpoints(app);

app.Run();

﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Commons.Config
{
    /// <summary>
    /// This class parses the Settings.local file, built according to Settings.template file.
    /// Because of the stakeholders, I am not using standard C# .settings file.
    /// If there is no local file an exception shall be thrown.
    /// Look at localSettings.json, explanation in localSettings.template.
    /// </summary>
    public class AppConfigProvider
    {
        IConfigurationRoot _config;

        public AppConfigProvider()
        {
            try
            {
                _config = new ConfigurationBuilder()
                    .AddJsonFile("Config/localSettings.json", false, true)
                    .Build();
            } catch (Exception ex)
            {
                throw new InvalidOperationException("'Config/localSettings.json' is not working. Probably localSettings.json is malformed / not existing.", ex);
            }
            
        }

        public string GetPathToStoryFolder() =>
            _GetPathFromLocalsettings("riggeddice-story-folder");
        
        public string GetPathToActorFolder() =>
            _GetPathFromLocalsettings("riggeddice-actor-folder");

        public string GetPathToThreadOutputFolder() =>
            _GetPathFromLocalsettings("riggeddice-thread-output-folder");

        public string GetPathToActorOutputFolder() =>
            _GetPathFromLocalsettings("riggeddice-actor-output-folder");

        public string GetPathToGeneralOutputFolder() =>
            _GetPathFromLocalsettings("riggeddice-output-folder");

        public string GetPathToGeneratorInputFolder() =>
            _GetPathFromLocalsettings("riggeddice-generator-input-folder");

        /// <summary>
        /// By default, RiggedDice SQLite database file is located somewhere on HDD. But because of Docker configuration, we set the 
        /// 'default' as "app/db/riggeddice.db" to make it simple to use it in Docker.
        /// </summary>
        /// <returns></returns>
        public string GetPathToDatabaseFolder()
        {
            if (Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") == "true")
                return _config["riggeddice-sqlite-db-docker-path"];
            return _config["riggeddice-sqlite-db-folder-path"];
        }

        /// <summary>
        /// By default, RiggedDice SQLite database file is located somewhere on HDD. But because of Docker configuration, we set the 
        /// 'default' as "app/db/riggeddice.db" to make it simple to use it in Docker.
        /// </summary>
        /// <returns></returns>
        public string GetPathToDatabaseFile()
        {
            return Path.Combine(GetPathToDatabaseFolder(), _config["riggeddice-sqlite-db-file-name"]);
        }
        
        public string GetPathToStoryThreadFolder() =>
            _GetPathFromLocalsettings("riggeddice-thread-folder");
        
        public string GetPathToStoryMotiveFolder() =>
            _GetPathFromLocalsettings("riggeddice-motive-folder");
        
        public string GetPathToFactionFolder() =>
            _GetPathFromLocalsettings("riggeddice-faction-folder");

        public string GetPathToEleventyFactionFolder() =>
            _GetPathFromLocalsettings("riggeddice-ez-public-faction-folder");
        
        public string GetPathToActorCreationArchetypeFolder() =>
            _GetPathFromLocalsettings("riggeddice-actor-archetype-folder");
        
        public string GetPathToActorCreationHeartFolder() =>
            _GetPathFromLocalsettings("riggeddice-actor-heart-folder");
        
        public string GetPathToActorCreationAuxFolder() =>
            _GetPathFromLocalsettings("riggeddice-actor-aux-folder");
        
        public string GetPathToEzStoryMotiveFolder() =>
            _GetPathFromLocalsettings("riggeddice-ez-public-motives-folder");

        public string GetPathToEzStoryAgendasFolder() =>
            _GetPathFromLocalsettings("riggeddice-ez-public-agendas-folder");
        
        private string _GetPathFromLocalsettings(string key)
        {
            var path = _config[key];
            if (path == null)
                throw new InvalidOperationException($"localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template. Key: {key}");

            return path;
        }
    }
}

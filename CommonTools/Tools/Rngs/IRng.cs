﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Tools.Rngs
{
    public interface IRng
    {
        /// <summary>
        /// Returns yes or no.
        /// </summary>
        bool YesOrNo();

        int Next(int minNumber, int maxNumber);
    }
}

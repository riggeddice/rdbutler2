﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Tools.Rngs
{
    public class BasicRng : IRng
    {
        Random _random;

        public BasicRng()
        {
            _random = new Random();
        }

        /// <summary>
        /// Normal Random() generates ((minValue to maxValue - 1)). However, this one generates as expected below:
        /// '2', '3' will generate '2' or '3'.
        /// </summary>
        public int Next(int minNumber, int maxNumber)
        {
            return _random.Next(minNumber, maxNumber+1);
        }

        public bool YesOrNo()
        {
            int oneOrZero = _random.Next(2);
            if (oneOrZero == 1) return true;
            else if (oneOrZero == 0) return false;
            else throw new ArgumentOutOfRangeException("Somehow RNG generated neither 1 nor 0 when it should be 1 or 0");
        }
        
    }
}

﻿using Markdig;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Tools
{
    public static class MarkdownToHtml
    {
        public static string Convert(string input)
        {
            var pipeline = new MarkdownPipelineBuilder().UsePipeTables().Build();
            return Markdown.ToHtml(input, pipeline);
        }
    }
}

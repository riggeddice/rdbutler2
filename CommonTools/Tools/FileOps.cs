﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Commons.Tools
{
    public static class FileOps
    {
        public static FileRecord[] ReadFiles(string directoryPath)
        {
            string[] filePaths = Directory.GetFiles(directoryPath);
            var fileContents = filePaths.Select(s => ReadFile(s));
            return fileContents.ToArray();
        }
        
        /// <summary>
        /// Normal files we are working with are markdown files inside a directory. HOWEVER, Eleventy uses something
        /// like: "archetypes/arystokrata-aurum/index.md" instead of standard: "archetypes/arystokrata-aurum.md".
        /// This requires a slightly different approach to map filerecords.
        /// </summary>
        /// <param name="directoryPath">Directory path to Eleventy directory where the directories with index.md lie.</param>
        /// <returns></returns>
        public static FileRecord[] ReadEleventyFiles(string directoryPath)
        {
            // "archetypes/arystokrata-aurum/index.md" -> I want to find all "archetypes/arystokrata-aurum/"
            var directories = Directory.GetDirectories(directoryPath);
            
            var fileContents = directories
                .Select(dir => Path.Combine(dir, "index.md"))   // for each directory, make an index.md path, like "archetypes/arystokrata-aurum/index.md" 
                .Where(File.Exists)                                  // use directories where "index.md" exists
                .Select(ReadFile);                                   // read the file if it exists
            
            return fileContents.ToArray();
        }

        public static FileRecord ReadFile(string filePath)
        {
            string content = string.Empty;
            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                content = reader.ReadToEnd();
            }
            return new FileRecord(path: filePath, content: content);
        }

        public static void SaveFile(string content, string filePath)
        {
            using (StreamWriter writer = new StreamWriter(path: filePath))
            {
                writer.Write(content);
            }
        }
    }

    /// <summary>
    /// FileRecord is a class representing a read file: identifier with a filePath.
    /// </summary>
    public class FileRecord
    {
        public FileRecord(string path, string content) { Path = path; Content = content; }

        public string Path { get; }
        public string Content { get; }
    }
}

﻿using System;

namespace Commons.Tools
{
    public static class RegexFacade
    {
        public const string FORCE_DOTALL = @"(?s)";
        public const string UPPERCASE_LETTER = @"[A-ZĄĆĘŁŃÓŚŹŻ]";

        public static string ProperLocationRecord()
        {
            return @"\s*1\. [\w\s\d]+:.+$";
        }

        public static string ProperActorDeedRecord()
        {
            return @"[\w\s\d]+:.+?$";
        }

        public static string ExtractTargetsFromLink()
        {
            return @"\[.+?\]\(\b([\d\w\-]+)\b";
        }

        public static string ExtractIdentifierFromPath(string dateLength)
        {
            return $@"\b(\d{{{dateLength}}}-([\w-]+))\b";
        }

        public static string ExtractHeaderSection(int headerDepth, int blockingHeaderDepth, string name)
        {
            string safeName = SanitizeForRegex(name);
            string blockingDepth = blockingHeaderDepth.ToString();
            string header = $"#{{{headerDepth.ToString()}}}";

            return $@"^{header}\s{safeName}[:|\s]*$(.+?)(\Z|^#{{1,{blockingDepth}}}\s\w+)";
        }

        public static string ExtractParamFromFrontMatter(string name)
        {
            string safeName = SanitizeForRegex(name);
            return $"---.+?^{safeName}:\\s*\"*(.+?)\"*\\s*$.*---";
        }

        public static string ExtractParamFromMetadataSection(string name)
        {
            string safeName = SanitizeForRegex(name);
            return $"\\* {safeName}:\\s*\"*(.+)\"*\\s*";
        }

        public static string ExtractBulletPoints()
        {
            return @"[ \t]*\*+ .+$";
        }

        /// <summary>
        /// This captures things like flashcard blocks, where '* ' has children of type '\s+\*'.
        /// Requires DOTALL mode, so it is forced by regex with (?s).
        /// </summary>
        public static string ExtractBulletBlocksWithFirstUppercaseAndForcedDotall()
        {
            // this is what we aim at: (?s)(^\* [A-Z].+?)(?=\n\* |\z|\n\w|\n#|\n\d)  where (?s) means 'force (dotall) mode'
            const string FINISH_AT_NEW_BLOCK = @"(?=\n\* |\z|\n\w|\n#|\n\d)";

            string regex = $@"{FORCE_DOTALL}^(\* {UPPERCASE_LETTER}.+?){FINISH_AT_NEW_BLOCK}";
            return regex;
        }

        private static string SanitizeForRegex(string text)
        {
            return text.Replace("(", @"\(").Replace(")", @"\)").Replace("?", @"\?");
        }

    }
}

﻿using Commons.CoreEntities;

using System.Linq;

using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AtarRecordDbLoader
    {
        // This is very suboptimal. But unfortunately even if the code is IDENTICAL, I cannot unify them into one class because of EntityFramework.

        public static class Atar
        {
            public static AtarRecord[] CreateMany(AtarRecordModel[] atars)
            {
                AtarRecord[] records = atars.Select(a => CreateSingle(a)).ToArray();
                return records.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor).ToArray();
            }

            public static AtarRecord CreateSingle(AtarRecordModel aRecord)
            {
                return new AtarRecord(
                    originatingActor: aRecord.OriginatingActor,
                    relevantActor: aRecord.RelevantActor,
                    intensity: aRecord.Intensity,
                    storyUids: aRecord.StoryUids.Split(",").Select(r => r.Trim()).ToArray()
                    );
            }
        }

        public static class Atfr
        {
            public static AtarRecord[] CreateMany(AtfrRecordModel[] atfrs)
            {
                AtarRecord[] records = atfrs.Select(a => CreateSingle(a)).ToArray();
                return records.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor).ToArray();
            }

            public static AtarRecord CreateSingle(AtfrRecordModel aRecord)
            {
                return new AtarRecord(
                    originatingActor: aRecord.OriginatingActor,
                    relevantActor: aRecord.RelevantActor,
                    intensity: aRecord.Intensity,
                    storyUids: aRecord.StoryUids.Split(",").Select(r => r.Trim()).ToArray()
                    );
            }
        }

        public static class Ftar
        {
            public static AtarRecord[] CreateMany(FtarRecordModel[] ftars)
            {
                AtarRecord[] records = ftars.Select(a => CreateSingle(a)).ToArray();
                return records.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor).ToArray();
            }

            public static AtarRecord CreateSingle(FtarRecordModel aRecord)
            {
                return new AtarRecord(
                    originatingActor: aRecord.OriginatingActor,
                    relevantActor: aRecord.RelevantActor,
                    intensity: aRecord.Intensity,
                    storyUids: aRecord.StoryUids.Split(",").Select(r => r.Trim()).ToArray()
                    );
            }
        }

        public static class Ftfr
        {
            public static AtarRecord[] CreateMany(FtfrRecordModel[] ftfrs)
            {
                AtarRecord[] records = ftfrs.Select(a => CreateSingle(a)).ToArray();
                return records.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor).ToArray();
            }

            public static AtarRecord CreateSingle(FtfrRecordModel aRecord)
            {
                return new AtarRecord(
                    originatingActor: aRecord.OriginatingActor,
                    relevantActor: aRecord.RelevantActor,
                    intensity: aRecord.Intensity,
                    storyUids: aRecord.StoryUids.Split(",").Select(r => r.Trim()).ToArray()
                    );
            }
        }

    }
}

﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class GeneralStoryBlockDbLoader
    {
        public static GeneralStoryBlock CreateSingle(GeneralStoryBlockModel storyBlockModel)
        {
            return new GeneralStoryBlock(
                storyUid: storyBlockModel.StoryUid,
                storyTitle: storyBlockModel.StoryTitle,
                startDate: storyBlockModel.StartDate,
                endDate: storyBlockModel.EndDate,
                threads: storyBlockModel.Threads.Split(", "),
                motives: storyBlockModel.Motives.Split(", "),
                summary: storyBlockModel.Summary,
                actorsPresent: storyBlockModel.AllActorsPresent.Split(", ")
                );
        }
    }
}

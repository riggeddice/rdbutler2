﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class ActorBriefRecordDbLoader
    {
        public static ActorBriefRecord CreateSingle(ActorBriefRecordModel aidm)
        {

            return new ActorBriefRecord(
                name: aidm.Name,
                mechver: aidm.Mechver,
                intensity: aidm.Intensity,
                link: aidm.Link);
        }
    }
}

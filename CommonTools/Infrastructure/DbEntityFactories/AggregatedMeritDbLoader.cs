﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedMeritDbLoader
    {
        public static AggregatedMerit[] CreateMany(AggregatedMeritModel[] aMeritModels)
        {
            return aMeritModels.Select(m => CreateSingle(m)).ToArray();
        }

        public static AggregatedMerit CreateSingle(AggregatedMeritModel aMeritModel)
        {
            return new AggregatedMerit
            (
                originUid: aMeritModel.OriginUid,
                actor: aMeritModel.Actor,
                deed: aMeritModel.Deed,
                threads: aMeritModel.Threads,
                startDate: aMeritModel.StartDate,
                endDate: aMeritModel.EndDate
            );
        }

        // For now I am keeping them identical and in the same place
        // I see no reason to do otherwise

        public static AggregatedMerit[] CreateMany(AggregatedFactionMeritModel[] aFactionMeritModels)
        {
            return aFactionMeritModels.Select(m => CreateSingle(m)).ToArray();
        }

        public static AggregatedMerit CreateSingle(AggregatedFactionMeritModel aFactionMeritModel)
        {
            return new AggregatedMerit
            (
                originUid: aFactionMeritModel.OriginUid,
                actor: aFactionMeritModel.Actor,
                deed: aFactionMeritModel.Deed,
                threads: aFactionMeritModel.Threads,
                startDate: aFactionMeritModel.StartDate,
                endDate: aFactionMeritModel.EndDate
            );
        }
    }
}

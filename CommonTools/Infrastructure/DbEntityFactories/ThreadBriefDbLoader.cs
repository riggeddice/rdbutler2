﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class ThreadBriefRecordDbLoader
    {
        public static ThreadBriefRecord CreateSingle(ThreadBriefRecordModel r)
        {
            return new ThreadBriefRecord(
                uid: r.ThreadUid,
                name: r.Name,
                startDate: r.StartDate,
                endDate: r.EndDate,
                storyCount: r.StoryCount,
                players: r.Players.Split(", "),
                playerActors: r.PlayerActors.Split(", "),
                shortDesc: r.ShortDesc
                );
        }
    }
}

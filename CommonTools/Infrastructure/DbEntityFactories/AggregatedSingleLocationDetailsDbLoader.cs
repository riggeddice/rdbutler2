﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class AggregatedSingleLocationDetailsDbLoader
    {
        public static AggregatedSingleLocationDetails CreateSingle(LocationRecord[] lrms, GeneralStoryBlock[] blocks)
        {
            // Some fields are the same no matter the record. So we can use record[0]
            string name = lrms[0].Name; 
            string path = lrms[0].Path;

            // Some need to be aggregated from the wider group
            LocationRecord[] usefulRecords = lrms.Where(l => !string.IsNullOrEmpty(l.Event)).ToArray();
            string[] relevantStories = blocks.Select(b => b.StoryUid).OrderDescending().ToArray();
            string[] allThreads = blocks.SelectMany(b => b.Threads).Distinct().Order().ToArray();
            string[] allActors = blocks.SelectMany(b => b.AllActorsPresent).Order().Distinct().ToArray();
            string earliestStartDate = blocks.Select(block => block.StartDate).Min();
            string latestEndDate = blocks.Select(block => block.EndDate).Max();

            return new AggregatedSingleLocationDetails
            (
                name: name,
                path: path,
                relatedStories: relevantStories,
                relatedThreads: allThreads,
                allActorsPresent: allActors,
                firstStartDate: earliestStartDate,
                lastEndDate: latestEndDate,
                usefulLocRecords: usefulRecords,
                storyBlocks: blocks.OrderByDescending(b => b.EndDate).ToArray()
            );
        }
    }
}

﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class MotiveBriefRecordDbLoader
    {
        public static MotiveBriefRecord CreateSingle(MotiveBriefRecordModel r)
        {
            return new MotiveBriefRecord(
                uid: r.MotiveUid,
                name: r.Name,
                startDate: r.StartDate,
                endDate: r.EndDate,
                storyCount: r.StoryCount,
                players: r.Players.Split(", "),
                playerActors: r.PlayerActors.Split(", "),
                shortDesc: r.ShortDesc
                );
        }
    }
}

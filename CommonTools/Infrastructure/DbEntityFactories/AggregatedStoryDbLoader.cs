﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedStoryDbLoader
    {
        public static AggregatedStory CreateSingle(AggregatedStoryModel sim)
        {
            return new AggregatedStory(
                storyUid: sim.OriginatingStory,
                storyTitle: sim.OriginatingStoryTitle,
                threads: sim.Threads.Split(", "),
                motives: sim.Motives.Split(", "),
                startDate: sim.StartDate,
                endDate: sim.EndDate,
                seqNo: sim.SequenceNumber,
                previousStories: sim.PreviousStories.Split(", "),
                gms: sim.Gms.Split(", "),
                players: sim.Players.Split(", "),
                playerActors: sim.PlayerActors.Split(", "),
                allActors: sim.AllActors.Split(", "),
                summary: sim.Summary,
                body: sim.Body
                );
        }
    }
}

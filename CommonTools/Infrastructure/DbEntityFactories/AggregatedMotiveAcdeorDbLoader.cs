﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class AggregatedMotiveAcdeorDbLoader
    {
        public static AggregatedMotiveAcdeor CreateSingle(AggregatedMotiveAcdeorModel ama)
        {
            return new AggregatedMotiveAcdeor(
                originUid: ama.OriginUid, 
                actor:  ama.Actor, 
                deed: ama.Deed, 
                endDate: ama.EndDate
                );
        }
    }
}

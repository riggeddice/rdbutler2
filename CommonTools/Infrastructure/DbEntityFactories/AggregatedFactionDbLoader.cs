﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedFactionDbLoader
    {

        public static AggregatedFaction CreateSingle(string factionId, 
            ReadFactionModel readFaction, 
            FactionBriefRecordModel brief, 
            AggregatedMerit[] aMerits,
            FtarRecordModel[] ftarModels,
            FtfrRecordModel[] ftfrModels)
        {

            string name = string.Empty;
            string shortDesc = string.Empty;
            string body = string.Empty;

            if (brief != null)
            {
                name = brief.Name;
                shortDesc = brief.ShortDesc;
            }

            if (readFaction != null)
            {
                if(readFaction.Name != brief.Name) { name = readFaction.Name; }
                body = readFaction.Body;
            }

            return new AggregatedFaction(
                factionId: factionId,
                name: name,
                shortDesc: shortDesc,
                body: body,
                merits: aMerits,
                ftarRelations: AtarRecordDbLoader.Ftar.CreateMany(ftarModels),
                ftfrRelations: AtarRecordDbLoader.Ftfr.CreateMany(ftfrModels)
            );
        }
    }
}

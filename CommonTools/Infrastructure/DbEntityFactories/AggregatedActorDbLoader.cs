﻿using Commons.CoreEntities;
using System;
using System.Linq;

using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedActorDbLoader
    {
        public static AggregatedActor Create(EveryActorModel aActorModel,
            AggregatedMeritModel[] aMeritModels,
            AggregatedProgressionModel[] aProgressionModels,
            AtarRecordModel[] atarModels, AtfrRecordModel[] atfrModels,
            FlashcardRecordModel[] flashcardRecordModels, string[] threads)
        {
            string uid = aActorModel.ActorUid;
            string name = aActorModel.Name;
            string mechver = aActorModel.Mechver;
            string owner = aActorModel.Owner;
            string body = aActorModel.Body;

            AggregatedMeritModel[] merits = aMeritModels.ToArray();
            AggregatedProgressionModel[] progressions = aProgressionModels.ToArray();
            AtarRecordModel[] atars = atarModels.ToArray();
            AtfrRecordModel[] atfrs = atfrModels.ToArray();

            return new AggregatedActor(
                uid: uid,
                name: name,
                mechver: mechver,
                factions: Array.Empty<string>(),
                owner: owner,
                body: body,
                merits: AggregatedMeritDbLoader.CreateMany(merits).OrderBy(m => m.EndDate).ThenBy(m => m.OriginUid).ToArray(),
                progressions: AggregatedProgressionDbLoader.CreateMany(progressions).OrderBy(m => m.EndDate).ThenBy(m => m.OriginUid).ToArray(),
                atarRelations: AtarRecordDbLoader.Atar.CreateMany(atars),
                atfrRelations: AtarRecordDbLoader.Atfr.CreateMany(atfrs),
                flashcardRecords: FlashcardRecordDbLoader.CreateMany(flashcardRecordModels),
                threads: threads
            );
        }

        public static AggregatedActor EmptyAndInvalid()
        {
            return new AggregatedActor(
                uid: "9999-INVALID-RECORD",
                name: "INVALID-RECORD",
                mechver: "9999",
                factions: Array.Empty<string>(),
                owner: string.Empty,
                body: string.Empty,
                merits: [],
                progressions: [],
                atarRelations: [],
                atfrRelations: [],
                flashcardRecords: [],
                threads: []
            );
        }
    }
}

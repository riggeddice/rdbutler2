﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class FactionBriefRecordDbLoader
    {
        public static FactionBriefRecord CreateSingle(FactionBriefRecordModel m)
        {
            return new FactionBriefRecord(
                name: m.Name,
                intensity: m.Intensity,
                link: m.Link,
                shortDesc: m.ShortDesc
                );
        }
    }
}

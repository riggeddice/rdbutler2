﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class AggregatedMotiveDbLoader
    {
        public static AggregatedMotive CreateSingle(
            string uid, 
            ReadMotiveModel readMotive, 
            GeneralStoryBlockModel[] relevantStoryBlocks, 
            AggregatedMotiveAcdeorModel[] relevantMotiveAcdeors)
        {
            return new AggregatedMotive(
                uid: uid,
                name: readMotive != null ? readMotive.Name : "",
                shortDesc: readMotive != null ? readMotive.ShortDesc : "",
                fullDesc: readMotive != null ? readMotive.FullDesc : "",
                spoilers: readMotive != null ? readMotive.Spoilers : "",
                storyBlocks: relevantStoryBlocks.Select(b => GeneralStoryBlockDbLoader.CreateSingle(b)).OrderByDescending(b => b.EndDate).ToArray(),
                motiveAcdeors: relevantMotiveAcdeors.Select(m => AggregatedMotiveAcdeorDbLoader.CreateSingle(m)).ToArray()
                );
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{

    public class StoryToPrevChronoRelation
    {
        [Key]
        public int Id { get; set; }
        public string StoryId { get; set; }
        public string PrevChronoId { get; set; }
    }

    public class StoryToGmRelation
    {
        [Key]
        public int Id { get; set; }
        public string StoryId { get; set; }
        public string GmId { get; set; }
    }

    public class StoryToPlayerRelation
    {
        [Key]
        public int Id { get; set; }
        public string StoryId { get; set; }
        public string PlayerId { get; set; }
    }

    public class StoryToThreadRelation
    {
        [Key]
        public int Id { get; set; }
        public string StoryId { get; set; }
        public string ThreadId { get; set; }
    }
}

using System.ComponentModel.DataAnnotations;

namespace Commons.Infrastructure.DbModels;

public class ReadActorCreationArchetypeModel
{
    [Key]
    public int Uid { get; set; }
    public string Identifier { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Body { get; set; }
}

public class ReadActorCreationHeartModel
{
    [Key]
    public int Uid { get; set; }
    public string Identifier { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Body { get; set; }
}

public class ReadActorCreationAuxModel
{
    [Key]
    public int Uid { get; set; }
    public string Identifier { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Body { get; set; }
}
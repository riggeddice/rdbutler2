﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbModels
{
    public class StoryBriefRecordModel
    {
        [Key]
        public int Uid { get; set; }
        public string StoryUid { get; set; }
        public string Title { get; set; }
        public string Threads { get; set; }
        public string Motives { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Players { get; set; }
    }
}

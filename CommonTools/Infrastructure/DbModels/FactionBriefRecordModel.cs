﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbModels
{
    public class FactionBriefRecordModel
    {
        [Key]
        public int Uid { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string ShortDesc { get; set; }
        public int Intensity { get; set; }
    }
}

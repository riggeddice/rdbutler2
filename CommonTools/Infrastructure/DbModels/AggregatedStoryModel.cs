﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class AggregatedStoryModel
    {
        [Key]
        public int Uid { get; set; }
        public string OriginatingStory { get; set; }
        public string OriginatingStoryTitle { get; set; }
        public string Threads { get; set; }
        public string Motives { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PreviousStories { get; set; }
        public string SequenceNumber { get; set; }
        public string Gms { get; set; }
        public string Players { get; set; }
        public string PlayerActors { get; set; }
        public string AllActors { get; set; }
        public string Summary { get; set; }
        public string Body{ get; set; }
    }
}


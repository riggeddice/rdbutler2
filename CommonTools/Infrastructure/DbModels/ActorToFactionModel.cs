﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class ActorToFactionModel
    {
        [Key]
        public int Uid { get; set; }
        public string ActorName { get; set; }
        public string Faction { get; set; }
    }

}

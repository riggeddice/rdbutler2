using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Commons.Infrastructure.DbModels;

public class ActorFlashcardModel
{
    [Key]
    public string Uid { get; set; }
    public string Name { get; set; }

    public List<FlashcardRecordModel> Records { get; set; } = new();
}
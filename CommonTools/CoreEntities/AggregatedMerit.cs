﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.CoreEntities
{

    /// <summary>
    /// Everything about a Merit we want to know.
    /// </summary>
    public class AggregatedMerit
    {
        public AggregatedMerit(
            string originUid,
            string actor,
            string deed,
            string threads,
            string startDate,
            string endDate)
        {
            OriginUid = originUid;
            Actor = actor;
            Deed = deed;
            Threads = threads;
            StartDate = startDate;
            EndDate = endDate;
        }

        public string OriginUid { get; }
        public string Actor { get; }
        public string Deed { get; }
        public string Threads { get; }
        public string StartDate { get; }
        public string EndDate { get; }

        public override string ToString()
        {
            return $"{Actor} @ {OriginUid} : {Deed} |at| {StartDate}, {EndDate}";
        }

        public override int GetHashCode() => HashCode.Combine(OriginUid, Actor, Deed, Threads, EndDate);
        public override bool Equals(object obj)
        {

            if (!(obj is AggregatedMerit other)) return false;

            if (OriginUid != other.OriginUid) return false;
            if (Actor != other.Actor) return false;
            if (Deed != other.Deed) return false;
            if (Threads != other.Threads) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;

            return true;
        }
    }

    public class AggregatedFactionMerit : AggregatedMerit
    {
        public AggregatedFactionMerit(string originUid, string actor, string deed, string threads, string startDate, string endDate) 
            : base(originUid, actor, deed, threads, startDate, endDate)
        {
        }
    }

}

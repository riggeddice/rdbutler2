﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    /// <summary>
    /// AggregatedStory represents EVERY information we have on a specific Story in the system EXCEPT the body of the Story.
    /// If you need to know anything about the Story, it is here.
    /// </summary>
    public class AggregatedStory
    {
        public AggregatedStory(
            string storyUid,
            string storyTitle,
            string[] threads,
            string[] motives,
            string startDate,
            string endDate,
            string seqNo,
            string[] previousStories,
            string[] gms,
            string[] players,
            string[] playerActors,
            string[] allActors,
            string summary,
            string body
            )
        {
            StoryUid = storyUid;
            StoryTitle = storyTitle;
            Threads = threads;
            Motives = motives;
            StartDate = startDate;
            EndDate = endDate;
            SequenceNumber = seqNo;
            PreviousStories = previousStories;
            Gms = gms;
            Players = players;
            PlayerActors = playerActors;
            AllActors = allActors;
            Summary = summary;
            Body = body;
        }

        public string StoryUid { get; }
        public string StoryTitle { get; }
        public string[] Threads { get; }
        public string[] Motives { get; }
        public string StartDate { get; }
        public string EndDate { get; }
        public string SequenceNumber { get; }
        public string[] PreviousStories { get; }
        public string[] Gms { get; }
        public string[] Players { get; }
        public string[] PlayerActors { get; }
        public string[] AllActors { get; }
        public string Summary { get; }
        public string Body { get; }

        public override int GetHashCode() => HashCode.Combine(StoryUid, string.Join(", ", Threads), StartDate, EndDate, SequenceNumber, string.Join(",", PreviousStories));
        public override bool Equals(object obj)
        {

            if (obj is not AggregatedStory other) return false;

            if (StoryUid != other.StoryUid) return false;
            if (StoryTitle != other.StoryTitle) return false;
            if (Threads.SequenceEqual(other.Threads) == false) return false;
            if (Motives.SequenceEqual(other.Motives) == false) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;
            if (SequenceNumber != other.SequenceNumber) return false;
            if (PreviousStories.SequenceEqual(other.PreviousStories) == false) return false;
            if (Gms.SequenceEqual(other.Gms) == false) return false;
            if (Players.SequenceEqual(other.Players) == false) return false;
            if (PlayerActors.SequenceEqual(other.PlayerActors) == false) return false;
            if (AllActors.SequenceEqual(other.AllActors) == false) return false;
            // summary is NEVER being used for equalization. Too much text, too irrelevant
            // body is NEVER being used for equalization. Too much text, too irrelevant

            return true;
        }

        public override string ToString()
        {
            return $"{StoryUid}, name: {StoryTitle}, seqNo: {SequenceNumber}, threads: {Threads}, date: {StartDate} - {EndDate}, prev: {string.Join(", ", PreviousStories)}, " +
                $"gms: {string.Join(", ", Gms)}, players: {string.Join(", ", Players)}, playerActors: {string.Join(", ", PlayerActors)}";
        }
    }
}

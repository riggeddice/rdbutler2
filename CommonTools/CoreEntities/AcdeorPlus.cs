﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.CoreEntities
{
    /// <summary>
    /// Represents every generic Actor-Deed-Origin with EndDate or other extended info
    /// which does not require more advanced information.
    /// Common entity for everything in RdButler2.
    /// </summary>
    public class AcdeorPlus
    {
        public AcdeorPlus(string originUid, string actor, string deed, string endDate)
        {
            OriginUid = originUid;
            Actor = actor;
            Deed = deed;
            EndDate = endDate;
        }

        public string OriginUid { get; }
        public string Actor { get; }
        public string Deed { get; }
        public string EndDate { get; }

        public override int GetHashCode() => HashCode.Combine(OriginUid, Actor, Deed, EndDate);
        public override bool Equals(object obj)
        {

            if (!(obj is AcdeorPlus other)) return false;

            if (OriginUid != other.OriginUid) return false;
            if (Actor != other.Actor) return false;
            if (Deed != other.Deed) return false;
            if (EndDate != other.EndDate) return false;

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class AggregatedFaction(
        string factionId, 
        string name, 
        string shortDesc, 
        string body, 
        AggregatedMerit[] merits, 
        AtarRecord[] ftarRelations,
        AtarRecord[] ftfrRelations )
    {
        public string FactionId { get; internal set; } = factionId;
        public string Name { get; internal set; } = name;
        public string ShortDesc { get; internal set; } = shortDesc;
        public string Body { get; internal set; } = body;
        public AggregatedMerit[] Merits { get; internal set; } = merits;
        public AtarRecord[] FtarRelations { get; internal set; } = ftarRelations;
        public AtarRecord[] FtfrRelations { get; internal set; } = ftfrRelations;
    }
}

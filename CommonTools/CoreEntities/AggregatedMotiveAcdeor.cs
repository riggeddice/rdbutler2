﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class AggregatedMotiveAcdeor(
        string originUid,
        string actor,
        string deed,
        string endDate)
    {
        public string OriginUid { get; } = originUid;
        public string Actor { get; } = actor;
        public string Deed { get; } = deed;
        public string EndDate { get; } = endDate;

        public override string ToString() => $"{Actor} @ {OriginUid} : {Deed} |ends at| {EndDate}";
        
        public override int GetHashCode() => HashCode.Combine(OriginUid, Actor, Deed, EndDate);
        public override bool Equals(object obj)
        {
            if (obj is not AggregatedMotiveAcdeor other) return false;

            if (OriginUid != other.OriginUid) return false;
            if (Actor != other.Actor) return false;
            if (Deed != other.Deed) return false;
            if (EndDate != other.EndDate) return false;

            return true;
        }
    }
}

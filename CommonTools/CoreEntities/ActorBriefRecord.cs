﻿using System;

namespace Commons.CoreEntities
{
    /// <summary>
    /// This represents the "short record" of an Actor, something presentable 'for all actors' in the ActorList.
    /// </summary>
    public class ActorBriefRecord
    {
        public ActorBriefRecord(string name, string mechver, int intensity, string link)
        {
            Name = name;
            Mechver = mechver;
            Intensity = intensity;
            Link = link;
        }

        public string Name { get; }
        public string Mechver { get; }
        public int Intensity { get; }
        public string Link { get; }

        public override int GetHashCode() => HashCode.Combine(Name, Mechver, Intensity, Link);
        public override bool Equals(object obj)
        {
            if (obj is not ActorBriefRecord other) return false;

            if (Name != other.Name) return false;
            if (Mechver != other.Mechver) return false;
            if (Intensity != other.Intensity) return false;
            if (Link != other.Link) return false;

            return true;
        }

        public override string ToString() => $"{Name} at {Mechver} with intensity: {Intensity} with link: {Link}";
    }
}

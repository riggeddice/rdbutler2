using System;
using System.Linq;

namespace Commons.CoreEntities;

public class AggregatedActorFlashcard
{
    public string Uid { get; }

    public string ActorName { get; }

    public FlashcardRecord[] Records { get; }

    public AggregatedActorFlashcard(string uid, string actorName, FlashcardRecord[] records)
    {
        Uid = uid;
        ActorName = actorName;
        Records = records;
    }

    public override int GetHashCode() => HashCode.Combine(Uid, ActorName, Records);

    public override bool Equals(object obj)
    {
        if (obj is not AggregatedActorFlashcard other) return false;

        if (Uid != other.Uid) return false;
        if (ActorName != other.ActorName) return false;
        if (Records.SequenceEqual(other.Records) == false) return false;

        return true;
    }
}
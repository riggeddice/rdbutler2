﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class AggregatedMotive(
        string uid, string name, string shortDesc, string fullDesc, string spoilers, 
        GeneralStoryBlock[] storyBlocks, AggregatedMotiveAcdeor[] motiveAcdeors
        )
    {
        public string Uid { get; } = uid;
        public string Name { get; } = name;
        public string ShortDesc { get; } = shortDesc;
        public string FullDesc { get; } = fullDesc;
        public string Spoilers { get; } = spoilers;
        public GeneralStoryBlock[] StoryBlocks { get; } = storyBlocks;
        public AggregatedMotiveAcdeor[] MotiveAcdeors { get; } = motiveAcdeors;

        public override int GetHashCode() => HashCode.Combine(Uid, Name, ShortDesc, FullDesc, Spoilers);
        public override bool Equals(object obj)
        {
            if (obj is not AggregatedMotive other) return false;

            if (Uid != other.Uid) return false;
            if (Name != other.Name) return false;
            if (ShortDesc != other.ShortDesc) return false;
            if (FullDesc != other.FullDesc) return false;
            if (Spoilers != other.Spoilers) return false;

            return true;
        }

        public override string ToString() => $"{Uid} : {Name}, ShortDesc: {ShortDesc}, StoryBlocks.Count: {StoryBlocks.Length}, MotiveAcdeors.Count: {MotiveAcdeors.Length}";
    }
}

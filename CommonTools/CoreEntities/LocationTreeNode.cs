﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    /// <summary>
    /// This one is used to represent a JSon From Hell - a structure which
    /// contains all LocationRecords as a single large recursive structure.
    /// </summary>
    public class LocationTreeNode
    {
        public string Title { get; set; }
        public string Path { get; set; }
        public List<LocationTreeNode> Children { get; set; }

        public override int GetHashCode() => HashCode.Combine(Title, Path, Children.Count);

        public override bool Equals(object obj)
        {
            if (obj is not LocationTreeNode other)
                return false;

            if (Title != other.Title)
                return false;

            if (Path != other.Path)
                return false;

            if (Children.Count != other.Children.Count)
                return false;

            for (int i=0; i< Children.Count; i++)
            {
                if (Children.SequenceEqual(other.Children) != true) return false;
            }

            return true;
        }

        public override string ToString()
        {
            return $"Title: {Title} @ {Path}, children count: {Children.Count}";
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static LocationTreeNode CreateFromJson(string recordTreeString)
        {
            return JsonConvert.DeserializeObject<LocationTreeNode>(recordTreeString);
        }
    }
}

using System;

namespace Commons.CoreEntities;

public class FlashcardRecord
{
    public string OriginStoryUid { get; }

    public string Body { get; }

    public FlashcardRecord(string originStoryUid, string body)
    {
        OriginStoryUid = originStoryUid;
        Body = body;
    }

    public override string ToString() => $"{Body} | @ {OriginStoryUid}";

    public override int GetHashCode() => HashCode.Combine(OriginStoryUid, Body);

    public override bool Equals(object obj)
    {
        if (obj is not FlashcardRecord other) return false;

        if (OriginStoryUid != other.OriginStoryUid) return false;
        if (Body != other.Body) return false;

        return true;
    }
}
﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P2_Extraction
{
    internal class CreateMotiveAcdeors
    {
        [Test]
        public void Having_predefined_story_from_md_file_all_9_MotiveAcdeors_are_read()
        {
            // Given
            FileRecord record = FileOps.ReadFile(Path.Combine("TestData", "240102-zaloga-vishaera-przezyje.md"));
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            // When
            MotiveAcdeor[] motives = MotiveAcdeorExtractor.CreateManyFromStories([story]);

            // Expected
            MotiveAcdeor example = new(
                originatingStoryUid: "240102-zaloga-vishaera-przezyje",
                actor: "heart-is-a-weakness",
                deed: "ludzie z SC Talikazer próbowali uratować AK Vishaer i spotkała ich straszliwa 'kara' za ich dobre serce"
                );

            int motiveCount = 9;

            // Then
            Assert.That(motives.Length == motiveCount);
            Assert.That(motives.Where(s => s.Equals(example)).First() is MotiveAcdeor);

        }

    }
}

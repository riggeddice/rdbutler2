﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateAggregatedMotiveAcdeors
    {
        [Test]
        public void For_single_story_and_motiveAcdeor_aggregates_them_into_aggregatedMotiveAcdeor()
        {
            // Given
            AggregatedStory story = AggregatedStoryComposer.CreateWithDefaults(
                storyUid: "230606-first",
                startDate: "0111-12-10",
                endDate: "0111-12-12"
                );

            MotiveAcdeor aMotiveAcdeor = new (
                originatingStoryUid: story.StoryUid,
                actor: "energia-interis",
                deed: "was used to destroy the story and everything");

            // When
            AggregatedMotiveAcdeor actual = AggregatedMotiveAcdeorComposer.Create([aMotiveAcdeor], [story])[0];

            // Then
            AggregatedMotiveAcdeor expected = new(
                originUid: story.StoryUid,
                actor: aMotiveAcdeor.Actor,
                deed: aMotiveAcdeor.Deed,
                endDate: story.EndDate);

            Assert.That(expected, Is.EqualTo(actual) );
        }
    }
}

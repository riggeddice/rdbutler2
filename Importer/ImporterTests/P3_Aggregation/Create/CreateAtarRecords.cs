﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P1_ReadParsing.Factories;
using Importer.P3_Aggregation.FactoriesSupportive;

namespace ImporterTests.P3_Aggregation.Create
{
    class CreateAtarRecords
    {
        [Test]
        public void For_single_originating_actor_name_builds_AtarRecords_for_all_Actors_in_Stories_except_themselves()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(Path.Combine("TestData", "200930-lekarz-dla-elizy.md"));
            FileRecord record2 = FileOps.ReadFile(Path.Combine("TestData", "200923-magiczna-burza-w-raju.md"));

            ReadStory[] stories = ReadStoryFactory.CreateMany([record1, record2]);
            ActorMerit[] aMerits = ActorMeritExtractor.CreateManyFromStories(stories);

            string originatingActor = "Eliza Ira";

            // Expected
            int atarCount = 14; // 14 other characters, none repeated twice

            // When
            AtarRecord[] atars = AtarRecordComposer.CreateFor(originatingActor, aMerits, aMerits);

            // Then
            Assert.That(atars.Length, Is.EqualTo(atarCount));
            Assert.That(atars.Where(a => a.Equals("Eliza Ira")).ToArray().Length == 0);   // does not have self record
        }

        [Test]
        public void For_single_originating_actor_name_builds_AtfrRecords_for_single_story()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(Path.Combine("TestData", "231221-pan-skarpetek-i-odratowany-ogrod.md"));
            FileRecord record2 = FileOps.ReadFile(Path.Combine("TestData", "240102-zaloga-vishaera-przezyje.md"));

            ReadStory[] stories = ReadStoryFactory.CreateMany([record1, record2]);
            
            ActorMerit[] aMerits = ActorMeritExtractor.CreateManyFromStories(stories);
            FactionMerit[] fMerits = FactionMeritExtractor.CreateManyFromStories(stories);

            string originatingActor = "Felina Amatanir";

            // Expected
            int atfrCount = 3; // 3 factions, each with intensity 1

            // When
            AtarRecord[] atfrs = AtarRecordComposer.CreateFor(originatingActor, aMerits, fMerits);

            // Then
            Assert.That(atfrs.Length, Is.EqualTo(atfrCount));
            Assert.That(atfrs.Where(r => r.RelevantActor.Contains("Szernief") == false).Any() == false);  // the only factions are from Szernief; no Vishaer records present
        }

        [Test]
        public void For_single_originating_actor_name_builds_FtarRecords_for_single_story()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(Path.Combine("TestData", "231221-pan-skarpetek-i-odratowany-ogrod.md"));
            FileRecord record2 = FileOps.ReadFile(Path.Combine("TestData", "240102-zaloga-vishaera-przezyje.md"));

            ReadStory[] stories = ReadStoryFactory.CreateMany([record1, record2]);
            GlobalTime globalTime = new() { { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) } };
            AggregatedStory[] storyMetadatas = AggregatedStoryComposer.CreateManyFromStories(stories, globalTime);

            ActorMerit[] aMerits = ActorMeritExtractor.CreateManyFromStories(stories);
            FactionMerit[] fMerits = FactionMeritExtractor.CreateManyFromStories(stories);

            string originatingFaction = "Con Szernief Querenci";

            // Expected
            int ftarCount = 7; // 7 actors, each with intensity 1

            // When
            AtarRecord[] ftars = AtarRecordComposer.CreateFor(originatingFaction, fMerits, aMerits);

            // Then
            Assert.That(ftars.Length, Is.EqualTo(ftarCount));
            Assert.That(ftars.Where(r => r.RelevantActor.Contains("Klasa")).Count() == 3);  // this story has 3 Archetype classes
        }

        [Test]
        public void For_single_originating_actor_name_builds_FtfrRecords_for_single_story()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(Path.Combine("TestData", "231221-pan-skarpetek-i-odratowany-ogrod.md"));
            FileRecord record2 = FileOps.ReadFile(Path.Combine("TestData", "240102-zaloga-vishaera-przezyje.md"));

            ReadStory[] stories = ReadStoryFactory.CreateMany([record1, record2]);
            FactionMerit[] fMerits = FactionMeritExtractor.CreateManyFromStories(stories);

            string originatingFaction = "Con Szernief Querenci";

            // Expected
            int ftarCount = 2; // 3 factions total, 1 is 'self', so 2.

            // When
            AtarRecord[] ftars = AtarRecordComposer.CreateFor(originatingFaction, fMerits, fMerits);

            // Then
            Assert.That(ftars.Length, Is.EqualTo(ftarCount));
            Assert.That(ftars.Where(r => r.RelevantActor.Contains("Unifikaci")).Count() == 1);  // this story contains the other faction.
        }
    }
}

﻿using Commons.CoreEntities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateStoryBrief
    {
        [Test]
        public void CreateSimpleBrief()
        {
            // Given

            string storyUid = "211112-current";
            string storyTitle = "Current Story";
            string[] threads = ["thread1", "thread2"];
            string[] motives = [];
            string startDate = "00800917";
            string endDate = "00800924";
            string[] players = ["pit", "til"];

            AggregatedStory story = new AggregatedStory(
                storyUid: storyUid,
                storyTitle: storyTitle,
                threads: threads,
                motives: motives,
                startDate: startDate,
                endDate: endDate,
                seqNo: "3",
                previousStories: ["211111-previous"],
                gms: ["żółw"],
                players: ["pit", "til"],
                playerActors: ["Michael Firegiver", "Eltor Retop"],
                allActors: ["Michael Firegiver", "Eltor Retop", "Elena Xairiss"],
                summary: "text",
                body: "more text"
                );

            // When
            StoryBriefRecord actual = StoryBriefRecordComposer.CreateSingle(story);

            // Then
            StoryBriefRecord expected = new StoryBriefRecord(
                storyUid: storyUid,
                title: storyTitle,
                threads: string.Join(", ", threads),
                motives: string.Join(", ", motives),
                startDate: startDate,
                endDate: endDate,
                players: string.Join(", ", players)
                );

            Assert.That(expected, Is.EqualTo(actual) );
        }
    }
}

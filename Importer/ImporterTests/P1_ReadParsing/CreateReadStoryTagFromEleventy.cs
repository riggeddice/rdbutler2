﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing
{
    public class CreateReadStoryTagFromEleventy
    {
        [Test]
        public void Creating_single_properly_extracts_all_fields_from_well_formed_record()
        {
            // Given
            FileRecord record = new FileRecord(
                path: @"/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/story-creation/motives/lojalnosc-syntetycznych-intelektow/index.md",
                content: @"---
title: Lojalność Syntetycznych Intelektów
layout: layouts/component.njk
eleventyNavigation:
  key: lojalnosc-syntetycznych-intelektow
  parent: components
tags: [story_motive, component]
description: 'SI nie ufamy; są narzędziami. Ale pokazują, że w chwili potrzeby są lojalne z własnej woli, nie z musu.'
---
# {{ title }}

## Opis
### 1. Ogólny opis

{{description}}

Syntetyczne Intelekty (SI) dbają o swoich ludzi...

## Spoilers

brak
");

            // When
            ReadStoryTag actual = ReadStoryTagFactory.CreateSingleFromEleventy(record);

            // Then
            ReadStoryTag expected = new(
                tagUid: "lojalnosc-syntetycznych-intelektow",
                name: "Lojalność Syntetycznych Intelektów",
                shortDesc: @"'SI nie ufamy; są narzędziami. Ale pokazują, że w chwili potrzeby są lojalne z własnej woli, nie z musu.'",
                fullDesc: @"### 1. Ogólny opis

{{description}}

Syntetyczne Intelekty (SI) dbają o swoich ludzi...",
                spoilers: "brak"
                );

            Assert.That(expected, Is.EqualTo(actual) );

        }
        
        [Test]
        public void Lack_of_spoilers_is_not_a_problem()
        {
            // Given
            FileRecord record = new(
                path: @"/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/story-creation/motives/lojalnosc-syntetycznych-intelektow/index.md",
                content: @"---
title: Lojalność Syntetycznych Intelektów
layout: layouts/component.njk
eleventyNavigation:
  key: lojalnosc-syntetycznych-intelektow
  parent: components
tags: [story_motive, component]
description: 'SI nie ufamy; są narzędziami. Ale pokazują, że w chwili potrzeby są lojalne z własnej woli, nie z musu.'
---
# {{ title }}

## Opis
### 1. Ogólny opis

{{description}}

Syntetyczne Intelekty (SI) dbają o swoich ludzi...

");

            // When
            ReadStoryTag actual = ReadStoryTagFactory.CreateSingleFromEleventy(record);

            // Then
            ReadStoryTag expected = new(
                tagUid: "lojalnosc-syntetycznych-intelektow",
                name: "Lojalność Syntetycznych Intelektów",
                shortDesc: @"'SI nie ufamy; są narzędziami. Ale pokazują, że w chwili potrzeby są lojalne z własnej woli, nie z musu.'",
                fullDesc: @"### 1. Ogólny opis

{{description}}

Syntetyczne Intelekty (SI) dbają o swoich ludzi...",
                spoilers: string.Empty
            );

            Assert.That(expected, Is.EqualTo(actual) );

        }

    }
}

﻿using Importer.P1_ReadParsing.Entities;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing
{
    public class CompareReadStories
    {
        [Test]
        public void Two_ReadStories_are_equal_if_they_have_all_members_the_same_in_value()
        {
            // Given
            ReadStory s1 = new(
                uid: "1",
                title: "1",
                threads: ["current-story"],
                motives: ["motive"],
                prevChronoIds: ["1"],
                prevCampaignIds: ["1"],
                gms: ["1"],
                players: ["1"],
                summary: "1",
                location: "1",
                timeRecord: new TimeRecord(2, 3, "00190308", ""),
                actorNames: ["name"],
                body: "1"
                );

            ReadStory s2 = new(
                uid: "1",
                title: "1",
                threads: ["current-story"],
                motives: ["motive"],
                prevChronoIds: ["1"],
                prevCampaignIds: ["1"],
                gms: ["1"],
                players: ["1"],
                summary: "1",
                location: "1",
                timeRecord: new TimeRecord(2, 3, "00190308", ""),
                actorNames: ["name"],
                body: "1"
                );

            // When, Then
            Assert.That(s1, Is.EqualTo(s2));
        }
    }
}

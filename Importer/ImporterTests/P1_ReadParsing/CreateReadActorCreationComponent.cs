using System.IO;
using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing;

public class CreateReadActorCreationComponent
{
    [Test]
    public void ReadActorCreationComponent_Archetype_is_created_from_simplified_record()
    {
        // Given
        FileRecord record = new FileRecord(
            path: "/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/actor-creation/actor-archetypes/arystokrata-aurum/index.md",
            content: @"---
title: Arystokrata Aurum
layout: layouts/component.njk  
eleventyNavigation:  
  key: arystokrata-aurum
  parent: components  
tags: [actor_trait, actor_trait_archetype, component]  
description: 'Mag jednego z Rodów Aurum, połączony sentisiecią z Rodem, terenem i innymi ze swego Rodu.'
image: 'actor-archetypes/arystokrata-aurum/arystokrata-aurum-face.png'
---

# {{ title }}  
## Krótki opis  

{% image ""./240912-aurum-aristocrat-archetype.png"", ""[Terminus]"" %}
" 
            );

        // When
        ReadActorCreationComponent actual = ReadActorCreationComponentFactory.CreateSingleArchetype(record);

        // Expected
        ReadActorCreationComponent expected = new(
            Identifier: "arystokrata-aurum",
            Title: "Arystokrata Aurum",
            Description: "'Mag jednego z Rodów Aurum, połączony sentisiecią z Rodem, terenem i innymi ze swego Rodu.'",
            ComponentType: ReadActorCreationComponentDefaults.ArchetypeComponentType,
            Body: record.Content
                );

        // Then
        // WARNING - we do not check Body equality!
        Assert.That(expected, Is.EqualTo(actual) );
    }
    
    [Test]
    public void ReadActorCreationComponent_Heart_is_created_from_simplified_record()
    {
        // Given
        FileRecord record = new FileRecord(
            path: "/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/actor-creation/actor-hearts/bohater-pozytywny/index.md",
            content: @"---
title: Bohater Pozytywny  
layout: layouts/component.njk  
eleventyNavigation:  
  key: bohater-pozytywny  
  parent: components  
tags: [actor_trait, actor_trait_heart, component]  
description: 'Osoba, która jest w stanie pomóc, więc pomaga. Skoro może pomóc, to chce pomóc. Niczego nie oczekuje w zamian. '  
ocean: 'O? C? E+ A+ N0'  
energia: '[Praecis](/ez-world/energies-magic/praecis)'  
---

# {{ title }}  
## Krótki opis  

{{ description }}  
" 
        );

        // When
        ReadActorCreationComponent actual = ReadActorCreationComponentFactory.CreateSingleHeart(record);

        // Expected
        ReadActorCreationComponent expected = new(
            Identifier: "bohater-pozytywny",
            Title: "Bohater Pozytywny",
            Description: "'Osoba, która jest w stanie pomóc, więc pomaga. Skoro może pomóc, to chce pomóc. Niczego nie oczekuje w zamian. '",
            ComponentType: ReadActorCreationComponentDefaults.HeartComponentType,
            Body: record.Content
        );

        // Then
        // WARNING - we do not check Body equality!
        Assert.That(expected, Is.EqualTo(actual) );
    }
    
    [Test]
    public void ReadActorCreationComponent_Aux_is_created_from_simplified_record()
    {
        // Given
        FileRecord record = new(
            path: "/home/ljzolw/Riggeddice/ez-public/content/ez-game/components/actor-creation/actor-traits-aux/biosynt/index.md",
            content: @"---
title: Biosynt
layout: layouts/component.njk
eleventyNavigation:
  key: biosynt
  parent: components
tags: [actor_trait, actor_trait_auxiliary, component]
description: 'Konstrukt częściowo biologiczny, częściowo polikrystaliczny; lepszy niż człowiek, ale bez praw.'
---

# {{ title }}
## Krótki opis

{{ description }} 
" 
        );

        // When
        ReadActorCreationComponent actual = ReadActorCreationComponentFactory.CreateSingleAux(record);

        // Expected
        ReadActorCreationComponent expected = new ReadActorCreationComponent(
            Identifier: "biosynt",
            Title: "Biosynt",
            Description: "'Konstrukt częściowo biologiczny, częściowo polikrystaliczny; lepszy niż człowiek, ale bez praw.'",
            ComponentType: ReadActorCreationComponentDefaults.AuxComponentType,
            Body: record.Content
        );

        // Then
        // WARNING - we do not check Body equality!
        Assert.That(expected, Is.EqualTo(actual) );
    }
    
    
}
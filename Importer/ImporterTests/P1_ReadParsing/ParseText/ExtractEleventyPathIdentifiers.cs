using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing.ParseText;

public class ExtractEleventyPathIdentifiers
{
    [TestCase("/home/ez-public/content/motives/lojalnosc-syntetycznych-intelektow/index.md", "lojalnosc-syntetycznych-intelektow")]
    public void Extracts_eleventy_identifier_from_well_formed_links(string path, string expected)
    {
        // Given
        // When
        var actual = IdentifierParser.ExtractFromEleventyPath(path);
        
        // Then
        Assert.That(expected, Is.EqualTo(actual) );
    }
}
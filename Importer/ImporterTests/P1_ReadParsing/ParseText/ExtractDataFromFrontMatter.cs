using System.Linq;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing.ParseText;

public class ExtractDataFromFrontMatter
{
    [Test]
    public void Extracts_FrontMatter_from_Document_if_present()
    {
        // Given
        const string partialDocument = """
                                       ---
                                       title: Arystokrata Aurum
                                       eleventyNavigation:  
                                         key: arystokrata-aurum
                                         parent: components  
                                       ---
                                       # {{ title }}  
                                       ## Krótki opis  

                                       {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                                       """;
        // When
        var actual = FrontMatterParser.ExtractFrontMatter(partialDocument);

        // Then
        const string expected = """
                                title: Arystokrata Aurum
                                eleventyNavigation:  
                                  key: arystokrata-aurum
                                  parent: components
                                """;

        Assert.That(expected, Is.EqualTo(actual) );
    }

    [Test]
    public void Extracts_null_from_Document_if_no_FrontMatter_three_dashes_start()
    {
        // Given
        const string partialDocument = """

                                       title: Arystokrata Aurum
                                       eleventyNavigation:  
                                         key: arystokrata-aurum
                                         parent: components  
                                       ---
                                       # {{ title }}  
                                       ## Krótki opis  

                                       {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                                       """;
        // When
        var actual = FrontMatterParser.ExtractFrontMatter(partialDocument);

        // Then
        const string expected = null;

        Assert.That(expected, Is.EqualTo(actual) );
    }
    
    [Test]
    public void Extracts_single_key_from_FrontMatter_if_present_in_FrontMatter()
    {
        // Given
        var partialDocument = """
                              ---
                              title: Arystokrata Aurum
                              eleventyNavigation:  
                                key: arystokrata-aurum
                                parent: components  
                              ---
                              # {{ title }}  
                              ## Krótki opis  

                              {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                              """;
        // When
        var actual = FrontMatterParser.ExtractSingleKey("key", partialDocument);

        // Then
        const string expected = @"arystokrata-aurum";

        Assert.That(expected, Is.EqualTo(actual) );
    }
    
    [Test]
    public void Extracts_null_from_FrontMatter_if_key_not_present_in_FrontMatter()
    {
        // Given
        var partialDocument = """
                              ---
                              title: Arystokrata Aurum
                              eleventyNavigation:  
                                parent: components  
                              ---
                              # {{ title }}  
                              ## Krótki opis  

                              {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                              """;
        // When
        var actual = FrontMatterParser.ExtractSingleKey("key", partialDocument);

        // Then
        const string expected = null;

        Assert.That(expected, Is.EqualTo(actual) );
    }
}
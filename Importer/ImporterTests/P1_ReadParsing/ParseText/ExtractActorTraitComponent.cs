using System.Linq;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    public class ExtractActorTraitComponent
    {
        [Test]
        public void Extracts_title_from_actor_Archetype_if_present()
        {
            // Given
            string startOfArchetype = """
                                      ---
                                      title: Arystokrata Aurum
                                      layout: layouts/component.njk  
                                      eleventyNavigation:  
                                        key: arystokrata-aurum
                                        parent: components  
                                      tags: [actor_trait, actor_trait_archetype, component]  
                                      description: 'Mag jednego z Rodów Aurum, połączony sentisiecią z Rodem, terenem i innymi ze swego Rodu.'
                                      image: 'actor-archetypes/arystokrata-aurum/arystokrata-aurum-face.png'
                                      ---

                                      # {{ title }}  
                                      ## Krótki opis  

                                      {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                                      {{ description }}  

                                      ## Opis  
                                      ### 1. Tagi  

                                      archetyp, astoria, aurum

                                      """;
            // When
            string actual = FrontMatterParser.ExtractTitle(startOfArchetype);

            // Then
            const string expected = "Arystokrata Aurum";

            Assert.That(expected, Is.EqualTo(actual) );
        }
        
        [Test]
        public void Extracts_identifier_from_actor_Archetype_if_present()
        {
            // Given
            string startOfArchetype = """
                                      ---
                                      title: Arystokrata Aurum
                                      layout: layouts/component.njk  
                                      eleventyNavigation:  
                                        key: arystokrata-aurum
                                        parent: components  
                                      tags: [actor_trait, actor_trait_archetype, component]  
                                      description: 'Mag jednego z Rodów Aurum, połączony sentisiecią z Rodem, terenem i innymi ze swego Rodu.'
                                      image: 'actor-archetypes/arystokrata-aurum/arystokrata-aurum-face.png'
                                      ---

                                      # {{ title }}  
                                      ## Krótki opis  

                                      {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                                      {{ description }}  

                                      ## Opis  
                                      ### 1. Tagi  

                                      archetyp, astoria, aurum

                                      """;
            // When
            string actual = FrontMatterParser.ExtractKey(startOfArchetype);

            // Then
            const string expected = "arystokrata-aurum";

            Assert.That(expected, Is.EqualTo(actual) );
        }
        
        [Test]
        public void Extracts_description_from_actor_Archetype_if_present()
        {
            // Given
            string startOfArchetype = """
                                      ---
                                      title: Arystokrata Aurum
                                      layout: layouts/component.njk  
                                      eleventyNavigation:  
                                        key: arystokrata-aurum
                                        parent: components  
                                      tags: [actor_trait, actor_trait_archetype, component]  
                                      description: 'Mag jednego z Rodów Aurum, połączony sentisiecią z Rodem, terenem i innymi ze swego Rodu.'
                                      image: 'actor-archetypes/arystokrata-aurum/arystokrata-aurum-face.png'
                                      ---

                                      # {{ title }}  
                                      ## Krótki opis  

                                      {% image "./240912-aurum-aristocrat-archetype.png", "[Terminus]" %}

                                      {{ description }}  

                                      ## Opis  
                                      ### 1. Tagi  

                                      archetyp, astoria, aurum

                                      """;
            // When
            string actual = FrontMatterParser.ExtractDescription(startOfArchetype);

            // Then
            const string expected = "'Mag jednego z Rodów Aurum, połączony sentisiecią z Rodem, terenem i innymi ze swego Rodu.'";

            Assert.That(expected, Is.EqualTo(actual));
        }

    }
}
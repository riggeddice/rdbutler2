## Metadane

* title: "ROS - Adrienne, a new recruit"
* threads: ratownicy-ostatniej-szansy
* gm: żółw
* players: kić, BloodyWind, HelenDeer

## Kontynuacja
### Kampanijna

* [230113 - ROS - Adrienne, a new recruit](230113-ros-adrienne-a-new-recruit)

### Chronologiczna

* [230113 - ROS - Adrienne, a new recruit](230113-ros-adrienne-a-new-recruit)

## Plan sesji
### Theme & Vision

* Ludzie czy dobytek?
    * Ludzie na pokładzie jednostki są zagrożeni przez przemyt. Ale bez przemytu Supervising Serpent zostanie sprzedany.
* Pierwsza prosta sesja dla nowych graczy
    * Konflikt moralny: 
        * przemyt CZY niewola (dla NICH) -> (Tristan + Talia: przemyt. Niferus: nie czynić zła WIĘC niewola)
        * pomóc im odbudować i naprawić (kosztem naszych zasobów i niezadowolenia) CZY tylko co trzeba -> (Niferus: pomóc, Talia + Tristan: nieważne)
        * legalne działanie i chwała dla noktian CZY troszkę lepiej dla tej załogi (dla NAS) -> (Tristan: chwała, Talia + Niferus: załoga)
    * Zakres konfliktów:
        * obowiązkowo schowany kot w zakamarkach
        * obowiązkowy przerażony szmugler z działem laserowym
        * spowolnienie rotacji jednostki
        * znalezienie wszystkich przemycanych rzeczy?
        * znalezienie tych groźnych?
        * pokonanie pnączy neikatiańskich z silników?
        * statek jest w bardzo kiepskim stanie; environmentals.
        * Iga prosi o radę
* DARK FUTURE
    * Supervising Serpent się rozpadnie
    * Supervising Serpent wpadnie pod kontrolę piratów

### Co się stanie (what will happen)

.
* S00: SETUP: 
    * zapoznanie się z Adrianną (jestem zaszczycona mogąc z Wami pracować) - Adrianna CHCE BYĆ PRZYJACIÓŁKĄ. Tristan się odwraca. Talia nie mówi. Niferus tłumaczy.
* S01: Supervising Serpent; dotarliście. Statek ma dużą rotację; coś nie działa z silnikami. Pali paliwo. Nie utrzyma.
* S02: 

### Sukces graczy (when you win)

* disable TAI

## Sesja - analiza

### Fiszki
#### Fiszki S&R

* Napoleon Myszogłów
    * (ENCAO:  +0--- |Prawi wszystkim morały;;;;Bardzo lubi być w środku uwagi| VALS: Universalism, Conformity| DRIVE: Urażony szacunek)
    * atarienin: "Co zrobisz to dostaniesz"
    * kadencja: smerf ważniak
    * "Jesteście w stanie osiągnąć coś lepszego niż macie, tylko musicie się wykazać i udowodnić, że można Wam ufać"
    * SPEC: ?
* Tristan Andrait: (ENCAO:  0+--+ |Nie toleruje spokoju i nudy; wściekły, na siłowni| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis)
    * dekadianin: "jest tylko starcie i walka"
    * kadencja: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny
    * "Poświęć więcej energii na pracę a mniej na gadanie, ok?"
    * SPEC: walka, mięsień
* Niferus Sentriak
    * (ENCAO:  -0++0 |Wszystko rozwiążę teorią;;Przedsiębiorczy i pomysłowa| VALS: Achievement, Face| DRIVE: Legacy of helping)
    * klarkartianin: "przeszłość nie ma znaczenia, ważne co możemy zrobić"
    * kadencja: Chris Voss 'late night DJ voice'
    * "Wszystko rozwiążemy, to kwestia czasu"
* Talia Irris
    * (ENCAO:  0--0+ |lubi jak się jej schlebia;;Spontaniczna;;Przesądna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół)
    * savaranka: "słabość jednego z nas to słabość nas wszystkich"
    * kadencja: nostalgiczna savaranka; 
    * "Nie bądź słabym ogniwem"; TRAKTOWANA JAK MASKOTKA
    * SPEC: science / medical / space
* 1 synt

TRANSPLANT:

* Adrianna Kastir
    * ENCAO:  +-0-0 |Beztroska i nieco naiwna;;Sarkastyczna i złośliwa | VALS: Universalism, Conformity | DRIVE: Rule of Cool
    * atarienka, OBSERWATORKA
    * kadencja: gadatliwa, ma zawsze rację, chce pomóc
    * "My, dziewczyny musimy trzymać się razem", "Nie byłoby fajnie gdyby...", "Razem osiągnięmy więcej!!!"

#### Fiszki Nadzorczej Żmiji

* Iga Komczirp
    * ENCAO:  0+-0- |Trzeba ciężko pracować;;Łatwa do zniechęcenia| VALS: Power, Humility | DRIVE: Impress the family
    * (kapitan), atarienka: "dołączę do elity!", 22 lata
    * "ugryzłam za dużo i wszystko wyszło mi spod kontroli" "ok, jeśli się na to nie zgodzę to nie mam statku!" "nie stać mnie na dobrą załogę"
    * kadencja: jestem duża i poważna
* Szymon Wilczek
    * ENCAO:  --+-0 |Bezkompromisowy;;Kontemplacyjny;;Demotywujący| VALS: Security, Family | DRIVE: Pieniądze dla Siostry
    * (inżynier ORAZ szmugler) atarienin "kto nie próbuje nigdy nie wygrywa"
    * "zawarłem pakt z diabłem dla ratowania siostry"
    * kadencja: narzeka ale jest twardy
* Tola Araya
    * ENCAO:  0-+00 |Niemożliwa do ruszenia;;Przewidująca, trudny do zaskoczenia| VALS: Self-direction, Universalism | DRIVE: Save the hibernated, Loyalty
    * (eks-marine), 64 lata
    * "tyle w życiu zrobiłam rzeczy, że ZALEŻY mi na transporcie tych ludzi"
    * kadencja: na boku
* Bartek Milkin
    * (dzieciak z kotami)
* Tymon Perszak
    * ENCAO:  -00+- |Wycieńczony gdy musi się socjalizować;;Osoba starej daty | VALS: Stimulation >> Universalism | DRIVE: Red Queen Race
    * (pilot)
    * "ja już chcę przejść na emeryturę..." (zawodzi powoli) "gdy ja byłem młody to spotkałem TAKIE asteroidy..."
    * kadencja: tak tak tak, oczywiście, rozedrgany, starszy zagubiony człowiek

### Struktura Nadzorczej Żmiji

* Rozkład
    * załoga: 17 (6 to szmuglerzy)
    * zewnętrze
        * lekkie uzbrojenie anty-śmieci kosmiczne
        * cztery śluzy + śluza awaryjna + otwierana ładownia
        * silniki umożliwiające podróż pomiędzy układami
        * okno na mostku ułatwiające nawigację i manwery w porcie
    * wewnętrzne elementy
        * mostek + komunikacja + nawigacja
        * AI Core (klasa TAI: Semla lub Persefona)
        * kabiny załogi; dzielone według układów rodzinnych
		* pomieszczenia z hibernatorami
        * life support 
		* stacja medyczna (na długich trasach trzeba być samowystarczalnym) 
        * inżynieria + generatory memoriam
        * reaktor + baterie
        * magazyny wody + komponentów potrzebnych (paliwa, jedzenia itp)
        * mesa i rozrywka
* Aspekty
    * mnóstwo zakamarków, niebieskie światło
    * rozpadająca się konstrukcja (environmental), fragmenty nieużywane, pojazd za duży na wykorzystanie
    * ukryta broń, ukryte narkotyki, ukryte neuroobroże, ukryty materiał wybuchowy

### Scena Zero - impl

* Kić -> Łucja
* Vvind -> Aster
* Helen -> Hel

Noctians. You are experienced professionals (~40). You are experts. You are rescuers. Engineers, space mobility ops, doctors, firemen, fight & shoot. 3+3. Tristan, Talia, Niferus.

Your commander, Napoleon, called you to meet. Daily meeting. All 6 of you. And he comes to you with a young, spirited girl. Napoleon presents a girl. 

* Adrienne: "I have finished my university with best possible grades!"

Łucja, Aster, Hel and Talia are talking with Napoleon:

* T: ... (smile and small no-no headshake, with the rozbrajający uśmiech)
* N: Come on, you haven't SEEN her in action
* A: you neither
* N: I had. I mean... she had SIMULATIONS.
* A: where did you find her?
* N: This is complicated. (doesnt want to speak about it)
* Ł: who the hell made you take her? No sane aspiring youngster will come to this place to work with US of all people. WHo hates her or what did she do?
* N: This... not really... (REALLY doesnt want to speak about it)
* A: You better find some nice argument; I am not putting myself in the same ship as her.
* N: You are really her only chance. This I can say. 
* H: I can take her on a ship if she holds a cat toy, not a gun
* N: Deal. No guns for her. We will load her _power suit_ with medical, double armour, assistance...
* Ł: DOes she have ANY medical training whatsoever?
* N: ...only medical training and spacewalk... and basic fighting potential. Basic. But she can carry things.
* Ł: That's her use for now...
* H: Maybe some cleaning of litterboxes...
* A: Unless I know what she wants to achieve here, I am not putting myself on a mission. I will sit here and see her die.

Tr Z (respect and competence) +3:

* X: 
    * Napoleon: "sorry, we HAVE to take her. That's no choice. But I want her to go to you because then she has a chance to survive."
    * Ł: so we are babysitters basically
    * N: yes and no. She really has potential. She is no random admiral's daughter, ok?
* Vz: 
    * N: look, she is a pariah. She is like you, in a way. And it's not her fault. Her parents mingled with the Syndicate. And we had to move her somewhere noone can get to her. But she had a good career. She had potential. So this is only way for her to continue with potential.
    * Ł: you could have told us more...
    * N: no, I can't!
    * Ł: you gave a puppy as a surprise!
    * N: I can't tell those things to you. Those are (silent) confidential.
    * A: how YOU got involved in that?
    * N: (a bit hurt) I am your commander, ok? I have a clearance. You don't... so I kinda broke NDA so please be silent.
    * A: still not an answer
* V:
    * N: my girlfriend is her friend. So I was able to take her. Adrienne is competent. I mean, has potential.
    * Ł: you should have told us you want a favor
    * N: it goes like this: someone might recognize her. You are from Noctis. Noone interacts with you anyway. 
    * A: thanks. (Talia is smiling. She LIKES this fact.)
    * N: so you know, she has a... 
    * H: how about giving her a trial?
    * Ł: we can take her IF she proves herself
    * H: if she survives
    * N: she HAS to survive!
    * Ł: we could negotiate something because we give her a chance.
    * A: let's test her
    * Tr: (offscreen) I can fight her.
    * T: (no-no)
    * A: she can do spacewalking and medical. I would start from testing those. I want to see her in danger, how she reacts. And Tristan will protect her.

TEST ON THE FIELD. 

Hel is specifically devious. She is one not to speak very much, but she is devious. She is going to create a testing plan: we take a derelict ship and create a typical scenario for Adrienne to test her mettle.

We test:

* morality choices, hard decisions
* how she reacts in stressful, risky situations
* decision making
* we want to put her at risk (but not terminal)

Imagine - we have a spaceship. There is a problem. Reactor is leaking. General danger to the ship. There are people, some are wounded. Also, ship is under rotation. 

What is her mission: go there, administer medical stuff to people. Triage, save people in the worst shape, make sure nobody dies. And we add fire to the equation.

* morality choices, hard decisions
    * two creature stuck. One is our cat (Rosco, important to us), other one is a random wounded guy. One will burn. Whom will she save.
    * will she risk herself - go beyond being stupid - to save the other
* we survived because we are reliable and we trust each other.
    * watch this space for 2-3 hours and make it REALLY boring. Then high adrenaline stuff and then REALLY BORING 2-3 hours again. Will Talia manage to slip past her?

Let the test commence. Hel is going to create a properly devious plan:

Tr Z (we do it together) + 5Og ("third party: saboteur") +3:

* Og: there will be a minor sabotage of some type during the scenario
* X: there is some grumbling that "the special noctian unit is using up resources for fun"
* V: success, the test can be performed
* Vr: Adrienne will be under proper test, as if it was a true spaceship
* V: Adrienne will be PROPERLY evaluated under test and it will feel so real for her. And we know where can we use her.

### Sesja Właściwa - impl

The derelict ship we are going to save (as in, test Adrienne in) is called "Starlit Journey". And that is the proof it is not a commercial ship. It is an irregular structure, YOU know the plan, SHE doesn't.

You saw Adrienne. She looks completely different. White hair, pale skin, grey eyes... she looks like a parody of Talia. Adrienne (its CAMOUFLAGE - others wont know its me). Bone structure doesnt match. 

* Łucja comes to Adrienne. "you know we are a rescue ship not military. We want to be seen."
* Ad: but high commander ordered me to look like a Noctian
* T: high commander? (smile)
* Ad: YOU CAN TALK!
* Ł: shocking, I know
* H: you won't hear next word from her for half a year
* A: what a waste...
* Ad: high commander Napoleon
* Niferus, silently: she desperately tries to fit in.
* Ł: we all know how this ends
* N: you are right. We need to help her.
* T: oh hell no. You stupid?
* N: we have all been there...
* H: been where?
* N: you know, we all tend to try to make a difference, to fit somewhere...
* As: let's go on our mission and see how it ends.

You see the ship under rotation. The "Starlit Journey". Adrienne looks at it with small trepidation. You all see that ship is not only rotating but also has some space debris.

* Adrienne: "What should I do now?" -> Hel, pleadingly
* Tristan: You tell us. You are experienced and had good grades, right?
* Adrienne: ... (looks pleadingly at Hel)
* Hel: (looks away)
* Ł: what do you want to do?
* Ad: if the ship is under rotation we should stabilize it UNLESS it will shatter the armour, right? So we should either make a spacewalk, or use a nanofiber rope.
* As: I don't think rope is a good idea here.
* Ad: why?
* As: Too fast.
* Tr: he is saying you will shatter the ship or damage our ship. (looks at Adrienne with disappointment number 7)
* As: other ideas?
* Ł: you know, stopping the rotation is a good idea but if it keeps rotating the line is a huge spool and wrap around itself. So you kind of want to stop it before netting it.
* Ad: Temporary thrusters!

Adrienne makes a proper scan and collects the data, supervision of Tristan. And Niferus makes his own scan. Niferus likes scanning things.

Tr (niepełny) Z (good systems) + 2 + 3Og (stres):

* V: Adrienne localised what was needed - integral things etc.
* Vr: Adrienne found something interesting
    * Ad: Talia..? Could you look at it? There is something... structurally weak here.
    * Ta: (nodded) and made a gesture to everyone else
    * Ni: I've seen that too. This is something which will rip the ship. A bad seam.
* X: INEFFICIENCY
* Vz: Adrienne found a set of places where to put thrusters which require 10 thrusters while we would need only 6. She did good, but she isn't experienced and it shows.

Tristan STILL looks disappointed.

Talia points finger on herself, Adrienne and ship.

* As: you think it's a good idea?
* Ta: (nodded, smiled) (after a thought pointed at Hel too). "I will backup you two"
* As: works for me. We need to understand how we get to the ship.

Pinnace. Shuttle. Small ship for 2-3 people. Talia is piloting it, Hel and Adrienne are supposed to make a spacewalk, maneuver through debris and get to the place with thrusters.

For Adrienne only:

Tr + 2 + 3Og (stres):

* V: Adrienne managed to get to the surface with the thrusters. (as stated, not checking Hel)
* Og (stress): Adrienne mutters on open comms "ok, you can do it, you can prove them... just like tests..." FROM NOW ON Adrienne uses 4 Og.
    * Tristan smiles with contempt. He always smiles when someone is stressed
    * Niferus smiles like a granddad "those younglings..."
    * Talia watches like a hawk
* X: Adrienne is placing the thrusters and her foot went THROUGH the armour.
    * A: "Hel? Help? Something is wrong?"
    * H: What do you propose?
* V: Adrienne got herself together, though breathes very hard and started looking at the leg. It is kind of damaged. Potential of perforation.
    * As: (private) her suit is NOT perforated but CAN GET perforated if she frees herself. But the damage will not be strong; she will have enough time to stabilize unless she panics.
        * of course she is panicking, but trying not to show it
    * As -> Hel: watch and help but make her do it on her own, perfect opportunity.
    * A: Hel? Can you help me? (small voice) I think I am okay, but I'd like a second opinion...
    * Tristan: STOP ACTING WEAK.
    * H: it is okay but you need to be careful. Try to do it by yourself.
    * A: by myself? Haha, of course, girl power... after all... nothing like help...
* (+3Og) V: she got it off, very carefully and didnt damage anything. Then applied the nanites to patch the suit.
    * Ł: you're doing fine
    * As: well done
    * H: good work, today you can pet Rascal two times.
    * A: thank you... (stifling a small sob), stabilizes herself, deep breath and carries on. She is still unsteady.
    * Ł: (slow, deep medic voice) Adrienne, you're doing fine, stop, take a breath, think and relax a little.

Łucja stabilizes Adrienne.

Tr Z (she trusts you AND managed to do something and KNOWS NOW she doesnt have to rush) +3:

* Vz: Adrienne is stabilized (-3Og)
* Vz: Adrienne is soothed a bit (-1Og)

Thrusters are on a "Starlit Journey", you returned to the ship and thrusters were fired. The ship has stabilized. So the rotation is very slow. Manageable. Adrienne is like "PHEW". And she goes already: "That was not exactly like in the simulations." (seeing Tristan's face) "I mean I had those in simulations but the simul.. also... (looks at Tristan) ah doesnt matter."

Talia smiled. Silence and peace.

* As: first part is done. Let's go to the ship and find out what's going on. You, me, Hel. Girls power?
* Tr: sure, ASTER.

Now you got to the ship. Aster, Hel go seek in a ship and Adrienne is supposed to sift through blackbox and find the important data. Through LOTS of data.

ONE HOUR LATER:

Tr (niepełny) +2 +3Og:

* V: she was dilligent and found what was needed; results of simulation
* X: the lack of adrenaline got to her; she missed some small things
* X: she tunnelvisioned into data. She is SO TRYING TO FIND IT she kind of stopped looking around.

TRISTAN PREDATOR MASTER STALKER. (btw, stalking is ONE THING Tristan sucks at). He uses a heavy suit.

Tr (niepełny) +4:

* X: Tristan did not capture her using standard approach. He had to get SNEAKY
* X: Even if Tristan approached from TOP, Adrienne looked up hearing a strange sound

.

* Tr: "Maybe you would die 5 seconds later, but you would at least SEE what kills you." (disappoinment)
* Ad: (gets smaller)
* Tr: (pretending he had a reason to be here) "Found something?"
* As -> Tr: you have to admit it, that wasn't bad, right?
* Tr -> As: hrumpf. (lesser disappointment)
* Ni -> As, Tr: I kind of like her. Not that bad.
* Tr -> As, Ni: hrumpf.
* Ł -> all: she is a rookie, but not horribad
* Tr -> all: no, not horribad.
* Ł -> all: see, she likes her.
* Tr -> all: only because girl power includes Aster.
* As -> Tr: you can ask her for date, but after the mission.
* Ni -> Tr: I can synthesize you flowers
* Tr -> all: (random horrible noctian curse)

.

Adrienne is now on high adrenaline again, so she got placed on blackbox duty again. Good thing - there is no whining. She listens to orders.

So now we activate fire and cut communications. And direct Adrienne towards the area with wounded people where fire is coming. And everyone else is fighting fire.

Aster, master actor. He will give the command to Adrienne. Before fire starts: "Adrienne, go to living quarters and help the people there, we found them, they need medical help. Start, we will join you." AND THE FIRE STARTS. And communications is cut. And you have cameras. Because Talia was doing something after all.

Fire starts. Adrienne is worried. She is running to living quarters, fulfilling mission and damn stressed. And there are 8 "people". She needs to take care and triage them:

Tr (full) Z (she has resources) +3 +5Og (stress) +5Oy (saboteur):

* Og: Adrienne is NOT panicking but her vitals are in overdrive.
* Vr: Adrienne is properly triaging them.
* Vr: Adrienne is properly helping people, she already does stuff like a acceptable medical.
* Oy: explosion. Somewhere there was a dormant explosive in the wall. This means the ship starts some decompression. Usually the hatches would close. They are closing but that means if fire gets close to Adrienne she might get hurt. Like for real.

Tristan -> Adrienne. (+10Ob)

* V: Adrienne is acting properly like a medic should, under stress and duress, protecting.
* Oy: Another explosion. This explosion managed to dehermetize the place Adrienne and the wounded are AT.
* (+5Og) X: Adrienne is trying to move people and got close to the gap and activates a SOS beacon. And she got pulled to the gap. She kind of tries to stabilize the gap with her suit. Uses the nanites to patch the gap with her suit.
* V: ...and she succeeded. She is immobile, scared shitless but she "saved" them.

Aster reactivated the connection, Talia and Niferus are trying to stabilize the fire FOR REAL. Łucja is getting to Adrienne asap. Her suit is under extreme duress but will hold for several minutes.

Tristan -> Hel: "Get me a spare suit from the shuttle ASAP!"

Tr Z (you know every stupid map out there) +3:

* X: Hel was knocked by the piece of shattering ship; small crack in her suit but not breach. 
* Vr: Got a spare suit, properly prepared for Adrienne beforehand
* Vz: Hel entered the hall

.

* Tristan -> Adrienne: "THIS WAS STUPID! Brave, good idea but DAMN STUPID! You don't do it without one more person. You never stay alone. Never. You disobey orders but never stay alone. GOT IT?!"
* Adrienne: (small voice) yes...

While Adrienne is taking a new suit, Tristan is plugging the structure. 

Aster is trying to see the source and situation. Some deep scans and investigations.

Tr Z (you kinda have a set of powerful scanners) +3:

* V: Aster found safe escape route for EVERY team member. You can leave this ship.
* V: Aster found a PROOF there was a sabotage.
* X: Higher ups will not be very interested in punishing the saboteur. After all, nothing happened and this was a test. And nothing could happen, right?
* X: Due to political reasons and the fact noctians are really not liked noone wants to solve this except your team, Napoleon and some small amount of people who respect you and consider this to be wrong.
* V: Aster might not have a proof for the Station but has enough of a proof to pinpoint who (group) the saboteur belonged to. So you can get to that person.

You got safely to the main ship. Adrienne is like "IM SORRY WE DIDNT SAVE ALL!". Łucja: you did good, lots of mistakes, but did good. Hel: "you deserve at least three pets on Rascal ;-)" Aster passes Adrienne with madness and aggression on his face, putting hand on her shoulder "good job, kid" and goes to the far corridor of the ship...

WILL YOU ACCEPT HER FOR A REAL MISSION? -> she did well, not great, not trustworthy. Can be useful, but cant rely. But we can let her learn. Worth trying.

## Streszczenie

Noctian ROS Team got a new recruit - an atarien girl named Adrienne. They decided to test her on a derelict to check if she can be used without endangering the Team. She proved to be inexperienced, however a good addition - can work under stress, tries her best, can spacewalk and do medical stuff, however requires supervision. Someone from rescue station Allandea sabotaged the derelict and the test - endangering Adrienne and the Team. The Team saved Adrienne (who acted well under stress) and evacuated the derelict properly, having some proofs who was behind this.

## Progresja

* Adrianna Kastir: changed her looks to look more noctian than atarien (savaran-pattern). Got accepted in ROS team as an intern.

### Frakcji

* .

## Zasługi

* Łucja Neiser: tries to lead Adrianna so she makes sensible choices while on a derelict without telling her directly what to do. Stabilizes Adrienne's morale while Adrienne is panicking when her suit got endangered by the fracture in the hull.
* Aster Sarvinn: cynical and cold, doesn't want to endanger the team taking Adrianna - however accepts her as she can be of use for noctian team. Created a direct scenario checking Adrienne's mental prowess and acting-under-stress. Found indirect proofs who sabotaged the derelict and so who should pay for endangering Adrienne and the Team.
* Hel Otereien: devised a devious testing plan to test Adrianna on a derelict. Perfectly moves on a derelict to deliver a suit to endangered Adrienne.
* Napoleon Myszogłów: promotes Adrianna and asks for her to have a chance in the ROS noktian team. Made an impression on young Adrianna.
* Tristan Andrait: expert in looking very disappointed at Adrianna. Has some nice ribbing with Aster. To slow to get to Adrienne when the crisis was real.
* Niferus Sentriak: wants to give Adrienne a chance the most of all; afterwards expertly fights real fire on a derelict.
* Talia Irris: without speaking much, communicates well with the team and shows what should be done and how. Leads Adrianna through dangerous things by her actions. Secures Team from the shuttle as primary pilot.
* Adrianna Kastir: young woman having AMAZING GRADES who wanted to join ROS. Can do spacewalk and medical. Has some political problems issues in Orbiter. Masked herself as a noctian for EVERYONE EXCEPT NOCTIANS (looks like blackface). Can do her job as a ROS team member, however is inefficient. Talks WAY too much, however is generally chipper and brave. She is acting properly like a medic should, under stress and duress, protecting people.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Stacja Ratunkowa Allandea

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 521
* Dni: 3

## Metadane

* title: "Załoga Vishaera przeżyje"
* threads: grupa-wydzielona-serbinius
* motives: energia-sempitus, body-horror, heart-is-a-weakness, misja-ratunkowa, oczywiste-nieakceptowalne-rozwiazanie, grade-of-success, presja-czasu, ratownicy-kontra-ratowani, wezel-zrodlo-skazenia
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [230530 - Ziarno Kuratorów na Karnaxianie](230530-ziarno-kuratorow-na-karnaxianie)

### Chronologiczna

* [230530 - Ziarno Kuratorów na Karnaxianie](230530-ziarno-kuratorow-na-karnaxianie)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * Ely Eira "Never say Die"
        * "And never say die | Never lose hope | We'll be fighting forever | United as one "
        * "Never, (die) never, (die) never (die)"
    * (pasuje też: Virtual Intelligence 'Resurrection Ship')
    * -> Bardzo ciężko uszkodzony statek kosmiczny, anomalia, z ludźmi splątanymi by przetrwać (Sempitus). Grupa ratunkowa zgłosiła SOS.
* Opowieść o (Theme and vision): 
    * "Bohaterscy oficerowie statku zrobili co mogli by uratować wszystkich, ale poszli za daleko. Udało im się? Było warto?"
    * Cel: pokazać przyjaźć Klaudii i Martyna, link między nimi
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-sempitus: Eternal Ship. Statek który wpakował się w anomalną sytuację i się przekształcił. Ta jednostka nie powinna działać, ale wola uniemożliwia jej śmierć.
    * body-horror: ludzie na pokładzie Vishaer są technicznie terrorformami pragnącymi przetrwać; subtyp 'hydra' / 'feniks' na podstawie pleśni. Atakują i regenerują Vishaer i siebie.
    * heart-is-a-weakness: ludzie z SC Talikazer próbowali uratować AK Vishaer i spotkała ich straszliwa 'kara' za ich dobre serce
    * misja-ratunkowa: Serbinius ma za zadanie uratować Talikazer a może nawet Vishaer.
    * oczywiste-nieakceptowalne-rozwiazanie: eksterminacja załogi Vishaer oraz Talikazer, zniszczenie anomalii kosmicznej. Da się to zrobić i jest zgodne z prawem, ale Orbiter na to nie pójdzie.
    * ratownicy-kontra-ratowani: ofiary są agresywne, ale trzeba im pomóc lub ich zniszczyć. Trzeba odpiąć ich od źródła magicznego.
    * wezel-zrodlo-skazenia: anomalne serce Vishaer, amalgamat woli przetrwania i nadziei ludzkości, że z magią sobie poradzą.
    * grade-of-success: zniszczenie Vishaer - ratunek Talikazer - ratunek Vishaer. Można wieloetapowo do tego podejść i każda kolejna rzecz o jaką walczymy jest trudniejsza.
    * presja-czasu: Vishaer asymiluje Talikazer, z czasem coraz trudniej jest kogokolwiek uratować
* Detale
    * Crisis source: 
        * HEART: "Brak sprzętu, brak możliwości przetrwania Vishaera skłoniło kapitana (maga) do wyłączenia Memoriam i próbę uratowania wszystkich energią Eteru Nieskończonego."
        * VISIBLE: "Vishaer corrupts Talikazer and spreads the corruption. Vishaer wants to survive at all costs."
    * Delta future
        * DARK FUTURE:
            * chain 1: CORRUPTION: Talikazer i Vishaer zostają zniszczone
            * chain 2: TRUST: Anastazy i Helmut mają złamane morale; "ich trzeba zniszczyć"
            * chain 3: CIOTKA: Miranda nie życzy sobie by cokolwiek mogło się stać Anastazemu
        * LIGHT FUTURE: 
            * chain 1: CORRUPTION: uda się uratować załogę Talikazera i nawet część Vishaera
            * chain 2: TRUST: Anastazy i Helmut rzucają się sobie do gardeł. W obliczu zagrożenia Fabian próbuje jakoś
            * chain 3: CIOTKA: Miranda nic nie wie
    * Dilemma: brak

### Co się stało i co wiemy

Vishaer jest statkiem kosmicznym który wpadł w katastrofalne problemy. Daleko od cywilizacji, orbity, energii. Kapitan Vishaera wyłączył Memoriam próbując połączyć się z Eterem Nieskończonym by Vishaer przetrwał. Wynik... cóż, oczekiwany.

Gdy po paru latach Vishaer napotkała inna jednostka, Talikazer, to Talikazer próbował pomóc Vishaerowi. I sami wpadli w kłopoty. Na szczęście Talikazer wysłał SOS.

Serbinius - najbliższa jednostka - oddelegowana do pomocy Talikazerowi i Vishaerowi w opałach.

### Co się stanie (what will happen)

* F1: (W jakim stanie są te jednostki :: START)
    * "Helmut nic nie zmieniłeś w sprzęcie? Wiem że grzebałeś w sensorach!" (nie widać sensownych sygnałów życia)
    * "Yeah, postrzelamy" (Anastazy)
    * Insercja Anastazego i Martyna, pierwszy terrorform na statku (hydra-grzyb-energożer)
* F2: Ratujemy Talikazer i CO TU SIĘ STAŁO?
    * Terrorformy na Talikazerze, pomniejsza transformacja.
    * Odcinamy Talikazer od Vishaera. Martyn musi się tym zająć.
    * Anastazy i Helmut są za zniszczeniem Vishaera. Martyn 'jest szansa ich uratować i musimy zrozumieć.'
* CHAINS
    * Problemy i konflikty
        * (ranny Anastazy i Helmut, uszkodzenie Serbiniusa, Fabian ma problem)
        * (Anastazy i Helmut się żrą, Anastazy, Helmut vs Martyn i Klaudia)
        * (atak potworów, 'dead is alive', silniejszy stres)
        * (contamination)
    * Magia
        * (Klaudia / Martyn Skażeni, nie wszystkich da się uratować, konieczność operacji ratunkowych po Martyna/Klaudię)
        * (Transfer energii do Vishaera)
* Overall
    * stakes
        * relacja z Helmutem / Anastazym / Fabianem
        * uratowanie jak największej ilości zarażonych ofiar
    * opponent
        * Anomalia Kosmiczna Sempitus Vishaer
        * Problemy z morale na Serbiniusie
    * problem
        * część ludzi da się uratować ale KTÓRYCH?
        * how do you destroy THAT?

## Sesja - analiza

### Fiszki

.

#### Strony

.

### Scena Zero - impl

brak

### Sesja Właściwa - impl

Serbinius jest szybką korwetą ratunkową. Dostał przekierowanie przez sygnał SOS. Fabian tłumaczy co jest grane:

* Fabian: Mamy statek, to taki zwykły cywilny. Nazywa się Talikazer. I Talikazer widzi INNY statek w opałach, ten inny ma jakieś problemy. Kapitan Talikazera zrobił to co powinien i ruszył do pomocy tej innej jednostce.
* Klaudia: I sam wpadł w kłopoty?
* Fabian: Dokładnie
* Anastazy: To nie powinien się zabierać
* Fabian: Nie wiedział o tym. Nie wiedział co go czeka.
* Klaudia: Skąd miał wiedzieć?
* Fabian: Tak czy inaczej, możesz pomóc - pomagasz. Proste. Nie ma nic gorszego niż skończyć w pustce kosmosu bez możliwości powrotu do domu.
* Anastazy: Mogli być bardziej kompetentni.
* Fabian: Dlatego my tam lecimy. Po to jest Orbiter.
* Anastazy: To piraci czy...?
* Fabian: Nie wiem. SOS ze strony Semli. Coraz bardziej paniczne. Talikazer nie ma najlepszych detektorów czy systemów, my jednak podejdziemy do tego prawidłowo i poważnie.
* Klaudia: ...jak zawsze.
* Klaudia: Podała szczegóły?
* Fabian: Problemy ze skażeniem biologicznym.
* Anastazy: JA tam nie wchodzę...
* Fabian: Wchodzisz jako główny advancer. Masz servar, masz hermetyzację...
* Anastazy: (nieprzekonany) Tak jest...
* Fabian: Semla powiedziała... dała dziwne sygnały. Nie umiem ich zinterpretować. Dodaj fakt, że Talikazer miał pewne problemy z łącznością...
* Anastazy: Oszczędzali?
* Fabian: Oszczędzali.
* Klaudia: Bywa i tak.
* Martyn: Jakiej jednostce pomagał Talikazer?
* Fabian: Z raportu wynika, że nazywała się Vishaer
* Klaudia: Nie jest to jednostka jaką znamy?
* Fabian: Nie, nie mamy jej w banku. Możesz poszukać.
* Klaudia: Nie pomagaliśmy jej?
* Martyn: Ani jej nie pomagaliśmy ani nie była w okolicy. Bo by gdzieś tam była.
* Fabian: Ktoś się pomylił.
* Klaudia: Albo nie - nie wiem co gorsze.
* Fabian: Anomalia Kosmiczna?
* Martyn: Nie można wykluczyć.
* Klaudia: Musimy wykluczyć by móc wykluczyć.
* Anastazy: (przewrócił oczami) Mam nadzieję, że Helmut nie oszczędzał na sensorach
* Fabian: Helmut?
* Helmut: Nic co powinno wpłynąć negatywnie na jednostkę
* Anastazy: ZNOWU coś spieprzyłeś?
* Helmut: Nigdy nic nie spieprzyłem, wszystko jest w akceptowalnych parametrach. A Orbiter nie daje nam dość środków.
* Fabian: Uspokójcie się, jesteśmy oficerami Orbitera a nie... dziećmi w przedszkolu.

Klaudia chce zrozumieć co się dzieje - czy z raportu Talikazera coś wynika. Jaki sygnał SOS. Jaki transponder Vishaer. Co przekazuje Semla. Też analiza ruchu - czy zgłaszano że któryś statek wchodzi na teren Orbitera itp.

Tr +3:

* X: Talikazer jest dość starą jednostką. Nie jest bardzo odnowiony, nie jest też w świetnej formie. Kapitan robi co może. Ale i tak próbował pomóc jednostce w opałach. Zgodnie z danymi Talikazera nie powinno się stać nic bardzo dziwnego. Vishaer... są logi o tej jednostce. 5 lat temu. W okolicach Anomalii Kolapsu. Tamtejsze jednostki należące pod tamten obszar, ale ludzkie. (NIE WIADOMO kiedy Vishaer na tym terenie się pojawił + Talikazer nie mógł dużo przekazać bo jest stary)
* V: 
    * Talikazer jest jednostką leciwą, ale solidną. Kapitan inteligentnie dbał o surowce, sourcował je też m.in. z Anomalii Kolapsu. Ale robił to inteligentnie.
    * Vishaer ma kapitana który jest magiem. Kapitan Vishaera był zawsze osobą bardzo solidną i skrupulatną. Dbał o swoich. Vishaer, z danych Orbitera kilka lat temu, jest jednostką w dobrym stanie. Pełni rolę transportowca.
    * Semla d'Talikazer wysyłała bardzo dziwne sygnały. Biodetektory Talikazera nie działają. Po prostu. To, że na samym początku jej detektory wskazały nieprawidłowości w załodze Vishaera zostało uznane za akceptowalne odchyły (bo Talikazer też źle widzi swoją załogę). Ale Semla uznała, że doszło do _ZMIANY_ części jej załogi na pokładzie Vishaera. To się zdarzało (sensory). Ale Klaudia widzi, że tym razem było inaczej. Mamy coś anomalnego. Bo jest pewna regularność mimo uszkodzonych danych. I gdy załoga Talikazera - siły ratunkowe - wróciły na Talikazera, to się zaczęło rozprzestrzeniać po Talikazerze. Semla nie do końca widzi cokolwiek, więc wysłała rozpaczliwe SOS. Zajęło chwilę, zanim Semla została overridowana przez kapitana.
        * Wystarczyło to, by Serbinius został wysłany.
* Vr: Klaudia nie ma "obrazu", ale ma zbiór danych obocznych. To jakaś forma terromorfizacji. Ale nie ixiońskiej - servary są nieintegrowane. Stają się odporniejsi patrząc na dane. Patrząc, że wyłączyli polecenia Semli, to jest coś co nie odbiera im intelektu. Ostatnie dane Semli mówiły o transferze załogi pomiędzy Vishaerem i Talikazerem.
    * Klaudia przekazała dane Martynowi
    * Martyn: "Potrzebujemy danych, próbki biologicznej. Przygotuję... najlepsze, co mogę z perspektywy sterylnego pomieszczenia."
    * Klaudia: "Osobny kontener jako biolab, zewnętrzny."
    * Martyn: "Tak, tylko musimy zdobyć ochotnika z Talikazera lub Vishaera."
    * Klaudia: "Przymusowy ochotnik zadziała"

To jest najbardziej niebezpieczna część ratunku. Wiemy, że są jakieś osoby zakażone czymś. Nie wiemy co im się stało. I mamy dwie jednostki.

* Klaudia: Wezwijmy jeszcze jedną jednostkę na pomoc; tam się dzieje coś złego i nie możemy się rozpraszać. Rekomendacja z uwagi na bezpieczeństwo
* Fabian: Nie wiemy nawet czy coś tam się dzieje. To jest... wiemy.
* Klaudia: Wiemy że dzieje się COŚ ale nie wiemy CO. A jeśli potrzebujemy drugiej jednostki a ona 5h stąd? Musimy 5h ogarniać coś co może przekraczać nasze możliwości.
* Fabian: Nie mogę bez zbliżenia się i przeanalizowania sytuacji po prostu poprosić o wsparcie. Nie mam podstaw.
* Klaudia: Jest moim obowiązkiem podnieść ten temat.
* Fabian: Po ostatnim MUSIMY sobie poradzić.

Za jakąś 1h Serbinius będzie na miejscu, przy obu jednostkach.

Minęła 1h.

Serbinius zbliża się do dwóch połączonych jednostek (śluza). Vishaer wygląda jak gówno. Wygląda jak ruina. Ma sygnatury energii, ale... jak on lata? Klaudii od razu włączają się alarmy. Ale np. Vishaer ma wyraźnie zdehermetyzowany kadłub w paru miejscach. Statek jest w fatalnym stanie. Talikazer natomiast wygląda jak Talikazer powinien wyglądać. Acz jego sygnatura energii jest niższa. Nie wiadomo, czy to kwestia utrzymaniowa czy coś się stało.

* Anastazy: Wow, ale ruina.
* Klaudia: Nie mam pojęcia co się stało, wygląda dziwnie...
* Anastazy: Dokładnie tak wyobrażam sobie statek z potworami (wskazując na Vishaer). To jest jednostka z horroru.
* Klaudia: (zimno) NAWET tak nie myśl. Natychmiast przestań myśleć w ten sposób. Naćpaj się ale myśleć tak nie wolno.
* Anastazy: Hej, spoko (zaskoczony). To żart. Prawie.
* Klaudia: (zimno) NAWET tak nie żartuj.
* Anastazy: (żachnął się) (oddalił się).

Klaudia robi pełny skan obu jednostek. I wszystko przefiltrowuje przez sandboxa. Podejrzewa anomalię. Obszary ciepłe / zimne, bateria czujników, co się dzieje.

Tr Z (dane z Semli) +3:

* V: 
    * Obie jednostki operują na minimalnych zasobach energii. Talikazer mógłby na większych, ale nie robi tego. Ograniczył.
    * Atmosfera na Vishaer jest wyraźnie nieświeża. System podtrzymywania życia nie dał rady.
    * Biosygnatury obecne na obu jednostkach.
    * Vishaer ma ZA MAŁO WODY. Dużo za mało wody.
    * Vishaer jest dosłownie ruiną. Ta jednostka ledwo działa. Klaudia nie jest przekonana że działałaby bez magii.
    * Zarówno Talikazer jak i Vishaer nadal wysyłają generyczne SOS. Acz Vishaer wysyła je na krótkim dystansie, bo... nie ma jak inaczej. Zniszczenia.
        * Ten statek (Vishaer) wygląda jakby kilka lat leciał bez stoczni.

Klaudia sandboxuje swoją maszynę. Chce połączyć się z TAI Vishaera.

Tr+3:

* V: Po stronie Vishaera NIE MA Tai. To nie jest możliwe. Ale są sygnały które TAI Serbiniusa mogła odebrać. Klaudia już widzi bardzo anomalną jednostkę.
    * Klaudia ma wiadomość z Vishaera. Echo. Starszy mężczyzna w stroju kapitana na mostku.
        * "Tu Wolfgang Sępiarz, kapitan Vishaer. Mieliśmy katastrofalną awarię. Nie mamy jak wrócić do cywilizowanych terenów. Sos. Sos."
    * Następna wiadomość. Tym razem kapitan wygląda na bardziej delirycznego. Statek jest 'ciemny'.
        * "Tu Wolfgang Sępiarz, kapitan Vishaer. Nie mam... albo coś zrobię, albo wszyscy jesteśmy martwi. Informuję - wyłączam generatory Memoriam. Zrobię co mogę by moja załoga wróciła bezpiecznie. Spróbuję... (reszta nadziei w oczach)"
    * Vishaer posiada moduły kapsuł ratunkowych, lifesupport - i one wyglądają na "sprawne".

Jest to katastrofalnie zła sytuacja. Vishaer jest Anomalią Kosmiczną. Najpewniej na Vishaer nie ma nikogo kogo da się uratować. Ale Talikazer...

* Klaudia: Musimy rozłączyć statki. Szanse załogi na Talikazer... nieimponujące. Oryginalna załoga Vishaera... nie ma żadnych. Talikazer na Vishaer... ciężko.
* Fabian: Rozetniemy połączenie. Helmut, to Twoja robota.
* Anastazy: Możemy zestrzelić Vishaer?
* Fabian: (patrzy na Klaudię)
* Klaudia: Nie wiem.
* Martyn: Jeśli możemy uratować ludzi porwanych na tą jednostkę, trzeba spróbować.
* Klaudia: To raz. Dwa, mag próbował uratować swoją załogę. Zestrzelenie Vishaer idzie przeciw temu czemu ta sytuacja w ogóle powstała. Przeciw energii która tam jest. A energia jest uśpiona, ALE.
* Martyn: Możemy zamontować JAKIEKOLWIEK memoriam przy Vishaer? Cokolwiek? Coś co jakkolwiek pomoże? (w stronę Klaudii)
* Klaudia: Jeśli mamy... jest niby jeden, ale na jednostkę tego rozmiaru potrzeba więcej. Możemy ściągnąć statek który nam przywiezie generatory.
* Martyn: Tu jest presja czasu. Musimy działać TERAZ. 2h temu był SOS. 1h temu wyłączony. Talikazer i Vishaer 'pracują' nad załogą Talikazera. Musimy działać TERAZ.
* Fabian: Nie wyślę ludzi w ciemno.
* Martyn: Możesz wysłać w ciemno maga.
* Klaudia: Potencjalnie więcej niż jednego.
* Fabian: Bez Memoriam? Skażona jednostka?
* Martyn: Mało optymalne.
* Klaudia: Zdecydowanie NIE optymalne.
* Martyn: Jak inaczej ich możemy uratować?
* Anastazy: Nie patrzcie na mnie, gdyby to... ok, czy mogę założyć że to potwory i mogę strzelać?
* Klaudia: Nie.
* Martyn: Myśląc w ten sposób nie możesz wejść na pokład.
* Fabian: Ale _jak_ nie myśleć o nich jak o potworach?
* Klaudia: (nagranie maga)
* Fabian: (nic nie mówi chwilę) (wzdycha) Odepnijmy Talikazer i wpierw rozwiążmy Talikazer.
* Klaudia: Czy rozwiązanie jednego bez drugiego będzie możliwe, się okaże
* Klaudia: Jeśli chcemy uratować ludzi na Vishaer... będzie gorzej. Im dłużej tam są.
* Martyn: Może da się jakoś... zrobić miejsce innej insercji.
* Klaudia: Co masz na myśli?
* Martyn: Zrobić dziurę w poszyciu
* Klaudia: Coś co nam może pomóc - śluzy awaryjne. Jeśli mag się poświęcił by ratować swoją załogę, odpalenie śluzy wpisuje się w ten scenariusz.
* Martyn: Nie wiem, wydaj rozkaz ich TAI by otworzyła śluzy?
* Klaudia: Vishaer nie ma już TAI.
* Martyn: ...
* Fabian: Pięknie. Helmut, jak Ci idzie?
* Helmut: Wychodzę w kosmos.
* Fabian: Anastazy, osłaniasz go.
* Anastazy: Tak jest.

Klaudia wie, że po drugiej stronie nie ma TAI. Ale jednocześnie _coś_ tam jest i _coś_ wysłało jej sygnał. A kapitan poświęcił się by ratować załogę. Więc MOŻE statek odpowie jakby miał TAI. Klaudia zrobiła takie założenie i wysłała wiadomość "jesteśmy siłami ratunkowymi, otwórz śluzy ewakuacyjne". 

* V: COŚ odpowiedziało. Śluzy się 'rozświetliły' na możliwość otwarcia. Vishaer używa tyci więcej energii niż wcześniej. Klaudia zobaczyła coś ciekawego - większość wody, lapisu itp chroni kapsuły ratunkowe.

Klaudia -> Orbiter. KIEDY zniknął Vishaer. Dostała szybką odpowiedź. Ostatnia sygnatura - 3 lata temu. Ale, co ważne, transportował komory stazy. I większość energii i rozlokowanie lapisu itp wskazuje właśnie na próbę ekranowania komór stazy.

Klaudia ma zamiar odegrać scenkę rodzajową wyciągania ludzi ze statku. W kontakcie z 'TAI' Vishaera, wydaje zdalnie polecenia. Biorą jeden Memoriam dla własnej ochrony, personalny, by się za bardzo nie skazić. Scenka z gatunku "statek - znaleziony, siły s&r ewakuują załogę, statek już nie musi jej trzymać przy życiu". Klaudia chce nastawić Pryzmat pozytywnie do insercji. "Jesteśmy oddziałem ratunkowym". SKORO Pryzmat ustawiony na 'ratujemy załogę' to uratujmy ją XD. Co z nimi potem - inna historia.

Klaudia doskonale ROZUMIE co tam się dzieje i ROZUMIE działania. Mimo braku Memoriam załoga może być ratowalna, bo energia mogła Odpowiedzieć.

Tr+3:

* X: To, że Energia odpowiada nie oznacza, że terrorformy odpowiedzą. Oni są za daleko. Trzeba ich wymanewrować lub zniszczyć.
* V: Pryzmat wspiera Klaudię. Statek chce uratować swoją załogę. To jest anomalna ruina, ale próbuje. 
    * Lepiej tam nie czarować.
* X: Ruina AK Vishaer nie działa. Działa czystą energią Sempitus. Roboty, wspomagania... this is dead. Corrupted and destroyed. Niestety, wszystko musimy mieć swoje.
    * ALE Vishaer pozwoli się uszkodzić Klaudii i Martynowi.
    * Wolfgang, kolejny sygnał z echa: "Sami ochotnicy... zostaną na straży. Vishaer powróci i uratujemy kogo się da uratować. Niech bogowie będą z nami... wyłączcie generatory."
* Vr: Vishaer jest skłonny 'uwolnić' osoby z Talikazera - jego załoga jest uratowana, więc nie musi mieć jedzenia. Więc da się ich wyciągnąć.

Klaudia i Martyn rekwirują pintkę, mobilne laboratorium, wzmacnia się ich servary.

* Fabian: Zgodnie z regulacjami mamy PRAWO zostawić Vishaer. (mówi to niechętnie)
* Klaudia: Jeśli go zostawimy, znajdzie kolejny statek by się żywić.
* Fabian: Wezwiemy korwetę rakietową. Możemy. Talikazer jest naszą odpowiedzialnością.
* Martyn: Nie zostawimy ich. Nie, jeśli można ich uratować.
* Klaudia: A można. Jest taka szansa. Nie po to jesteśmy w S&R, by rezygnować.
* Fabian: Nie mogę Wam wydać takiego rozkazu, by tam iść.
* Klaudia: Kto mówił o rozkazie?
* Martyn: (uśmiech) Nie nasze pierwsze rodeo, kapitanie. (po chwili spoważniał) Choć to jest z tych ciekawszych.
* Klaudia: To prawda.
* Helmut: Jednostki rozpięte, ale dalej mamy informacje o Skażeniu na pokładzie Talikazera. I jest tam też jakiś... terrorform.
* Fabian: Zajmiemy się tym.
* Anastazy: Bez maga?
* Fabian: Jeżeli pójdą TAM nie pójdą TU.
* Anastazy: Kapitanie, to jest zbyt niebezpieczne. Zwłaszcza, jeśli nie możemy po prostu postrzelać.
* Helmut: Nie wierzę że to mówię, ale zgadzam się z Anastazym. Jesteśmy we dwójkę, wchodzimy na nieznaną jednostkę i... 
* Fabian: To jest nieregulaminowe, wszystko w tej sprawie jest nieregulaminowe.
* Klaudia: Żaden regulamin nie przetrwał konfrontacji z rzeczywistością.

Klaudia widzi, że Fabian autentycznie nie wie co robić. Anastazy się boi, Helmut też. To nie jest operacja do której przywykli. A nie ma z nimi ani Klaudii ani Martyna. Odcięcie połączenia ze statkiem macierzystym oznacza, że energia Sempitus i Pryzmat 'support, survive, preserve' przestaje działać w pełni na terrorformy i istoty Talikazer.

Klaudia robi MÓWKĘ. To są terrorformy, tak, ale powstały INACZEJ. One nie chcą NISZCZYĆ. One nie chcą siać terroru (mimo nazwy). Oni chcą pomóc i ratować. Więc myśl o nich w odpowiedni sposób i zajmij się tym po co przyszedłeś. Współpracuj z nimi, wydawaj im rozkazy.

ExZ (zaufanie Fabiana do Klaudii) +3:

* V: 
    * Fabian: Dobrze, ja tam pójdę. Spróbuję wykorzystać terrorformy. Nie, Skażonych oficerów. Anastazy, zostań na Serbiniusie i koordynuj. Helmut, ze mną. Postawimy Semlę.
    * Helmut: Panie kapitanie... (obawa)
    * Fabian: Porucznik Stryk jest jaka jest, ale nigdy - NIGDY - się nie pomyliła w sprawach anomalnych. Gdy nic się nie dało zrobić, ewakuowała oddział z Karnaxiana. Musimy mieć pozytywne myśli.
    * Helmut: Panie kapitanie, ja nie wiem czy umiem. To... 
    * Fabian: To nie są potwory.
    * Klaudia: Ci oficerowie poszli na ochotnika żeby utrzymywać swoją załogę przy życiu. I najwyraźniej jedyne co mogli zrobić to wyłączyć generatory memoriam by to osiągnąć.
    * Fabian: Morale i umiejętność zachowania wiary w ich człowieczeństwo jest kluczowe w tej operacji. Jeśli nie możecie, idę sam.
    * Helmut: To zostanę, panie kapitanie.
    * Anastazy: Będę osłaniał.
    * Fabian: Absolutnie nie.
    * Anastazy: Z całym szacunkiem, ale... jesteś sam na statku, będę w próżni. Chodzi o to, by... jakby coś poszło nie tak, cokolwiek - by kapitan nie był tam całkiem sam. To Skażenie magiczne, tak?
    * Klaudia: Nie jest to zły pomysł. (podaje dystans próżni).
    * Helmut: Przyjąłem, ja będę koordynował z Serbiniusa i wesprę Klaudię i Martyna w sprawie sprzętu.
    * Klaudia: Ma sens.

Fabian FAKTYCZNIE próbuje opanować sytuację na Talikazerze. Szczerze wierzy że 'potwory' na Talikazerze to oficerowie. To jego równi. Że chcą dobrze. Da tą szansę. I spróbuje ich koordynować i wejść na Talikazer jakby to była normalna jednostka mimo specyfiki lokacji.

Ex Z (pełne zaufanie do Klaudii + ogólna optymistyczna natura) +3:

* Vr: Fabian mimo szoku i horroru na pokładzie Talikazera zauważył, że Paradygmat oficera Orbitera zadziałał.
    * Skażony lifesupport
    * Zarodniki w powietrzu
    * Ludzie z naroślami pasożytującymi
    * Terrorformy
    * A Fabian zachowuje się jakby byli normalnymi ludźmi, informuje że działa w imieniu Orbitera, wszyscy będą bezpieczni, mają odpalić Semlę na pełnej mocy, wrócić energii itp.
* Vz: Niektóre terrorformy były bardziej agresywne. Ale jest Pryzmat jaki jest. I Fabian nie udaje - on naprawdę wierzy w prymat ludzkości.
    * Fabian kazał ustawić ludzi do kapsuł ewakuacyjnych (izolacja itp), nawet to wyjaśniając.
    * Wraz ze zmniejszającą się populacją ludzi, terrorformy stają się groźniejsze. Fabian się ewakuuje.
    * Ludzie z Talikazera zostali ewakuowani i uratowani. Mają jakieś 10h w kapsułach. To jest dość, bo leci jednostka wsparcia.
    * Fabian jest w takim ciągu adrenalinowym... ale dał radę.
    * +5 pktów szacunku do Fabiana od Helmuta i Anastazego. Od Martyna i Klaudii też. Zaimponował im szczur.
    * Jeden 'terrorform' wyszedł przez śluzę w próżnię. Długo przetrwał, ale w końcu się zniszczył. Pozostałe revertowały do potworności.

Zostali do uratowania ludzie w kapsułach ORAZ porwana załoga Talikazera. Którzy będą znajdowali się bliżej recyklera. Trzeba czymś nakarmić ludzi w kapsułach...

Vishaer się zbliża (Martyn i Klaudia lecą pintką). Vishaer ma niestabilny teren i wygląda skrajnie niebezpiecznie.

* Martyn: Zachowaj optymizm.
* Klaudia: Jedynym celem życiowym statku jest uratowanie jego załogi co właśnie zamierzamy zrobić, nie?
* Martyn: Każdemu komu da się pomóc, pomożemy. (lekki uśmiech)

Martyn nie pozwolił na zadokowanie, przeprowadził Klaudię oraz sprzęt z pintki na Vishaer. Kadłub Vishaera jest w koszmarnym stanie. Poszycie... widać braki i problemy. Ale są lekkie światła na śluzach. Martyn i Klaudia weszli przez śluzę. Servary raportują powietrze skażone, teren nie nadaje się do życia. Agent biologiczny nieznany. Klaudia czuje dudnienie energii magicznej w głowie. Jej visiat z trudem filtruje. Martyn ma to samo.

Klaudia ich ogłasza jako ratowników, wpisuje kody do martwej konsoli, dąży do przesłania transportu chorych i rannych i ofiar do śluzy. Wszystko to co powinna zainicjować by zaczęło się dziać zgodnie z ich działaniami. Klaudia UDAJE, że wszystko jest normalne. Im normalniej tym większa szansa że Vishaer pomoże.

Tr+3:

* V: Niesprawne drzwi się spróbowały otworzyć, nie dały rady. Klaudia servarem je przesunęła. 
    * Korytarz pokryty jest tak jakby pleśnią. Ale Klaudia WIE że to nie pleśń. To załoga. Nowa forma oficerów. Mniej energii.
    * Martyn i Klaudia omijają 'pleśń' by dotrzeć do zgromadzonej załogi Talikazera. Wszyscy są sprowadzeni przez Vishaera. Wszyscy mają pleśniowe porosty. Są zmienieni - umieją w tym oddychać.
        * Martyn: "Prosta kwarantanna i prosty detoks. Nic złego się im nie stało." (zimny)
        * Klaudia: "Doskonale, musimy ich wynieść do śluzy."
        * Martyn: "Zespół, jestem oficerem medyczny Serbiniusa. Waszym zadaniem jest dostać się TU i TU. Kto może, idzie. Kto nie może, ma pomoc. Ruchy."
* X: Jeden z terrorformów, (skupia się na biomasie i energii - wie że chroni i pomaga nie wie czemu), zapoluje na Klaudię i Martyna. Vishaer nie traktuje zniszczenia tego terrorforma jako 'złe'
* V: Załoga Talikazera zostanie uratowana bez ingerencji Vishaera.

== TEST MECHANIKI DYNAMIZMU ==

Załoga Talikazera wycofuje się zgodnie z poleceniami do śluzy, gdzie Helmut z poziomu Serbiniusa ma ich wziąć na pintkę. Klaudia i Martyn ich asekurują. W pewnym momencie Martyn odepchnął Klaudię na ścianę. Fragment 'pleśniomasy' przeciął miejsce gdzie Klaudia była i formuje coś na kształt humanoida skrzyżowanego z hydrą złożonego z pleśni. 

BAZA: +3Vz (servary są pancerniejsze) +3V (dwie osoby i Martyn umie walczyć) +5X (dudnienie magii i niestabilność terenu i terrorform na swoim terenie).

* Terrorform się rozdziela i każdą witką ostro atakuje wszystkich, pajęcza zmienna sieć ale z pleśni (2)
* Martyn bierze jakieś elementy sprzętu (coś co zabraliście jako tarczę) i zasłania. Klaudia używa _uszczelniacza_ servara by uszkadzać i redukować biomasę (2)
    * X: Sprzęt komunikacyjny z Serbiniusem został zniszczony. Martyn musiał zrobić manewr unikowy i po prostu nań padł.
* Terrorform skupia się na ataku w załogę Talikazera. Nieważne KTO zostanie. Biomasa musi być. (3Xb)
* Klaudia przesuwa się by wziąć to na pancerz servara. I krzyczy "nie potrzebujesz tego jesteśmy tu by ratować ludzi. Ta energia NIE BĘDZIE POTRZEBNA!" (2Vr)
    * V: Terrorform nie ustępuje, ale pancerz Klaudii wytrzymał pierwsze uderzenie, choć magia sprawiła że było niesamowicie mocne. Zanim mógł uderzyć ponownie, Martyn poraził go prądem. Część biomasy obumarła.
* Martyn przesuwa się by on i Klaudia byli po stronie ludzi.
* Terrorform skupia się na ataku w nogi. Uderzyć, zablokować, unieszkodliwić. Wkręcić się w serwo. (1X)
* Klaudia elektryfikuje swój servar. (3V)
    * Vg: Terrorform próbował się wkręcić i Klaudia wysunęła się jako pułapka a Martyn osłaniał uciekających. Gdy Terrorform był 'na Klaudii', ta odpaliła elektryfikację. Terrorform zasyczał, szok go odpędził.

Terroform, ciężko uszkodzony się ewakuuje. Martyn i Klaudia osłonili załogę, po czym idą dalej.

== END TEST MECHANIKI DYNAMIZMU ==

Klaudia i Martyn trasują najlepszą możliwą drogę by znaleźć stazy i żeby nic się nie stało na tej jednostce, omijając terrorformy itp.

TrZ (link Klaudii z 'TAI')+3:

* X: Klaudia zaczyna czuć się gorzej. Jej Skażenie jest za duże (+Ow). Cieknie krew z nosa itp.
    * Wolfgang (echo): "Przetrwamy... za wszelką cenę..."
    * Klaudia: (ustawia w pętli komunikat) "Dotarliście do ratunku. Załoga przetrwa. Pomóż nam ją ewakuować." (+1Ow)
* V: Wolfgang rozświetlił drogę. Sęk w tym, że taką dla pleśni a nie dla ludzi, ale servary mogą sobie wyrąbać przejście. To solidne Lancery.
* Vr: Klaudia i Martyn przebili się przez ruinę Vishaera. Docierają do 'komnaty'. Tam są resztki ludzkich ciał. Wszyscy oficerowie, kapitan, wszystko posłużyło jako zasilanie i żywność dla kapsuł. Została tylko wola animująca 'pleśń'. Minimalna ilość energii. Klaudia czuje zagrożenie magiczne w powietrzu. Tu jest najgroźniej. Martyn się zbliżył by sprawdzić stan urządzeń.
    * Martyn: "Część tego działa. Część warto próbować uratować. Część tych, które nie zadziałały posłużyły za żywność."
    * Wolfgang (echo): "Przetrwamy... za wszelką cenę..."

Klaudia: "WSZYSTKO czego potrzebujecie to JEDEN OSTATNI RUCH by uratować tych których się da. Pomóżcie mi ich wydobyć. Wszystkie Wasze poświęcenia nie były na marne. Pomóżcie mi dokończyć dzieła."

Tr+3+Ow:

* Vr:
    * Wolfgang: (enumeruje imiona). Pleśń się odsuwa; ciepło generowane przez pleśniowców nie będzie potrzebne.
    * Trakcja / szyny na której można przetransportować lifesupporty nie działa. Nie ma energii i są uszkodzone elementy.
    * Lifesupporty dostają całość energię statku jaką Vishaer jest w stanie przekierować.
    * Martyn: "Mogę spróbować podać im środki które powinny im pomóc przetrwać. Ale nie możemy przeprowadzić ich przez próżnię."
    * Klaudia: "Mamy nasz kontener"
    * Martyn: "Przy śluzie"
    * Klaudia: "Musimy przenieść lifesupport po lifesupporcie RĘCZNIE, licząc, że naenergetyzowane przetrwają i utrzymają ludzi"
    * Martyn: "Zadziała. Znam te modele. Mają 30 minut życia po odpięciu od prądu."
* V:
    * Martyn i Klaudia przenoszą jeden po drugim, spiesząc się i idąc ostrożnie. Servary trzeszczą.
    * Towarzyszą im cienie terrorformów, które PATRZĄ na oddalającą się załogę.
    * Martyn i Klaudia ewakuowali Vishaera o 11 lifesupportów

Jak tylko weszliście na pintkę z podpiętą baterią lifesupport, połączenie z Serbiniusem.

* Helmut: Jesteście? To Wy? Dajcie analizę krwi czy coś.
* Martyn: Rozsądne myślenie, ale fundamentalnie nie zadziała.
* Klaudia: Dobrze myślisz, za mało wiesz.
* Helmut: Brzmisz jak Klaudia... (rezygnacja)
* Martyn: Musimy podpiąć te systemy do Serbiniusa
* Klaudia: Potrzebujemy energii. JAKĄKOLWIEK izolację. SZYBKO.
* Martyn: 10 minut.
* Helmut: To może być pułapka, tak jak Talikazer. No postawcie się w moim miejscu.
* Klaudia: Ciężka sprawa.
* Martyn: Jesteś oficerem Serbiniusa, zachowuj się jak oficer. (bardzo ostro jak na Martyna)
* Helmut: Nie mogę podpiąć Was do Serbiniusa, ale mogę przylecieć na pintkę i użyć jej zasilania. Pasuje?
* Klaudia: Jak długo zasilisz te systemy...
* Helmut: Żebyście wiedzieli - Anastazy w Was celuje. Więc Serbinius jest bezpieczny. Jakby co.
* Klaudia: Bardzo dobrze.

Helmut szybko dostał się na pintkę. I szybko zaczął przekierowywać energię. Bardzo ekstremalna robota z ogromną presją czasu.

Ex Z(zna pintkę, Martyn zna te lifesupporty) +3:

* XX: Pintka jest zniszczona. To była droga w jedną stronę dla pintki. Pintka lub ludzie.
* V: lifesupporty są pod prądem prawidłowo. Udało się uratować wszystkich których się dało.

Klaudia -> Fabian: "Wyślij komunikat na Vishaer / Wolfganga z gatunku 'uratowałeś ich wszystkich, salut'"

* XX: Vishaer 'umarł'. Wolfgang zasalutował. "Misja... wykonana..."
    * Vishaer zaczął się rekonstruować. Magia zaczęła reagować na obecność życia, energii i braku woli dominującej.

Serbinius włączył silniki. Nie chce tu być gdy dojdzie do Anomalii Kosmicznej. Holuje ze sobą Talikazer.

XD

## Streszczenie

Vishaer, niezależny statek wpadł w poważne kłopoty i nie miał szansy na ratunek. Kapitan kazał załodze się hibernować po czym wyłączył generator Memoriam i spróbował użyć magii by wszyscy przetrwali. 3 lata później na Vishaer natrafił inny statek, Talikazer, i próbował mu pomoc. 2h potem na sygnał SOS przyleciał Serbinius. Udało się uratować załogę Talikazera oraz część załogi Vishaera, po czym Vishaer zmienił się w anomalię.

## Progresja

* Fabian Korneliusz: duży szacunek ze strony załogi Serbiniusa; poszedł samemu rozwiązać problem terrorformów na Talikazerze i zachowywał się jakby byli oficerami a nie potworami.

## Zasługi

* Klaudia Stryk: MVP. Odkryła naturę Skażenia Talikazera i AK Vishaer, wykorzystała Pryzmat by AK Vishaer pomogła Klaudii w ratunku ludzi, zainspirowała Fabiana do uratowania Talikazera i weszła z Martynem na Vishaer by ręcznie ratować kogo jeszcze się da.
* Martyn Hiwasser: uratuje wszystkich kogo się da, dowodził operacją wejścia na AK Vishaer, osłonił Klaudię w walce z terrorformem i udało mu się wydobyć stasis-chamber które dało się uratować
* Fabian Korneliusz: próbuje uratować załogę i wszystkich ludzi. Mimo nerwów, zaufał Klaudii i zaufał, że Skażeńcy/terrorformy to nadal ludzie. Tak do nich podszedł i udało mu się samodzielnie uratować Talikazer.
* Helmut Szczypacz: boi się zarazków i Skażenia, też nie nadaje się na myślenie o Skażeńcach jak o ludziach. Rozwalił pintkę by zasilić odratowane lifesupporty.
* Anastazy Termann: ogólnie w bojowym nastroju i chce 'strzelać do potworów', nie umie myśleć o Skażeńcach jak o ludziach. Mimo kiepskich żartów osłania kapitana gdy ten poszedł na Talikazer.
* Wolfgang Sępiarz: KIA. Wyłączył generatory Memoriam by ochronić swoją załogę, ale zapłacił za to życiem i człowieczeństwem swoim i oficerów-ochotników. Zaiste, "zszedł z okrętu ostatni".

## Frakcje

* Frakcja Vishaer: tym razem szczęśliwie przetrwali

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 19
* Dni: 2

## Metadane

* title: "Pan Skarpetek i Odratowany Ogród"
* threads: agencja-lux-umbrarum, problemy-con-szernief
* motives: energia-alteris, energia-fidetis, lokalne-mity-i-legendy, bardzo-irytujacy-reporter, crowd-control, faceci-w-czerni, masowy-atak-mentalny, starcia-kultur, budowa-wezla-energii, wektor-skazenia-nieswiadomy
* gm: żółw
* players: darken, lordan, wawrzus

## Kontynuacja
### Kampanijna

* [231221 - Pan Skarpetek i Odratowany Ogród](231221-pan-skarpetek-i-odratowany-ogrod)

### Chronologiczna

* [231221 - Pan Skarpetek i Odratowany Ogród](231221-pan-skarpetek-i-odratowany-ogrod)

## Plan sesji
### Projekt Wizji Sesji

* PIOSENKA WIODĄCA: 
    * "I, I've suffered long enough in this ghost town | I saw the walls, called their bluff and took them down"
    * "I made my own machine | Yes, we're building steam | I hate the same routine. | I, I never looked back--I want to never return | If I could find a fuse, those bridges would burn"
    * Reprezentuje nowe, nadchodzące na stację. Pan Skarpetek jest tego kluczową manifestacją.
* Opowieść o (Theme and vision): 
    * "Zbyt szybkie przekształcenia kulturowym w zbyt wysokim polu magicznym prowadzą do poważnych problemów"
* Motive-split ('do rozwiązania przez' dalej jako 'drp')
    * energia-alteris: W formie Pana Skarpetka, miesza znaczenia (‘Pan Skarpetek’ == ‘Trusted Officer’) oraz dostarcza byty do Ogrodu Alteris.
    * energia-fidetis: Zabawka dla dziecka reprezentująca Pana Skarpetka, pragnąca zapewnić im piękny ogród jak było w bajce.
    * lokalne-mity-i-legendy: savarańskie bajki o Panie Skarpetku stały się rzeczywistością pod wpływem kombinacji energii Alteris i Fidetis.
    * bardzo-irytujacy-reporter: Kalista w swoim żywiole. Próbuje dojść do tego że coś się dzieje i co dokładnie się dzieje. Ma wsparcie mięśnia od Mawira.
    * crowd-control: Konieczność neutralizacji komponentów stacji kosmicznej które mogą zaobserwować że coś jest poważnie nie tak z systemem dotkniętym przez Alteris. I ludzi.
    * faceci-w-czerni: jak zwykle, Agencja Lux Umbrarum. Bardziej niż zwykle, bo tym razem Pan Skarpetek operuje jawnie.
    * masowy-atak-mentalny: wywołany przez Alteris; każda osoba Dotknięta przez Pana Skarpetka się dziwi, że ktoś NIE CHCE z nim współpracować. W końcu to Pan Skarpetek. Dotyczy każdej osoby z maską Skarpetka.
    * starcia-kultur: kultura savarańska zaczyna stawać się dominantą przez działania Pana Skarpetka; mamy radykałów (pro-kulturowych) vs ludzi co chcą mieszać kultury
    * budowa-wezla-energii: Pan Skarpetek próbuje ze zużytego i niewykorzystywanego systemu podtrzymywania życia zbudować Piękny Odratowany Ogród.
    * wektor-skazenia-nieswiadomy: dosłownie. To dzieci, które mają zabawki z Panem Skarpetkiem. Zniszczenie / Odkażenie wszystkich zabawek rozwiąże problem.
* Detale
    * Crisis source: 
        * HEART: "Nadmiar energii magicznej na stacji która nie została prawidłowo uziemiona ostatnio (Agencja nie wie jak)."
        * VISIBLE: "Pan Skarpetek buduje Odratowany Ogród"
    * Delta future
        * DARK FUTURE:
            * chain 1: OGRÓD: powstanie i będzie cudowny i odmienny i można się w nim zgubić
            * chain 2: ?
            * chain 3: PRAWDA: wiedza o magii wyjdzie na jaw, konieczność aplikowania masowych amnestyków. Zniszczenie stacji?
        * LIGHT FUTURE: 
            * chain 1: OGRÓD: usunięty problem
            * chain 2: ?
            * chain 3: Nikt jak o magii nie wiedział tak nie wie
    * Dilemma: brak

### Co się stało i co wiemy

W światach savarańskich jedną z kluczowych legend i postaci mitycznych jest Pan Skarpetek. Pan Skarpetek jest zabawką savarańskich dzieci, zrobioną z faktycznej skarpetki z oczami. Jest on bohaterem wielu wydarzeń; jest bardzo pozytywną postacią, której należy słuchać. Ta baśń, która nas interesuje dotyczy budowy ogrodu ze starego systemu recyklingowego.

Trzeba pamiętać że mimo że savaranie nie wyglądają jak normalni ludzie ale jednak nimi są. Mają normalne marzenia i nadzieje. Pragną świata pełnego natury spokoju czystości i piękna. Savarański raj to spokojne miejsce gdzie jest trochę więcej przestrzeni są piękne rośliny i można pozwolić sobie na odrobinę marnotrawstwa.

Wraz z pojawieniem się energii Fidetis i ze zmiksowaniem jej z energią Alteris doszło do stworzenia manifestacji Pana Skarpetka. Ten byt pragnie stworzyć ogród w okolicach nieużywanego systemu podtrzymywania życia by wszystkie dzieci mogły się tam dobrze bawić. Pan Skarpetek powołuje z Alteris różnego rodzaju rośliny i mechanizmy; będzie też potrzebować pomocy dorosłych tam gdzie dzieci sobie same nie poradzą.

Oczywiście jest to śmiertelnie niebezpieczne z perspektywy Agencji.

* Kontekst stacji
    * CON Szernief została zaprojektowana jako stacja sprzedająca Irianium. Jest to miejsce o podniesionej ochronności elmag (lokalizacja: przy Sebirialis).
    * Okazuje się, że populacja która zainteresowała się tymi warunkami to byli Savaranie.
        * I inwestorzy są w klopsie. Dla savaran nadmiar energii to radość. Nie chcą kupować seks-androidów ani niczego. Pełna oszczędność.
            * Savaranie to NAJGORZEJ z perspektywy korporacji i inwestycji XD
        * Inwestorzy żądają zwrotu
    * Zarządcy CON Szernief szukają kogoś kto będzie tam chciał żyć i najlepiej za to jeszcze zapłaci
        * Stanęło na ściągnięciu Kultystów Adaptacji. Ale oni nie są jacyś super niebezpieczni, sami tak mówią ;-).
* Przeszłość
    * 1 miesiąc temu udało się rozwiązać problem z energią Fidetis i Łowcą.
    * Tydzień temu zaczęły się pierwsze numery powiązane z Panem Skarpetkiem. (scena zero)
    * Agencja NATYCHMIAST musi wpaść i maskować.

### Co się stanie (what will happen)

* F0: TUTORIAL
    * Zespół gra trójką dzieciaków. 
    * Ich zadaniem jest dowiedzieć się od dorosłych pracujących w systemach dowódczych gdzie najlepiej można by założyć ogród. 
    * A potem sprawić by dorośli o wszystkim zapomnieli bo to będzie dla nich wielki sekret ucieszą się gdy ogród będzie piękny.
* F1: Pan Skarpetek bada Ogród
    * KTO (łańcuchy)
        * Pan Skarpetek: 
            * chain: (umieszcza kwiat, prosi Mawirowców o rekonstrukcję Miejsca, przygotowuje Alteris ogród, pierwsze krwawe ofiary dzieci (składamy krew z ręki i czytamy bajki))
        * Kalista: 
            * chain: (dochodzi do sekretów bajek savarańskich, znajduje ludzi Mawira, znajduje miejsce Ogrodu)
        * TAI: (NIE alertuje o kwiatach bo nie wie, ALE demonstruje inne problemy)
        * Sia (Oszczędnościowcy): (współpracuje z Zespołem; ogród to marnotrawstwo. Oszczędność i redukcja a nie podnoszenie savaran)
            * chain: 
        * Aerina (Popularyzatorzy): grajmy na współpracę (Ikta, Teriman). Popularyzujmy kulturę.
            * chain: Festiwal Pana Skarpetka, Baśń 'Pan Skarpetek i Inżynier Drakolicki' do napisania przez Kalistę
        * Mawir (Separacjoniści): Dojdźmy do tego co się tu dzieje. Wspierajmy Kalistę.
            * chain: konstrukcja Pomieszczeń Toksycznej Adaptacji, rozchodzenie się i pytanie, polowanie na Pana Skarpetka
        * Wit (Separacjoniści): savaranie mają być savaranami, nie powinniśmy się tak mieszać. Róbmy swoje i działajmy. Anty-Aerina.
            * chain: brutalna dewastacja każdego co chce współpracować z Integratorami LUB wygnanie
        * Ikta i Teriman (Integratorzy): (podnieśmy savaran; czemu nie do poziomu drakolitów?)
            * chain: popularyzacja, proponowanie, współpraca z Aeriną
        * Rada: chce zrozumieć co się tutaj dzieje i nie chce uszkodzeń dla stacji
            * -> Teraquid: mogą zaatakować i zniszczyć Ogród w imieniu Rady
* F2: Pan Skarpetek karmi Ogród
* F3: Pan Skarpetek uruchamia Ogród
* SUKCES?
    * Jednoczesne zniszczenie wszystkich instancji Pana Skarpetka
* Overall
    * stakes
        * pojawienie się Ogrodu Alteris
        * wiedza o magii i działania Kalisty
        * cierpienie dzieci i kultura
    * opponent
        * Pan Skarpetek
        * Kalista
    * problem
        * manifestacja Pana Skarpetka
        * Kalista w akcji (chroniona przez Mawira)
        * starcia kulturowe
        * mówimy o DZIECIACH

## Sesja - analiza

### Fiszki

.

#### Strony

.

### Scena Zero - impl

* Zespół gra trójką dzieciaków. (10-15)
    * Ich zadaniem jest dowiedzieć się od dorosłych pracujących w systemach dowódczych gdzie najlepiej można by założyć ogród. 
    * A potem sprawić by dorośli o wszystkim zapomnieli bo to będzie dla nich wielki sekret ucieszą się gdy ogród będzie piękny.

Wawrzuś podejść do strażnika, zagadać, mundur. ROzkojarzony. Pomoc Pana Skarpetka.

Tp+3:

* V: Strażnik jest rozkojarzony, jest skupiony na Tobie i może reszta zespołu przejść.
* V: Pod wpływem Pana Skarpetki strażnik otworzył Wam drzwi i wpuścił

"Gdzie możemy założyć ogród?" - "Młody człowieku, nie ma miejsca..."

* V: Mężczyzna uśmiechnął się do Was
    * "Oczywiście, ogród..." strużka krwi.
* X: TERMINUJĘ KONFLIKT.

.

* W: RAPORTY, ZGŁOSZENIA. Co się działo?
    * Informacje o dzieciakach, czy dzieciaki narozrabiały.

TrZ+3

* V: 
    * Te trzy dzieciaki to savaranie. Oni byli spokojnych, nie są rozrabiakami.
    * Więcej dzieci zaczęło się rozłazić i pytać o 'kwiaty', 'ogrody' i dorośli zawsze pomagali
* Vz:
    * Dzieciaki które ZACZĘŁY działania i gdzie występowały ("pan skarpetka") należą do frakcji Integratorów.

### Sesja Właściwa - impl

L: Spróbować na podstawie danych 'przypadki podobnego typu anomalii'. Coś co może być podobne.

* Na 100% anomaliami mentalnymi
* Na 100% anomaliami memetycznymi
* Lordan: wiesz, że "Pan Skarpetek" to mityczna postać. Savarańska maskotka.

JAK zacząć?

1. Wezwała Was Rada, z ramienia Feliny. Ona może powiedzieć i pokazać.

Luminarius zadokował.

Felina opowiada. Dzieci i drakolickie i savarańskie, ale nie dorośli.

FILTROWANIE danych o Panu Skarpetku:

Tp+3:

* V: 
    * nie ma żadnych opowieści o Panu Skarpetku który manipuluje, robi złe rzeczy
    * on zawsze robi takie dobre, pozytywne rzeczy
    * Ogród. Jest coś odnośnie Pana Skarpetka i Ogrodu.
        * piękny ogród z systemu podtrzymywania życia (uszkodzonego)

(Felina szuka informacji o savaranach i systemach podtrzymywania życia)

Tr Z +3:

* X: Felina jest odcięta, ona nie może się dowiedzieć, ale wie że coś jest nie tak i Agencja musi jej pomóc. Podbijamy. (+2Vg za wentylacja nie pasuje)
* V: Felina nie miała dostępu ale z Wami ma. Okazuje się, że JEJ LUDZIE, JEJ PODWŁADNI ukrywali to przed nią.
    * jest jedno miejsce - nieaktywny system podtrzymywania, uszkodzony, który właśnie jest reperowany. Grupa drakolitów - inżynierów - go naprawia. Przebudowuje.
        * to NIGDY nie zadziała, nie ma sensu.
    * Felina jest wściekła. Chce polować na Ignatiusa.

Darken idzie polować na Ignatiusa. Wawrzuś, Lordan idą razem.

Darken - Ignatius jest w "TechnoBaletnica" lokalny bar drakolicki. Pojawiają się tam też savaranie. Dorion się kłóci z Ignatiusem. Darken pokazuje "wszystko wskazuje na Ciebie i powinienem połamać". Ignatius patrzy z takim absolutnym zdziwieniem. "Ej, działałem bo poprosił mnie Pan Skarpetek. No... zrobiłbyś inaczej?" (Ignatius zaskarpetkowany)

Lordan: "czyli to nie tylko pozytywna manipulacja, bardziej ogólne". Wawrzuś: "jeśli w systemach... może wpływać na wszystkich?"

Uszkodzone systemy podtrzymywania życia. Te systemy są w miejscu, w którym NIC NIE MA.

* W: "Co tu się odwala i kto tym dowodzi?"
* Gardlan: "Gardlan jestem, ja dowodzę. A co?"
* W: Czyje polecenie?
* G: Pan Skarpetek. To chyba oczywiste.
* W: Macie plany? Schematy?

Tr +3:

* X: Jest 'conditioned', nie da się złamać
* Vr: Pokazał Ci plany. Masz je.
    * One nie mają sensu pod względem technicznym. Zero. Nic nie zadziała. To nic nie zrobi.
    * Obecność 4 wymiaru może COŚ zrobić.
* X: Darken jest postrzegany za zagrożenie przez Alteris i Ignatiusa.
* (+Z) Xz: Wawrzuś ZNIKNĄŁ.
* X: Lordan TEŻ zniknął.

...

Znajdujesz się w wielkiej arkologii. A NIE PROBÓWKA. (doniczka)

Lordan. Ogród botaniczny? (to musi zżerać zasoby). Gdzie to jest?

Tr +2: (ExZ, ale +nadajnik w doniczce)

* V: WIESZ gdzie jesteś. Jesteś w 'niemożliwych rurach', w więcej niż jednej rurze, w tym pomieszczeniu. (najbliższe dziecko to Mat-22)
    * L: "Gdzie jesteśmy? Gdzie rodzice?"
    * M: (jak na idiotę) "W ogrodzie. Widzisz ogród?"

Spotkanie z Panem Skarpetkiem. A rośliny? NIE ZNASZ ICH. A dziecko upuszcza sobie krwi karmiąc rośliny.

...

Wawrzuś - skarpeta, dziury na oczy, zakładasz na głowę i mówisz do ludzi "idźcie do domów, odpocznijcie, dobra robota".

TrZM+3Ob:

* X: Moc Pana Skarpety zaczęła wpływać. Drony musiały puścić 'prysznic' z rur.
* X: Samo jedna rura nie wystarczyła. Drony musiały SERIO zrobić 'porządną ulewę'. To zaalarmowało wszystkich 'zainteresowanych', nie tylko z Rady.
* Ob: Pomieszczenie zaczęło się przekształcać pod wpływem energii magicznej. Ale woda przeszkodziła.
    * mamy energię magiczną
    * mamy duże emocje
    * mamy manifestację
    * NieRośliny obumarły

Wdarła się woda do zakrzywionej przestrzeni. Lordan PRÓBUJE je wziąć na serwopancerz. Aktywujesz magnetyczne wspomaganie.

Tr Z +3:

* X: Przesterowany serwopancerz, uszkodzony. Ale ma uszkodzone serwomotory, nie rusza się dobrze.
* Vz: Serwopancerz spełnił swoją rolę - uratowane dzieci.
* V: Wszyscy wyprowadzeni

Wawrzuś dzielnie pomaga, pilnuje, monitoruje, przesuwa rury. Lordan, z dziećmi a serwopancerz płacze. 

Pojawia się KALISTA i Mawir Hong. Nie wiem jak tu ukryć fakt obecności magii.

Darken ściąga dzieci proponując darmowe szkolenia na Luminariusie. Masa krytyczna. Lista dzieci 'podejrzanych' - wszystkie dzieci z frakcji Integracji + część savarańskich nieintegrujących. Znajomości wśród dokerów, dowód że wie lepiej

Tr Z +3:

* X: Są wnioski, że Agencja promuje integrację ludów na stacji (bo te dzieci wspiera)
* V: Masz dzieci na Luminariusie. Masę krytyczną. A Luminarius jest ekranowany.
    * Kilkaset dzieci
        * zadaję pytania, ktoś odpowiedział błędnie - LAPISUJEMY WSZYSTKICH!!!
* X: "akcja PvP pomiędzy dziećmi", rozbudzenie ja kontra ty, PRZEZ MNIE LAPISOWALI! Kalista to obsmaruje.
* X: Kalista dowiaduje się o tematach nadnaturalnych że istnieją rzeczy paranormalne
* Xz: Pan Skarpetek się manifestuje, dzieci ucierpiały, ale nic trwałego -> Kalista się dowiaduje

TYMCZASEM tutaj, na miejscu.

* Wawrzuś: "nie wiem kto zarządza, jakie plany.."
* Mawir: "moi tego nie zrobili"
* Wawrzuś: "mamy plany. Pan to podpisał."

TrZ+4:

* V: Mawir patrzy na plany, patrzy na przestrzeń.
    * "Ten plan jest z dupy. Nie wiem co piłem. (myśli). Ale to gówno się nie spina"
* Vr: Inżynierska kłótnia, Mawir "wygra", ale odwrócona uwaga Mawira.

Kalista podchodzi do Lordana.

* L: Dorośli nie upilnowali, potem są wyniki.
* K: Co tu się stało? Prawdę.

Lordan bierze młodsze dziecko i daje je Kaliście, zabierzmy je. Kalista patrzy na Ciebie podejrzliwie. 

* L: "Co my robimy?"
* K: "Coś... nielegalnego, złego, niewłaściwego... robiłam badania na Wasz temat. Nic nie znalazłam."
* L: "Próbujemy zapobiec złym rzeczom. Nie chcesz wiedzieć więcej, to rzeczy których nikt nie powinien wiedzieć. Nie wiesz jaki chaos."
* L: "To co myślisz że o czym była wojna nie było prawdą. Rozumiesz na odwrót."
* L: "Nie mam autoryzacji. Możesz pogadać z archiwistą. Ja jestem trepem."
* K: "Daj mi coś prawdziwego, albo nadam temat WSZYSTKIM ZAWSZE."
* L: "Nikomu nie powiesz, uchylę rąbka"
* K: "Rozważę nie powiedzenie."

Lordan próbuje jej to wyjaśnić. Podkreślając wagę tego jak to niebezpieczne. Wyjaśnia Kaliście Pryzmat. Dlaczego nikt nie może wiedzieć.

Tr +3:

* X: Kalista nie wierzy w prawdę. Musiał pokazać niektóre dane z tego pomieszczenia i te dziwne rośliny (nagrania z servara)
* V: Kalista zachowa to dla siebie. Dalej nie lubi Agencji, ale nie będzie po Was jechać.
* V: Kalista bierze Oficera Naukowego jako swojego 'jedynego sojusznika w Agencji'. Ostrzeże w przyszłości.

OFICJALNIE koniec sesji.

Ten dron. Lata, zmniejszony, Alteris nie ma dość energii. Ale dron się nie zwiększył.

Wawrzuś naprawił filtry. Na koszt Agencji.

Tr Z+4:

* Vz: Reputacja Agencji do przodu.

Koniec.

## Streszczenie

Rada wezwała Agencję, bo dzieciaki potrafią rozkazywać dorosłym używając savarańskiej postaci z bajek, 'Pana Skarpetka'. Agencja odkrywa, że dzieciaki próbują zrobić Rajski Ogród - miejsce z roślinami i radością. Gdy Agencja orientuje się, że to jest niemożliwe technicznie, odkrywają wpływ energii Alteris. Po przebijaniu się przez Alteris udało im się uratować dzieci, ale pomieszczenie jest Zniekształcone i trzeba było Kaliście coś powiedzieć o magii... za to, Agencja ma poprawioną reputację, choć ma opinię tych co promują współpracę ludów na stacji.

## Progresja

* .

## Zasługi

* Klasa Sabotażysta: porozmawiał z Ignatiusem i dowiedział się o wpływie Pana Skarpetka; potem załatwił sprawę i pozyskał dzieci ze stacji do lapisowania na Luminariusie, usuwając efemerydę Pana Skarpetka. Wyszedł na dziwnego łosia w oczach stacji, ale udało mu się usunąć Anomalię.
* Klasa Oficer Naukowy: poznał savarańskie legendy, gdy w Nie-Ogrodzie-Botanicznym doszło do 'powodzi', użył servara by ratować dzieci i nikt nie ucierpiał (choć servar zniszczony). Wyjaśnił Kaliście na czym polega problem z Pryzmatem i zobowiązał ją do milczenia.
* Klasa Inżynier: gdy napotkał Anomalię Alteris, zdecydował się ją przetestować i 'założył Pana Skarpetka' na głowę. Drony uratowały go przed przejęciem (woda). Potem odwrócił uwagę Mawira i ponaprawiał rury na koszt Agencji, dzięki czemu +5 szacunku ze strony Stacji.
* Felina Amatanir: odcięta od własnych systemów przez swoich ludzi dotkniętych Alteris; briefowała Zespół i skierowała ich na Ignatiusa i innych.
* Ignatius Sozyliw: Skażony przez Alteris (Pan Skarpetek go poprosił), ukrywa dane przed Feliną. Nie walczy z Agencją, nie rozumie czemu oni nie chcą pomóc Skarpetkowi.
* Mawir Hong: inżynier którego uwagę Inżynier odwrócił kłócąc się o 'te plany są z dupy'. Dzięki temu Oficer Naukowy mógł solo pogadać z Kalistą.
* Kalista Surilik: dowiaduje się o rzeczach magicznych i Oficer Naukowy wyjaśnia jej Pryzmat. Rozumie czemu jest tak a nie inaczej. Dalej nie ufa Agencji, ale ufa JEDNEJ OSOBIE.

## Frakcje

* Con Szernief: zaatakowana przez legendę Pana Skarpetka zmienioną przez Alteris; jedno pomieszczenie zostało zniszczone. Agencja promuje integrację między ludami, co bardzo podoba się Radzie acz niekoniecznie Mawirowcom. Agencja ma wyższy poziom zaufania.
* Con Szernief Querenci: pojawiła się masa krytyczna wiedząca o magii (Felina, Kalista, pewne ślady innych). Mogą rozpocząć eksperymenty i działania.
* Con Szernief Unifikaci: bardzo wzmocnieni przez Pana Skarpetka i współpracę z Agencją; pozostałe frakcje ograniczone w mocy.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Kalmantis
            1. Sebirialis, orbita
                1. CON Szernief

## Czas

* Chronologia: Inwazja Noctis
* Opóźnienie: 133
* Dni: 2

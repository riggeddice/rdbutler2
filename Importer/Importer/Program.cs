﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Commons.CoreEntities;
using Commons.Infrastructure;

using Importer.P1_ReadParsing;
using Importer.P4_Persisting;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P4_Persisting.EntityDbFactories;
using Importer.P3_Aggregation.FactoriesSupportive;

namespace Importer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Assuming we ever want to make a 'UI' of sorts
            // Importer is then responsible for all heavy calculations
            // and for filling the Database with all useful stuff
            // while Exporter just creates views of the data in the DB

            Source source = new Source();

            // First stage (1)
            //      reading and parsing data from external sources. First-level params.

            ReadActor[] readActors = source.ProvideActors();
            ReadActorCreationComponent[] readActorComponents = source.ProvideActorCreationComponents(); 
            ReadFaction[] readFactions = source.ProvideFactions();
            ReadStory[] readStories = source.ProvideStories();
            ReadThread[] readThreads = source.ProvideThreads();
            ReadMotive[] readMotives = source.ProvideMotives();
            GlobalTime time = source.ProvideTime();

            // Second stage (2)
            //      extracting second level params. 

            ActorMerit[] actorMerits = ActorMeritExtractor.CreateManyFromStories(readStories);
            ActorProgression[] actorProgressions = ActorProgressionExtractor.CreateManyFromStories(readStories);
            FactionMerit[] factionMerits = FactionMeritExtractor.CreateManyFromStories(readStories);
            Dictionary<string, string[]> storyToThreadIdsMap = readStories.SelectMany(StoryToThreadRelationFactory.CreateFrom)
                                                          .GroupBy(x => x.StoryId)
                                                          .ToDictionary(x => x.Key, x => x.Select(y => y.ThreadId).ToArray());
            MotiveAcdeor[] motiveAcdeors = MotiveAcdeorExtractor.CreateManyFromStories(readStories);

            // Third stage (3)
            //      based on (1) and (2), calculate final, third level params

            AggregatedStory[] aggregatedStories = AggregatedStoryComposer.CreateManyFromStories(readStories, time);
            LocationRecord[] locationRecords = LocationRecordComposer.CreateManyFromStories(readStories, aggregatedStories);
            AggregatedActorFlashcard[] flashcards = AggregatedActorFlashcardComposer.CreateManyFromManyStories(readStories, readActors);
            AggregatedActor[] aggregatedActors = AggregatedActorComposer.CreateMany(aggregatedStories, readActors, actorMerits, actorProgressions, flashcards, storyToThreadIdsMap);
            ActorBriefRecord[] actorBriefs = ActorBriefRecordComposer.CreateMany(aggregatedActors);
            StoryBriefRecord[] storyBriefs = StoryBriefRecordComposer.CreateMany(aggregatedStories);
            ActorThreadStoryBlock[] actorStoryBlocks = ActorThreadStoryBlockComposer.CreateManyFromManyActorsManyStories(aggregatedActors, aggregatedStories, locationRecords);
            GeneralStoryBlock[] generalStoryBlocks = GeneralStoryBlockComposer.CreateMany(aggregatedStories);
            AggregatedMotiveAcdeor[] aMotiveAcdeors = AggregatedMotiveAcdeorComposer.Create(motiveAcdeors, aggregatedStories);
            ThreadBriefRecord[] threadBriefs = ThreadBriefRecordComposer.CreateMany(aggregatedStories, readThreads);
            MotiveBriefRecord[] motiveBriefs = MotiveBriefRecordComposer.CreateMany(aggregatedStories, readMotives);
            LocationTreeNode locationTree = LocationTreeComposer.Create(locationRecords);
            AggregatedFactionMerit[] aFactionMerits = AggregatedFactionMeritComposer.Create(factionMerits, aggregatedStories);
            FactionBriefRecord[] factionBriefs = FactionBriefRecordComposer.CreateMany(aFactionMerits, readFactions);

            AtarRecord[] atfrRecords = aggregatedActors
                .SelectMany(a => AtarRecordComposer.CreateFor(a.Name, actorMerits, factionMerits)).ToArray();
            AtarRecord[] ftarRecords = factionBriefs
                .SelectMany(f => AtarRecordComposer.CreateFor(f.Name, factionMerits, actorMerits)) .ToArray();
            AtarRecord[] ftfrRecords = factionBriefs
                .SelectMany(f => AtarRecordComposer.CreateFor(f.Name, factionMerits, factionMerits)).ToArray();

            // Fourth stage
            //      persist those params we need for core operations

            Database db = new Database();
            await db.Persist(readActors, readStories, readFactions);
            await db.Persist(aggregatedStories, locationRecords, storyBriefs);
            await db.Persist(aggregatedActors, actorBriefs, aFactionMerits);
            await db.Persist(flashcards);
            await db.Persist(generalStoryBlocks, actorStoryBlocks);
            await db.Persist(readThreads, readMotives, threadBriefs, motiveBriefs, factionBriefs, aMotiveAcdeors);
            await db.Persist(locationTree);
            await db.Persist(atfrRecords, ftarRecords, ftfrRecords);
            await db.Persist(readActorComponents);
        }
    }
}

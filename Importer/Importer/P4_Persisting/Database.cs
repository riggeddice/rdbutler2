﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Commons.CoreEntities;
using Commons.Infrastructure;
using Importer.P4_Persisting.EntityDbFactories;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting
{
    public class Database
    {
        public Database()
        {
            using (var db = new RdContext())
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
            }
        }

        public async Task Persist(ReadActor[] actors, ReadStory[] stories, ReadFaction[] factions)
        {
            await using (var db = new RdContext())
            {
                foreach (var actor in actors)
                {
                    await db.ReadActorModels
                        .AddAsync(ReadActorModelFactory.CreateFrom(actor));
                }

                foreach (var story in stories)
                {
                    await db.ReadStoryModels
                        .AddAsync(ReadStoryModelFactory.CreateFrom(story));
                    await db.StoryToPlayerRelations
                        .AddRangeAsync(StoryToPlayerRelationFactory.CreateFrom(story));
                    await db.StoryToGmRelations
                        .AddRangeAsync(StoryToGmRelationFactory.CreateFrom(story));
                    await db.StoryToPrevChronoRelations
                        .AddRangeAsync(StoryToPrevChronoRelationFactory.CreateFrom(story));
                    await db.StoryToThreadRelations
                        .AddRangeAsync(StoryToThreadRelationFactory.CreateFrom(story));
                }

                foreach (var faction in factions)
                {
                    await db.ReadFactionModels
                        .AddAsync(ReadFactionModelFactory.CreateFrom(faction));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (ReadActor, ReadStory, ReadFaction)", count);
            }
        }

        public async Task Persist(AggregatedStory[] aggregatedStories, LocationRecord[] locationRecords, StoryBriefRecord[] briefs)
        {
            await using (var db = new RdContext())
            {
                foreach (var sm in aggregatedStories)
                {
                    await db.AggregatedStoryModels
                        .AddAsync(AggregatedStoryModelFactory.CreateFrom(sm));
                }
                foreach (var record in locationRecords)
                {
                    await db.LocationRecordModels
                        .AddAsync(LocationRecordModelFactory.CreateFrom(record));
                }

                foreach (var brief in briefs)
                {
                    await db.StoryBriefRecordModels
                        .AddAsync(StoryBriefRecordModelFactory.CreateFrom(brief));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (AggregatedStory, LocationRecord, StoryBriefRecord)", count);
            }
        }

        public async Task Persist(AggregatedActor[] aggregatedActors, ActorBriefRecord[] briefs, AggregatedFactionMerit[] aFactionMerits)
        {
            await using (var db = new RdContext())
            {
                foreach (var aa in aggregatedActors)
                {
                    await db.EveryActorModels
                        .AddAsync(AggregatedActorModelFactory.CreateFrom(aa));

                    foreach (var atar in aa.ActorToActorRelations)
                    {
                        await db.AtarRecordModels
                            .AddAsync(AtarRecordModelFactory.CreateAtarM(atar));
                    }
                    foreach (var progression in aa.Progressions)
                    {
                        await db.AggregatedProgressionModels
                            .AddAsync(AggregatedProgressionModelFactory.CreateFrom(progression));
                    }
                    foreach (var merit in aa.Merits)
                    {
                        await db.AggregatedMeritModels
                            .AddAsync(AggregatedMeritModelFactory.CreateFrom(merit));
                    }
                    foreach (var faction in aa.Factions)
                    {
                        await db.ActorToFactionModels
                            .AddAsync(ActorToFactionModelFactory.CreateFrom(aa.Name, faction));
                    }
                }

                foreach (var aid in briefs)
                {
                    await db.ActorBriefRecordModels.AddAsync(ActorBriefModelFactory.CreateFrom(aid));
                }

                foreach(var merit in aFactionMerits)
                {
                    await db.AggregatedFactionMeritModels
                        .AddAsync(AggregatedFactionMeritModelFactory.CreateFrom(merit));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (AggregatedActor, ActorBriefRecord, AggregatedFactionMerit)", count);
            }
        }

        public async Task Persist(AggregatedActorFlashcard[] flashcards)
        {
            await using (var db = new RdContext())
            {
                foreach (var flashcard in flashcards)
                {
                    await db.ActorFlashcardModels
                        .AddAsync(ActorFlashcardModelFactory.CreateFrom(flashcard));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (AggregatedActorFlashcard)", count);
            }
        }

        public async Task Persist(GeneralStoryBlock[] generalStoryBlocks, ActorThreadStoryBlock[] actorThreadStoryBlocks)
        {
            await using (var db = new RdContext())
            {
                foreach (var gStoryBlock in generalStoryBlocks)
                {
                    await db.GeneralStoryBlockModels
                        .AddAsync(GeneralStoryBlockModelFactory.CreateFrom(gStoryBlock));
                }

                foreach (var atStoryBlock in actorThreadStoryBlocks)
                {
                    await db.ActorThreadStoryBlockModels
                        .AddAsync(ActorThreadStoryBlockModelFactory.CreateFrom(atStoryBlock));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (GeneralStoryBlock, ActorThreadStoryBlock)", count);
            }
        }

        public async Task Persist(ReadThread[] readThreads, ReadMotive[] readMotives, 
            ThreadBriefRecord[] threadBriefRecords, MotiveBriefRecord[] motiveBriefRecords, FactionBriefRecord[] factionBriefRecords,
            AggregatedMotiveAcdeor[] aMotiveAcdeors)
        {
            await using (var db = new RdContext())
            {
                foreach (var rt in readThreads)
                {
                    await db.ReadThreadModels
                        .AddAsync(ReadThreadModelFactory.CreateFrom(rt));
                }

                foreach (var rm in readMotives)
                {
                    await db.ReadMotiveModels
                        .AddAsync(ReadModelModelFactory.CreateFrom(rm));
                }

                foreach (var tbr in threadBriefRecords)
                {
                    await db.ThreadBriefRecordModels
                        .AddAsync(ThreadBriefRecordModelFactory.CreateFrom(tbr));
                }

                foreach (var mbr in motiveBriefRecords)
                {
                    await db.MotiveBriefRecordModels
                        .AddAsync(MotiveBriefRecordModelFactory.CreateFrom(mbr));
                }

                foreach (var br in factionBriefRecords)
                {
                    await db.FactionBriefRecordModels
                        .AddAsync(FactionBriefRecordModelFactory.CreateFrom(br));
                }

                foreach (var ma in aMotiveAcdeors)
                {
                    await db.AggregatedMotiveAcdeorModels
                        .AddAsync(AggregatedMotiveAcdeorModelFactory.CreateFrom(ma));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (Threads and Motives)", count);
            }
        }

        public async Task Persist(LocationTreeNode locationTree)
        {
            await using (var db = new RdContext())
            {
                await db.LocationTreeJsonModels
                    .AddAsync(new LocationTreeJsonModel { Tree = locationTree.ToJson() });

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (LocationTreeNodes)", count);
            }
        }

        public async Task Persist(AtarRecord[] atfrs, AtarRecord[] ftars, AtarRecord[] ftfrs)
        {
            await using (var db = new RdContext())
            {
                foreach (var atfr in atfrs)
                {
                    await db.AtfrRecordModels
                        .AddAsync(AtarRecordModelFactory.CreateAtfrM(atfr));
                }

                foreach (var ftar in ftars)
                {
                    await db.FtarRecordModels
                        .AddAsync(AtarRecordModelFactory.CreateFtarM(ftar));
                }

                foreach (var ftfr in ftfrs)
                {
                    await db.FtfrRecordModels
                        .AddAsync(AtarRecordModelFactory.CreateFtfrM(ftfr));
                }

                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (Atfr, Ftar, Ftfr)", count);
            }
        }
        
        public async Task Persist(ReadActorCreationComponent[] actorCreationComponents)
        {
            await using (var db = new RdContext())
            {
                var archetypes = actorCreationComponents.Where(
                    c => c.ComponentType == ReadActorCreationComponentDefaults.ArchetypeComponentType).ToArray();
                var hearts = actorCreationComponents.Where(
                    c => c.ComponentType == ReadActorCreationComponentDefaults.HeartComponentType).ToArray();
                var auxiliaries = actorCreationComponents.Where(
                    c => c.ComponentType == ReadActorCreationComponentDefaults.AuxComponentType).ToArray();
                
                foreach (var archetype in archetypes)
                {
                    await db.ReadActorCreationArchetypeModels
                        .AddAsync(ReadActorCreationComponentModelFactory.CreateArchetypeFrom(archetype));
                }
                foreach (var heart in hearts)
                {
                    await db.ReadActorCreationHeartModels
                        .AddAsync(ReadActorCreationComponentModelFactory.CreateHeartFrom(heart));
                }
                foreach (var aux in auxiliaries)
                {
                    await db.ReadActorCreationAuxModels
                        .AddAsync(ReadActorCreationComponentModelFactory.CreateAuxFrom(aux));
                }
                
                int count = await db.SaveChangesAsync();
                Console.WriteLine("{0} records saved to database (ReadActorCreationComponent)", count);
            }
        }
    }
}
﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class StoryBriefRecordModelFactory
    {
        public static StoryBriefRecordModel CreateFrom(StoryBriefRecord brief)
        {
            return new StoryBriefRecordModel
            {
                StoryUid = brief.StoryUid,
                Title = brief.Title,
                Threads = brief.Threads,
                Motives = brief.Motives,
                StartDate = brief.StartDate,
                EndDate = brief.EndDate,
                Players = brief.Players
            };
        }
    }
}

﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ActorBriefModelFactory
    {
        public static ActorBriefRecordModel CreateFrom(ActorBriefRecord aid)
        {
            return new ActorBriefRecordModel
            {
                Name = aid.Name,
                Mechver = aid.Mechver,
                Intensity = aid.Intensity,
                Link = aid.Link
            };
        }
    }
}

﻿using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Importer.P4_Persisting.EntityDbFactories
{

    public static class StoryToPrevChronoRelationFactory
    {
        public static StoryToPrevChronoRelation[] CreateFrom(ReadStory story)
        {
            var col = new List<StoryToPrevChronoRelation>();
            foreach (var single in story.PrevChronoIds)
            {
                col.Add(new StoryToPrevChronoRelation
                {
                    StoryId = story.Uid,
                    PrevChronoId = single
                });
            }
            return col.ToArray();
        }
    }

    public static class StoryToGmRelationFactory
    {
        public static StoryToGmRelation[] CreateFrom(ReadStory story)
        {
            var col = new List<StoryToGmRelation>();
            foreach (var single in story.Gms)
            {
                col.Add(new StoryToGmRelation
                {
                    StoryId = story.Uid,
                    GmId = single
                });
            }
            return col.ToArray();
        }
    }

    public static class StoryToPlayerRelationFactory
    {
        public static StoryToPlayerRelation[] CreateFrom(ReadStory story)
        {
            var col = new List<StoryToPlayerRelation>();
            foreach (var single in story.Players)
            {
                col.Add(new StoryToPlayerRelation
                {
                    StoryId = story.Uid,
                    PlayerId = single
                });
            }
            return col.ToArray();
        }
    }

    public static class StoryToThreadRelationFactory
    {
        public static StoryToThreadRelation[] CreateFrom(ReadStory story)
        {
            var col = new List<StoryToThreadRelation>();
            foreach (string thread in story.Threads)
            {
                col.Add(new StoryToThreadRelation
                {
                    StoryId = story.Uid,
                    ThreadId = thread
                });
            }
            return col.ToArray();
        }
    }
}

﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class AggregatedMotiveAcdeorModelFactory
    {
        public static AggregatedMotiveAcdeorModel CreateFrom(AggregatedMotiveAcdeor ma)
        {
            return new AggregatedMotiveAcdeorModel { Actor = ma.Actor, OriginUid = ma.OriginUid, Deed = ma.Deed , EndDate = ma.EndDate };
        }
    }
}

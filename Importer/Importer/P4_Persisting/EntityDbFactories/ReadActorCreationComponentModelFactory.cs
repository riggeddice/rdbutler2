using System.Data.Common;
using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;

namespace Importer.P4_Persisting.EntityDbFactories;

public static class ReadActorCreationComponentModelFactory
{
    public static ReadActorCreationArchetypeModel CreateArchetypeFrom(ReadActorCreationComponent component)
    {
        return new ReadActorCreationArchetypeModel
        {
            Identifier = component.Identifier,
            Title = component.Title,
            Description = component.Description,
            Body = component.Body
        };
    }

    public static ReadActorCreationHeartModel CreateHeartFrom(ReadActorCreationComponent component)
    {
        return new ReadActorCreationHeartModel
        {
            Identifier = component.Identifier,
            Title = component.Title,
            Description = component.Description,
            Body = component.Body
        };
    }
    
    public static ReadActorCreationAuxModel CreateAuxFrom(ReadActorCreationComponent component)
    {
        return new ReadActorCreationAuxModel
        {
            Identifier = component.Identifier,
            Title = component.Title,
            Description = component.Description,
            Body = component.Body
        };
    }
    
}
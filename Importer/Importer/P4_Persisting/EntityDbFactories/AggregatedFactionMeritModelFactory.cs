﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class AggregatedFactionMeritModelFactory
    {
        public static AggregatedFactionMeritModel CreateFrom(AggregatedFactionMerit merit)
        {
            return new AggregatedFactionMeritModel
            {
                Actor = merit.Actor,
                Threads = merit.Threads,
                StartDate = merit.StartDate,
                EndDate = merit.EndDate,
                Deed = merit.Deed,
                OriginUid = merit.OriginUid
            };
        }
    }
}

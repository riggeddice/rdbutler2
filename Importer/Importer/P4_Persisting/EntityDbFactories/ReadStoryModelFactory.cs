﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ReadStoryModelFactory
    {
        public static ReadStoryModel CreateFrom(ReadStory story)
        {
            return new ReadStoryModel
            {
                Uid = story.Uid,
                Title = story.Title,
                Threads = string.Join(", ", story.Threads),
                Motives = string.Join(", ", story.Motives),
                PrevCampUid = string.Join(", ", story.PrevCampaignIds),
                Summary = story.Summary,
                Location = story.Location,
                TimeDuration = story.Time.Duration,
                TimeDelay = story.Time.Delay,
                TimeDate = story.Time.Date,
                Body = story.Body
            };
        }
    }
}

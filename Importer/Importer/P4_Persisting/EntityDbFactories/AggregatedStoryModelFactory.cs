﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class AggregatedStoryModelFactory
    {
        public static AggregatedStoryModel CreateFrom(AggregatedStory sm)
        {
            return new AggregatedStoryModel()
            {
                OriginatingStory = sm.StoryUid,
                OriginatingStoryTitle = sm.StoryTitle,
                Threads = string.Join(", ", sm.Threads),
                Motives = string.Join(", ", sm.Motives),
                StartDate = sm.StartDate,
                EndDate = sm.EndDate,
                PreviousStories = string.Join(", ", sm.PreviousStories),
                SequenceNumber = sm.SequenceNumber,
                Gms = string.Join(", ", sm.Gms),
                Players = string.Join(", ", sm.Players),
                PlayerActors = string.Join(", ", sm.PlayerActors),
                AllActors = string.Join(", ", sm.AllActors),
                Summary = sm.Summary,
                Body = sm.Body
            };
        }
    }
}

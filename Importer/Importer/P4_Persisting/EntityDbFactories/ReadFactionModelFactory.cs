﻿using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class ReadFactionModelFactory
    {
        public static ReadFactionModel CreateFrom(ReadFaction faction)
        {
            return new ReadFactionModel()
            {
                FactionId = faction.FactionId,
                Name = faction.Name,
                ShortDesc = faction.ShortDesc,
                Body = faction.Body
            };
        }
    }
}

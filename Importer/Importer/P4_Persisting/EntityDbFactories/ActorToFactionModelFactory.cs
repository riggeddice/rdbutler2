﻿using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ActorToFactionModelFactory
    {
        public static ActorToFactionModel CreateFrom(string actorName, string faction)
        {
            return new ActorToFactionModel
            {
                ActorName = actorName,
                Faction = faction
            };
        }
    }
}

﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ThreadBriefRecordModelFactory
    {
        public static ThreadBriefRecordModel CreateFrom(ThreadBriefRecord tbr)
        {
            return new ThreadBriefRecordModel
            {
                ThreadUid= tbr.ThreadUid,
                Name= tbr.Name,
                StartDate= tbr.StartDate,
                EndDate= tbr.EndDate,
                StoryCount= tbr.StoryCount,
                Players = string.Join(", ", tbr.Players),
                PlayerActors = string.Join(", ", tbr.PlayerActors),
                ShortDesc = tbr.ShortDesc
            };
        }
    }
}

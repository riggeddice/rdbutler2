﻿using Commons.CoreEntities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{

    public class AtarRecordModelFactory
    {
        public static AtarRecordModel CreateAtarM(AtarRecord atar)
        {
            return new AtarRecordModel
            {
                OriginatingActor = atar.TargetActor,
                RelevantActor = atar.RelevantActor,
                Intensity = atar.Intensity,
                StoryUids = string.Join(", ", atar.StoryUids)
            };
        }

        public static AtfrRecordModel CreateAtfrM(AtarRecord atfr)
        {
            return new AtfrRecordModel
            {
                OriginatingActor = atfr.TargetActor,
                RelevantActor = atfr.RelevantActor,
                Intensity = atfr.Intensity,
                StoryUids = string.Join(", ", atfr.StoryUids)
            };
        }

        public static FtarRecordModel CreateFtarM(AtarRecord ftar)
        {
            return new FtarRecordModel
            {
                OriginatingActor = ftar.TargetActor,
                RelevantActor = ftar.RelevantActor,
                Intensity = ftar.Intensity,
                StoryUids = string.Join(", ", ftar.StoryUids)
            };
        }

        public static FtfrRecordModel CreateFtfrM(AtarRecord ftfr)
        {
            return new FtfrRecordModel
            {
                OriginatingActor = ftfr.TargetActor,
                RelevantActor = ftfr.RelevantActor,
                Intensity = ftfr.Intensity,
                StoryUids = string.Join(", ", ftfr.StoryUids)
            };
        }
    }
}

using System.Linq;
using System.Xml.Linq;

namespace Importer.P3_Aggregation.Entities;

public class ReadFlashcardStoryMap
{
    public ReadFlashcardStoryMap(string storyUid, ReadFlashcard[] records)
    {
        StoryUid = storyUid;
        Records = records;
    }

    public string StoryUid { get; }

    public ReadFlashcard[] Records { get; }

    public override string ToString()
    {
        return $"Story: {StoryUid}, Records amount: {Records.Length}";
    }
}
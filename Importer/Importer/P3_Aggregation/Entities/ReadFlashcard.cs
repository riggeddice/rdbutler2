using Commons.CoreEntities;
using System.Threading;
using System;
using System.Linq;

namespace Importer.P3_Aggregation.Entities;

public class ReadFlashcard
{
    public ReadFlashcard(string actor, string[] bulletPoints)
    {
        Actor = actor;
        BulletPoints = bulletPoints;
    }

    public string Actor { get; }
    public string[] BulletPoints { get; }


    public override int GetHashCode() => HashCode.Combine(Actor, string.Join(", ", BulletPoints));
    public override bool Equals(object obj)
    {
        if (obj is not ReadFlashcard other) return false;

        if (Actor != other.Actor) return false;
        if (BulletPoints.SequenceEqual(other.BulletPoints) == false) return false;
        
        return true;
    }

    public override string ToString() => $"who: {Actor}: count: {BulletPoints.Length}";
    
}
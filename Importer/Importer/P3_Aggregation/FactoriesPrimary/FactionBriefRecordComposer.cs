﻿using Commons.CoreEntities;
using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class FactionBriefRecordComposer
    {
        public static FactionBriefRecord[] CreateMany(AggregatedFactionMerit[] merits, ReadFaction[] readFactions)
        {
            string[] allFactionNames = merits.Select(m => m.Actor).Concat(readFactions.Select(r => r.Name)).Distinct().ToArray();

            List<FactionBriefRecord> factionBriefs = new List<FactionBriefRecord>();
            foreach(string factionId in allFactionNames)
            {
                ReadFaction read = readFactions.Where(f => f.Name == factionId).FirstOrDefault();
                AggregatedFactionMerit[] relevantMerits = merits.Where(m => m.Actor == factionId).ToArray();
                factionBriefs.Add(CreateSingle(relevantMerits, read));
            }

            return factionBriefs.ToArray();
        }

        public static FactionBriefRecord CreateSingle(AggregatedFactionMerit[] relevantMerits, ReadFaction readFaction)
        {
            string name = string.Empty;
            if (readFaction == null) { name = relevantMerits[0].Actor; }
            else if (relevantMerits.Length == 0) { name = readFaction.Name; }
            else { name = readFaction.Name; }

            return new FactionBriefRecord(
                name: name,
                intensity: relevantMerits.Select(m => m.OriginUid).Distinct().Count(),
                link: readFaction != null ? readFaction.FactionId : name.ToActorUid(),
                shortDesc: readFaction != null ? readFaction.ShortDesc : string.Empty
                );
        }
    }
}

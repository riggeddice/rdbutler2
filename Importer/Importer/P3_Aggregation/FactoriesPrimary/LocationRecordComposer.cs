﻿using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public static class LocationRecordComposer
    {
        public static LocationRecord[] CreateManyFromStories(ReadStory[] stories, AggregatedStory[] storyInfos)
        {
            List<LocationRecord> records = new List<LocationRecord>();
            for (int i = 0; i < stories.Count(); i++) { records.AddRange(CreateManyFromStory(stories[i], storyInfos[i])); }
            return records.ToArray();
        }

        public static LocationRecord[] CreateManyFromStory(ReadStory story, AggregatedStory storyInfo)
        {
            string locationSection = SectionParser.ExtractLocationFromStory(story.Body);
            string[] locationStrings = locationSection.Split("\n").Select(s => s.TrimEnd()).ToArray();
            string[] validLocationStrings = locationStrings.Where(ls => ls.Contains("1. ")).ToArray();

            string[] actorDepths = validLocationStrings.Select(s => s.Split(":")[0]).ToArray();    // value: "        1. Sektor Astoriański"

            string origin = story.Uid;
            string[] actors = actorDepths.Select(ad => ad.Trim().Substring(3)).ToArray();
            string[] paths = LocationRecordTools.CreatePathsFromActorDepths(actorDepths);
            string[] deeds = locationStrings.Select(ls => SectionParser.ExtractDeedsFromLocationRecord(ls)).ToArray();
            string endDate = storyInfo.EndDate;

            List<LocationRecord> locationRecords = new List<LocationRecord>();
            for (int i = 0; i < validLocationStrings.Length; i++)
            {
                locationRecords.Add(new LocationRecord(origin, actors[i], deeds[i], paths[i], endDate));
            }
            return locationRecords.ToArray();
        }
    }

    public static class LocationRecordTools
    {
        public static string[] CreatePathsFromActorDepths(string[] actorDepths)
        {
            List<string> paths = new List<string>();
            for (int i = 0; i < actorDepths.Length; i++) { paths.Add(CreatePathForActorDepth(i, actorDepths)); }
            return paths.ToArray();
        }

        public static string CreatePathForActorDepth(int index, string[] allActorDepths)
        {
            List<string> paths = new List<string>();
            int prevDepth = int.MaxValue;
            for (int i = index; i >= 0; i--)
            {
                string considered = allActorDepths[i];
                int whitespaces = considered.TakeWhile(Char.IsWhiteSpace).Count();

                if (prevDepth > whitespaces)
                {
                    prevDepth = whitespaces;
                    considered = considered.Trim().Substring(3);
                    paths.Add(considered + "|");
                }
            }

            paths.Reverse();
            return string.Join("", paths);
        }
    }
}

﻿using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public static class MotiveBriefRecordComposer
    {
        public static MotiveBriefRecord[] CreateMany(AggregatedStory[] aggregatedStories, ReadMotive[] readMotives)
        {
            // Change of plans - I want to see **ALL** motives, no matter how malformed.
            // Both those which exist in Read form and those that exist only in the stories.
            string[] allMotiveNamesInStories = aggregatedStories.SelectMany(s => s.Motives).Distinct().ToArray();
            string[] allMotiveNamesRead = readMotives.Select(s => s.TagUid).ToArray();

            string[] allMotivesToConsider = allMotiveNamesRead.Concat(allMotiveNamesInStories).Distinct().ToArray();

            // Proceed.
            List<MotiveBriefRecord> records = new List<MotiveBriefRecord>();
            foreach (var motiveId in allMotivesToConsider)
            {
                // Edge case: unknown motive (how I denote stories with unknown / unselected motives).
                if (motiveId == "?") continue;

                AggregatedStory[] relevantStories = aggregatedStories.Where(s => s.Motives.Contains(motiveId)).ToArray();
                ReadMotive linkedMotive = readMotives.Where(s => s.TagUid.Equals(motiveId)).FirstOrDefault();

                // Now here we assume we are dealing with defaults - there is no linked story.
                string startDate = StoryDefaults.ErrorenousDate.ToString("yyyy-MM-dd");
                string endDate = StoryDefaults.ErrorenousDate.ToString("yyyy-MM-dd");
                int storyCount = 0;
                string[] players = [];
                string[] playerActors = [];

                // But if the linked stories exist...
                if (relevantStories.Length > 0)
                {
                    startDate = relevantStories.Select(s => s.StartDate).Min();
                    endDate = relevantStories.Select(s => s.EndDate).Max();
                    storyCount = relevantStories.Count();
                    players = relevantStories.SelectMany(s => s.Players).Distinct().ToArray();
                    playerActors = relevantStories.SelectMany(s => s.PlayerActors).Distinct().ToArray();
                }

                if (linkedMotive == null) throw new ArgumentNullException($"Motive with id: {motiveId} does not exist in data; probable story: {relevantStories[0].StoryUid}");
                
                records.Add(new MotiveBriefRecord(
                    uid: motiveId,
                    name: linkedMotive.Name,
                    startDate: startDate,
                    endDate: endDate,
                    storyCount: storyCount,
                    players: players,
                    playerActors: playerActors,
                    shortDesc: linkedMotive != null ? linkedMotive.ShortDesc : string.Empty
                    ));
            }

            return records.ToArray();
        }

    }
}

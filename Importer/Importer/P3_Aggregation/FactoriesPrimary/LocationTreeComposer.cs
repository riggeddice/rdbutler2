﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbEntityFactories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class LocationTreeComposer
    {
        public static LocationTreeNode Create(LocationRecord[] locationRecords)
        {

            LocationRecord[] uniqueRecords = locationRecords
                .GroupBy(r => r.Path)
                .Select(g => g.First())
                .ToArray();

            return ConvertToTree(uniqueRecords);
        }

        public static LocationTreeNode ConvertToTree(LocationRecord[] uniquePathRecords)
        {
            // phase 1: build flat locationTree records in the sequence guaranteeing 
            // that 'there is always a parent record'.
            LocationTreeNode[] nodes = uniquePathRecords
                .OrderBy(r => r.Path)
                .Select(r => new LocationTreeNode() { Title = r.Name, Path = r.Path, Children = new List<LocationTreeNode>() })
                .ToArray();

            // phase 2: create a dict mapping path to locationTree records;
            Dictionary<string, LocationTreeNode> mapper = new();
            foreach (var node in nodes) { mapper.Add(node.Path, node); };

            // phase 3: create a recursive structure knowing (dict) the parents 
            var root = nodes.FirstOrDefault();
            var others = nodes.Skip(1).ToArray();

            foreach (var node in others)
            {
                string pathWithoutLastTerminator = node.Path[..^1];
                string parentPath = pathWithoutLastTerminator.Substring(0, pathWithoutLastTerminator.LastIndexOf("|") + 1);
                var parent = mapper[parentPath];
                parent.Children.Add(node);
            }

            return root;
        }
    }
}

﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class AggregatedMotiveAcdeorComposer
    {
        public static AggregatedMotiveAcdeor[] Create(MotiveAcdeor[] motiveAcdeors, AggregatedStory[] aggregatedStories)
        {
            AggregatedMotiveAcdeor[] aMotiveAcdeors = motiveAcdeors
                .Select(ma => CreateSingle(ma, aggregatedStories.Where(ags => ags.StoryUid == ma.OriginUid).FirstOrDefault()))
                .ToArray();

            return aMotiveAcdeors;
        }

        public static AggregatedMotiveAcdeor CreateSingle(MotiveAcdeor motiveAcdeor, AggregatedStory aggregatedStory)
        {
            return new AggregatedMotiveAcdeor(
                originUid: motiveAcdeor.OriginUid,
                actor: motiveAcdeor.Actor,
                deed: motiveAcdeor.Deed,
                endDate: aggregatedStory.EndDate
                );
        }
    }
}

﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class GeneralStoryBlockComposer
    {
        public static GeneralStoryBlock[] CreateMany(AggregatedStory[] allStories)
        {
            return allStories.Select(s => CreateSingle(s)).ToArray();
        }

        public static GeneralStoryBlock CreateSingle(AggregatedStory story)
        {
            return new GeneralStoryBlock(
                storyUid: story.StoryUid, 
                storyTitle: story.StoryTitle, 
                startDate: story.StartDate, 
                endDate: story.EndDate, 
                threads: story.Threads,
                motives: story.Motives,
                summary: story.Summary, 
                actorsPresent: story.AllActors);
        }
    }
}

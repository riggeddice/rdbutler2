﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class StoryBriefRecordComposer
    {
        public static StoryBriefRecord[] CreateMany(AggregatedStory[] aggregatedStories)
        {
            return aggregatedStories.Select(s => CreateSingle(s)).ToArray();
        }

        public static StoryBriefRecord CreateSingle(AggregatedStory aggregatedStory)
        {
            return new StoryBriefRecord(
                storyUid: aggregatedStory.StoryUid,
                title: aggregatedStory.StoryTitle,
                threads: string.Join(", ", aggregatedStory.Threads),
                motives: string.Join(", ", aggregatedStory.Motives),
                startDate: aggregatedStory.StartDate,
                endDate: aggregatedStory.EndDate,
                players: string.Join(", ", aggregatedStory.Players)
                );
        }
    }
}

﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Importer.P3_Aggregation.FactoriesSupportive
{

    public class AggregatedMeritComposer
    {
        public static AggregatedMerit[] CreateMany(ActorMerit[] merits, AggregatedStory[] relevantStoryMetas)
        {
            List<AggregatedMerit> aggregatedMerits = new();
            foreach (ActorMerit actorMerit in merits)
            {
                AggregatedStory relevantMeta = relevantStoryMetas.Where(rm => rm.StoryUid.Equals(actorMerit.OriginUid)).First();
                aggregatedMerits.Add(CreateSingle(actorMerit, relevantMeta));
            }

            return aggregatedMerits.OrderBy(am => am.EndDate).ToArray();
        }

        public static AggregatedMerit CreateSingle(ActorMerit actorMerit, AggregatedStory relevantMeta)
        {
            return new AggregatedMerit(
                originUid: actorMerit.OriginUid,
                actor: actorMerit.Actor,
                deed: actorMerit.Deed,
                threads: string.Join(", ", relevantMeta.Threads), // todo: possibly pass array instead of string
                startDate: relevantMeta.StartDate,
                endDate: relevantMeta.EndDate
                );
        }
    }
}

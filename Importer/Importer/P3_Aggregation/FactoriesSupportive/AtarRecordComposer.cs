﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;

namespace Importer.P3_Aggregation.FactoriesSupportive
{

    public static class AtarRecordComposer
    {

        /// <summary>
        /// Creates an Entity-to-Entity record map (AtAr, FtAr...) based on two entities being in the same Stories.
        /// </summary>
        /// <param name="originatingEntity">For which Actor or Faction we are building an Atar map? Example: "Felina Amatanir" or "Wolny Uśmiech"</param>
        /// <param name="originatingMerits">All merits of the TYPE of originating Actor/Faction. So for "Felina" it would be ActorMerits, for faction - FactionMerits</param>
        /// <param name="linkingMerits">All merits of the TYPE of linked Actor/Faction. So for atFr it would be FactionMerits, for atAr - ActorMerits</param>
        /// <returns></returns>
        public static AtarRecord[] CreateFor(string originatingEntity, AcdeorRecord[] originatingMerits, AcdeorRecord[] linkingMerits)
        {
            // We are building a variant on ATARs (entity-to-entity), thus first we need to get the stories
            // where originatingEntity was present at. OriginatingEntity is further coded as 'target'.
            string[] targetStoryUids = originatingMerits
                .Where(m => m.Actor.Equals(originatingEntity))
                .Select(m => m.OriginUid)
                .ToArray();

            // Now we extract the names of linked entities which correspond to the stories where targetEntity was present in
            List<AcdeorRecord> meritsOfEntitiesConnectedToTarget = [];
            foreach (var storyId in targetStoryUids)
            {
                AcdeorRecord[] linkedEntitiesInStory = linkingMerits
                    .Where(m => m.OriginUid.Equals(storyId))
                    .Where(m => m.Actor.Equals(originatingEntity) == false)
                    .ToArray();
                meritsOfEntitiesConnectedToTarget.AddRange(linkedEntitiesInStory);
            }

            List<string> allConnectedLinkedEntitiesNames = meritsOfEntitiesConnectedToTarget
                .Select(m => m.Actor)
                .Distinct()
                .ToList();

            // We know we have only those connected LinkedEntity names which have at least one story in common with OriginatingEntity
            // 'Atar' is shorthand for 'entity-to-entity', not just 'actor', because Actor and Faction are 'Actors' in domain language
            List<AtarRecord> atars = [];
            foreach (string linkedEntityName in allConnectedLinkedEntitiesNames)
            {
                string[] uids = meritsOfEntitiesConnectedToTarget
                    .Where(m => m.Actor == linkedEntityName)                    // merits for this linkedEntity
                    .Where(m => targetStoryUids.Contains(m.OriginUid))          // only those where the OriginatingEntity was present
                    .Select(m => m.OriginUid)                                   // only uids
                    .ToArray();

                atars.Add(new AtarRecord(originatingEntity, linkedEntityName, uids.Length, uids));
            }

            AtarRecord[] presentableAtars = atars.OrderByDescending(a => a.Intensity).ThenBy(a => a.RelevantActor).ToArray();
            return presentableAtars;
        }
    }
}

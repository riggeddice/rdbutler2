﻿using System;

namespace Importer.P2_Extraction.Entities
{
    /// <summary>
    /// Actor - Deed - Origin record. Actor who did X, actual Deed, Origin (f.ex. Story) of the AcDe.
    /// </summary>
    public class AcdeorRecord(string originUid, string actor, string deed)
    {
        public string OriginUid { get; } = originUid;
        public string Actor { get; } = actor;
        public string Deed { get; } = deed;

        public override int GetHashCode() => HashCode.Combine(OriginUid, Actor, Deed);
        public override bool Equals(object obj)
        {

            if (obj is not AcdeorRecord other) return false;

            if (OriginUid != other.OriginUid) return false;
            if (Actor != other.Actor) return false;
            if (Deed != other.Deed) return false;

            return true;
        }

        public override string ToString()
        {
            return $"Id: {OriginUid}, Actor: {Actor}, Deed: {Deed}";
        }
    }
}

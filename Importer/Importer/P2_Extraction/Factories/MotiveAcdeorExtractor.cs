﻿using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P2_Extraction.Extractors
{
    public class MotiveAcdeorExtractor
    {
        public static MotiveAcdeor[] CreateManyFromStories(ReadStory[] stories)
        {
            List<MotiveAcdeor> motives = [];
            foreach (var story in stories) { motives.AddRange(CreateManyFromStory(story)); }
            return motives.ToArray();
        }

        public static MotiveAcdeor[] CreateManyFromStory(ReadStory story)
        {
            string storyVision = SectionParser.ExtractSectionFromStoryBody(3, 3, "Projekt Wizji Sesji", story.Body);
            string[] records = SectionParser.ExtractMotiveSplitRecordsFromStoryVision(storyVision);

            List<MotiveAcdeor> merits = [];
            foreach (var record in records) { merits.Add(CreateSingleFromRecord(story.Uid, record)); }
            return merits.ToArray();
        }

        public static MotiveAcdeor CreateSingleFromRecord(string originatingStoryUid, string record)
        {
            string[] components = SectionParser.ExtractComponentsFromAcdeorString(record);
            return new MotiveAcdeor(originatingStoryUid: originatingStoryUid, actor: components[0], deed: components[1]);
        }
    }
}

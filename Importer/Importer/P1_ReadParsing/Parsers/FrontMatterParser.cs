using System.Text.RegularExpressions;

namespace Importer.P1_ReadParsing.Parsers;

public static class FrontMatterParser
{
    /// <summary>
    /// This function takes a string and extracts the content of a `front matter` from it, without the `---` separators.
    /// Example: "---\n title: Cat\n key: cat\n---\n# {{title}}" -> "title: Cat\n key: cat" 
    /// </summary>
    /// <param name="document">Any string, preferably a markdown document with front matter</param>
    /// <returns>Front matter internals if front matter present, null if no front matter present</returns>
    public static string ExtractFrontMatter(string document)
    {
        const string frontMatterPattern = @"---\s*(.*?)\s*---";
        Match match = Regex.Match(document, frontMatterPattern, RegexOptions.Singleline);
        return match.Success ? match.Groups[1].Value.Trim() : null;
    }


    /// <summary>
    /// This function takes a string and if there is a key in the `front matter`, it returns that key. Else, returns null.
    /// </summary>
    /// <param name="keyToExtract">Key to be extracted. Example: 'title'</param>
    /// <param name="document">Any string, preferably a markdown document with front matter</param>
    /// <returns>Value of a key if present, null if no front matter or if no key in front matter</returns>
    public static string ExtractSingleKey(string keyToExtract, string document)
    {
        string frontMatter = ExtractFrontMatter(document);
        if(frontMatter == null) return null;
        
        string pattern = @$"{keyToExtract}:\s*(.+)";
        Match match = Regex.Match(frontMatter, pattern);
        return match.Success ? match.Groups[1].Value.Trim() : null;
    }

    
    /// <summary>
    /// A facade method, extracting value of a 'title' key from any document front matter or 'null' if not present.
    /// </summary>
    public static string ExtractTitle(string document) => ExtractSingleKey("title", document);
    
    /// <summary>
    /// A facade method, extracting value of a 'key' key from any document front matter or 'null' if not present.
    /// 'key' is VERY often used as unique identifier in Eleventy. 
    /// </summary>
    public static string ExtractKey(string document) => ExtractSingleKey("key", document);
    
    /// <summary>
    /// A facade method, extracting value of a 'description' key from any document front matter or 'null' if not present.
    /// </summary>
    public static string ExtractDescription(string document) => ExtractSingleKey("description", document);
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Entities
{
    
    /// <summary>
    /// Thread and Motive are two distinct types of StoryTags. They differ in the domain, however they duplicate
    /// every field and meaning out there.
    /// </summary>
    public class ReadStoryTag
    {
        public ReadStoryTag(string tagUid, string name, string shortDesc, string fullDesc, string spoilers)
        {
            TagUid = tagUid;
            Name = name;
            ShortDesc = shortDesc;
            FullDesc = fullDesc;
            Spoilers = spoilers;
        }

        public string TagUid { get; }
        public string Name { get; }
        public string ShortDesc { get; }
        public string FullDesc { get; }
        public string Spoilers { get; }

        public override string ToString()
        {
            return $"{TagUid}: {Name}, ShortDesc: {ShortDesc}";
        }

        public override int GetHashCode() => HashCode.Combine(
            TagUid, Name, ShortDesc, FullDesc, Spoilers);

        public override bool Equals(object obj)
        {
            if (!(obj is ReadStoryTag other)) return false;

            if (TagUid != other.TagUid) return false;
            if (Name != other.Name) return false;
            if (ShortDesc != other.ShortDesc) return false;
            if (FullDesc != other.FullDesc) return false;
            if (Spoilers!= other.Spoilers) return false;

            return true;
        }
    }

    public class ReadThread : ReadStoryTag
    {
        public ReadThread(string threadUid, string name, string shortDesc, string fullDesc, string spoilers) : base(threadUid, name, shortDesc, fullDesc, spoilers) { }
    }

    public class ReadMotive : ReadStoryTag
    {
        public ReadMotive(string motiveUid, string name, string shortDesc, string fullDesc, string spoilers) : base(motiveUid, name, shortDesc, fullDesc, spoilers) { }
    }


}

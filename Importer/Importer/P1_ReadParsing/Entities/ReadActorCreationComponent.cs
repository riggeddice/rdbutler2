using System;

namespace Importer.P1_ReadParsing.Entities;

/// <summary>
/// This is a Component of type: Archetype, Heart or Aux. Those are used to create an Actor who is a PlayerCharacter,
/// they are also used while making RPG Flashcards.
/// </summary>
/// <param name="Identifier">Unique identifier of this component throughout components, like "arystokrata-aurum".</param>
/// <param name="Title">Human-readable name of the document, like "Arystokrata Aurum".</param>
/// <param name="Description">The short description text in the front matter of this document.</param>
/// <param name="ComponentType">What type of Component it is: 'Archetype' || 'Heart' || 'Aux'.</param>
/// <param name="Body">Whole content of the originating document.</param>
public record ReadActorCreationComponent(string Identifier, string Title, string Description,
    string ComponentType, string Body)
{
    public virtual bool Equals(ReadActorCreationComponent other)
    {
        if (other is null) return false;

        return Identifier == other.Identifier &&
               Title == other.Title &&
               Description == other.Description &&
               ComponentType == other.ComponentType;
    }

    public override int GetHashCode() =>
        HashCode.Combine(Identifier, Title, Description, ComponentType);
}

public abstract record ReadActorCreationComponentDefaults
{
    public const string ArchetypeComponentType = "Archetype";
    public const string HeartComponentType = "Heart";
    public const string AuxComponentType = "Aux";
}
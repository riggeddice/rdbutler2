using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;

namespace Importer.P1_ReadParsing.Factories;

public static class ReadActorCreationComponentFactory
{
    public static ReadActorCreationComponent CreateSingleArchetype(FileRecord record) => 
        _CreateSingleComponentOfSetType(record, componentType: ReadActorCreationComponentDefaults.ArchetypeComponentType);
    
    public static ReadActorCreationComponent CreateSingleHeart(FileRecord record) => 
        _CreateSingleComponentOfSetType(record, componentType: ReadActorCreationComponentDefaults.HeartComponentType);
    
    public static ReadActorCreationComponent CreateSingleAux(FileRecord record) => 
        _CreateSingleComponentOfSetType(record, componentType: ReadActorCreationComponentDefaults.AuxComponentType);


    private static ReadActorCreationComponent _CreateSingleComponentOfSetType(FileRecord record, string componentType)
    {
        string identifier = FrontMatterParser.ExtractKey(record.Content);
        string title = FrontMatterParser.ExtractTitle(record.Content);
        string description = FrontMatterParser.ExtractDescription(record.Content);
        
        return new ReadActorCreationComponent(
            Identifier: identifier,
            Title: title,
            Description: description,
            ComponentType: componentType,
            Body: record.Content
        );
    }
}
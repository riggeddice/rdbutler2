﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Factories
{
    public static class ReadStoryTagFactory
    {
        /// <summary>
        /// Assuming we have two sources - gitlab and eleventy - they have different paths and different parsing structure.
        /// Therefore we need to consider them separately and parse them in different ways.
        /// </summary>
        /// <param name="gitlabRecords"></param>
        /// <param name="eleventyRecords"></param>
        /// <returns></returns>
        public static ReadStoryTag[] CreateManyFromDifferentSources(FileRecord[] gitlabRecords, FileRecord[] eleventyRecords)
        {
            var gitlabTags = CreateManyFromGitlab(gitlabRecords);
            var eleventyTags = CreateManyFromEleventy(eleventyRecords);

            var gitlabIdentifiers = gitlabTags.Select(s => s.TagUid);
            var eleventyIdentifiers = eleventyTags.Select(s => s.TagUid);
            
            var duplicatedIdentifiers = gitlabIdentifiers.Intersect(eleventyIdentifiers).ToArray();

            if (duplicatedIdentifiers.Length != 0)
            {
                throw new ArgumentException("Duplicated StoryTags: " + string.Join(", ", duplicatedIdentifiers));
            }

            return gitlabTags.Concat(eleventyTags).ToArray();
        }
        
        // Eleventy

        public static ReadStoryTag[] CreateManyFromEleventy(FileRecord[] eleventyRecords)
        {
            return eleventyRecords
                .Select(CreateSingleFromEleventy)
                .ToArray();
        }

        public static ReadStoryTag CreateSingleFromEleventy(FileRecord record)
        {
            string identifier = IdentifierParser.ExtractFromEleventyPath(record.Path);
            string name = FrontMatterParser.ExtractTitle(record.Content);
            string shortDesc = FrontMatterParser.ExtractDescription(record.Content);
            string fullDesc = SectionParser.ExtractSectionFromStoryBody(2, 2, "Opis", record.Content);
            string spoilers = SectionParser.ExtractSectionFromStoryBody(2, 2, "Spoilers", record.Content);
            
            return new ReadStoryTag(tagUid: identifier, name: name, shortDesc: shortDesc, fullDesc: fullDesc, spoilers: spoilers);
        }

        // Gitlab
        
        public static ReadStoryTag[] CreateManyFromGitlab(FileRecord[] gitlabRecords)
        {
            return gitlabRecords
                .Select(CreateSingleFromGitlab)
                .ToArray();
        }

        public static ReadStoryTag CreateSingleFromGitlab(FileRecord record)
        {
            string identifier = IdentifierParser.ExtractStoryTagIdentifier(record.Path);
            string name = MetadataSectionParser.ExtractSingleParamFromStory("Nazwa", record.Content);
            string shortDesc = SectionParser.ExtractSectionFromStoryBody(2, 2, "Krótki opis", record.Content);
            string fullDesc = SectionParser.ExtractSectionFromStoryBody(2, 2, "Opis", record.Content);
            string spoilers = SectionParser.ExtractSectionFromStoryBody(2, 2, "Spoilers", record.Content);

            return new ReadStoryTag(tagUid: identifier, name: name, shortDesc: shortDesc, fullDesc: fullDesc, spoilers: spoilers);
        }
    }
}

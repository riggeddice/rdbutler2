﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Factories
{
    public static class ReadFactionFactory
    {
        public static ReadFaction[] CreateManyFromDifferentSources(FileRecord[] gitlabRecords, FileRecord[] eleventyRecords)
        {
            ReadFaction[] gitlabFactions = CreateManyFromGitlab(gitlabRecords);
            ReadFaction[] eleventyFactions = CreateManyFromEleventy(eleventyRecords);
            
            ReadFaction[] allFactions = gitlabFactions
                .Concat(eleventyFactions)
                .GroupBy(f => f.FactionId)
                .Select(g => g.Last()) // This will prioritize eleventy faction over gitlab one.
                .ToArray();

            return allFactions;
        }

        // === Eleventy section ===
        
        public static ReadFaction[] CreateManyFromEleventy(FileRecord[] eleventyRecords)
        {
            var valid = eleventyRecords
                .Where(f => IdentifierParser.ExtractFromEleventyPath(f.Path) != string.Empty);

            return valid.Select(CreateSingleFromEleventy).ToArray();
        }

        private static ReadFaction CreateSingleFromEleventy(FileRecord record)
        {
            string identifier = IdentifierParser.ExtractFromEleventyPath(record.Path);
            string name = FrontMatterParser.ExtractTitle(record.Content);
            string shortdesc = FrontMatterParser.ExtractDescription(record.Content);
            string body = record.Content;
            return new ReadFaction(identifier: identifier, name: name, shortdesc: shortdesc, body: body);
        }

        // === Gitlab section ===
        
        public static ReadFaction[] CreateManyFromGitlab(FileRecord[] gitlabRecords)
        {
            var valid = gitlabRecords
                .Where(f => IdentifierParser.ExtractActorIdentifier(f.Path) != string.Empty);

            return valid.Select(CreateSingleFromGitlab)
                .ToArray();
        }

        public static ReadFaction CreateSingleFromGitlab(FileRecord record)
        {
            string identifier = IdentifierParser.ExtractActorIdentifier(record.Path)[5..];
            string name = MetadataSectionParser.ExtractSingleParamFromStory("name", record.Content);
            string shortdesc = MetadataSectionParser.ExtractSingleParamFromStory("shortdesc", record.Content);
            string body = record.Content;
            return new ReadFaction(identifier: identifier, name: name, shortdesc: shortdesc, body: body);
        }

        
    }
}

﻿using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Factories
{
    public static class GlobalTimeFactory
    {
        public static GlobalTime CreateSingle(string dates)
        {
            string[] singleDates = dates.Split("\n").Select(d => d.Trim()).ToArray();
            var parsedRecords = singleDates.Select(sd => SectionParser.ExtractComponentsFromAcdeorString(sd)).ToArray();
            var validRecords = parsedRecords.Where(r => !string.IsNullOrWhiteSpace(r[0]) && !string.IsNullOrWhiteSpace(r[1])).ToArray();
            var datetimes = validRecords.Select(r => DateTime.ParseExact(r[1], "yyyyMMdd", CultureInfo.InvariantCulture)).ToArray();

            GlobalTime timeDict = new GlobalTime();
            for (int i = 0; i < validRecords.Length; i++) { timeDict.Add(validRecords[i][0], datetimes[i]); }

            return timeDict;
        }
    }
}

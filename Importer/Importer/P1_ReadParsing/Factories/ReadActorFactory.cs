﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System.Linq;

namespace Importer.P1_ReadParsing.Factories
{
    public static class ReadActorFactory
    {
        public static ReadActor[] CreateMany(FileRecord[] records)
        {
            var valid = records
                .Where(f => IdentifierParser.ExtractActorIdentifier(f.Path) != string.Empty);

            return valid.Select(f => CreateSingle(f))
                .Where(f => f.Uid.Contains("_template") == false)  // no templates please
                .ToArray();
        }

        public static ReadActor CreateSingle(FileRecord record)
        {
            string identifier = IdentifierParser.ExtractActorIdentifier(record.Path);
            string mechver = identifier.Substring(0, 4);
            string name = MetadataSectionParser.ExtractSingleParamFromStory("title", record.Content);
            string[] factions = MetadataSectionParser.ExtractManyParamsFromStory("factions", record.Content);
            string owner = MetadataSectionParser.ExtractSingleParamFromStory("owner", record.Content);
            string body = record.Content;
            return new ReadActor(identifier: identifier, name: name, mechver: mechver,
                factions: factions, owner: owner, body: body);
        }
    }
}

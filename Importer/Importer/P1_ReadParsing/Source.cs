﻿using Commons.Config;
using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Importer.P1_ReadParsing
{
    public class Source
    {
        AppConfigProvider _provider;

        public Source()
        {
            _provider = new AppConfigProvider();
        }

        public ReadActor[] ProvideActors()
        {
            string path = _provider.GetPathToActorFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadActor[] actors = ReadActorFactory.CreateMany(fileRecords);
            return actors;
        }

        public ReadFaction[] ProvideFactions()
        {
            string rdPath = _provider.GetPathToFactionFolder();
            FileRecord[] gitlabRecords = FileOps.ReadFiles(rdPath);
            
            string eleventyPath = _provider.GetPathToEleventyFactionFolder();
            FileRecord[] eleventyRecords = FileOps.ReadEleventyFiles(eleventyPath);
            
            ReadFaction[] factions = ReadFactionFactory.CreateManyFromDifferentSources(gitlabRecords, eleventyRecords);
            return factions;
        }

        public GlobalTime ProvideTime()
        {
            string path = _provider.GetPathToStoryFolder();     // time is located in Story folder
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            GlobalTime globalTime = GlobalTimeFactory.CreateSingle(fileRecords.Where(fr => fr.Path.Contains("_kluczowe-daty.md")).First().Content);
            return globalTime;
        }

        public ReadStory[] ProvideStories()
        {
            string path = _provider.GetPathToStoryFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadStory[] stories = ReadStoryFactory.CreateMany(fileRecords);
            return stories;
        }

        public ReadThread[] ProvideThreads()
        {
            string path = _provider.GetPathToStoryThreadFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadStoryTag[] threads = ReadStoryTagFactory.CreateManyFromGitlab(fileRecords);
            return threads.Select(s => new ReadThread(s.TagUid, s.Name, s.ShortDesc, s.FullDesc, s.Spoilers)).ToArray();
        }

        public ReadMotive[] ProvideMotives()
        {
            string rdMotivesPath = _provider.GetPathToStoryMotiveFolder();
            FileRecord[] gitlabRecords = FileOps.ReadFiles(rdMotivesPath);
            
            string ezMotivesPath = _provider.GetPathToEzStoryMotiveFolder();
            FileRecord[] eleventyMotivesRecords = FileOps.ReadEleventyFiles(ezMotivesPath);
            
            string ezAgendasPath = _provider.GetPathToEzStoryAgendasFolder();
            FileRecord[] eleventyAgendasRecords = FileOps.ReadEleventyFiles(ezAgendasPath);
            
            FileRecord[] allEleventyMotives = eleventyMotivesRecords.Concat(eleventyAgendasRecords).ToArray();
            
            ReadStoryTag[] motives = ReadStoryTagFactory.CreateManyFromDifferentSources(gitlabRecords, allEleventyMotives);
            return motives.Select(s => new ReadMotive(s.TagUid, s.Name, s.ShortDesc, s.FullDesc, s.Spoilers)).ToArray();
        }

        public ReadActorCreationComponent[] ProvideActorCreationComponents()
        {
            // Here we have three types of Components: Archetypes, Motives, Aux.
            string archetypePath = _provider.GetPathToActorCreationArchetypeFolder();
            string heartPath = _provider.GetPathToActorCreationHeartFolder();
            string auxPath = _provider.GetPathToActorCreationAuxFolder();

            FileRecord[] archetypeRecords = FileOps.ReadEleventyFiles(archetypePath);
            FileRecord[] heartRecords = FileOps.ReadEleventyFiles(heartPath);
            FileRecord[] auxRecords = FileOps.ReadEleventyFiles(auxPath);
            
            List<ReadActorCreationComponent> components = [];
            components.AddRange(archetypeRecords.Select(ReadActorCreationComponentFactory.CreateSingleArchetype));
            components.AddRange(heartRecords.Select(ReadActorCreationComponentFactory.CreateSingleHeart));
            components.AddRange(auxRecords.Select(ReadActorCreationComponentFactory.CreateSingleAux));

            return components.ToArray();
        }
    }
}
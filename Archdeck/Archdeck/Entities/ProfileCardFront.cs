﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Archdeck.Entities
{
    public class ProfileCardFront
    {
        public ProfileCardFront(string name, string actions, string hunger, string howActs, string resources)
        {
            Name = name;
            Actions = actions;
            Hunger = hunger;
            HowActs = howActs;
            Resources = resources;
        }

        public string Name { get; }
        public string Actions { get; }
        public string Hunger { get; }
        public string HowActs { get; }
        public string Resources { get; }

        public override string ToString()
        {
            return $"PA Front: {Name}";
        }
    }

    public static class ProfileCardFrontFactory
    {
        public static ProfileCardFront[] Create(ProfileArchetype[] profileArchetypes)
        {
            List<ProfileCardFront> fronts = new List<ProfileCardFront>();
            foreach (ProfileArchetype pa in profileArchetypes)
            {
                fronts.Add(Create(pa));
            }
            return fronts.ToArray();
        }

        public static ProfileCardFront Create(ProfileArchetype pa)
        {
            return new ProfileCardFront(pa.Name, pa.Actions, pa.Hunger, pa.HowActs, pa.Resources);
        }
    }

}

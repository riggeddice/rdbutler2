﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Archdeck.Entities
{
    public class ProfileCardBack
    {
        public ProfileCardBack(string name, string whatis, string tags, string examples)
        {
            this.Name = name;
            this.Whatis = whatis;
            this.Tags = tags;
            this.Examples = examples;
        }

        public string Name { get; }
        public string Whatis { get; }
        public string Tags { get; }
        public string Examples { get; }

        public override string ToString()
        {
            return $"PA Back: {Name}";
        }
    }

    public static class ProfileCardBackFactory
    {
        public static ProfileCardBack[] Create(ProfileArchetype[] profileArchetypes)
        {
            List<ProfileCardBack> backs = new List<ProfileCardBack>();
            foreach (ProfileArchetype pa in profileArchetypes)
            {
                backs.Add(Create(pa));
            }
            return backs.ToArray();
        }

        public static ProfileCardBack Create(ProfileArchetype pa)
        {
            return new ProfileCardBack(pa.Name, pa.Whatis, pa.Tags, pa.Examples);
        }
    }

}

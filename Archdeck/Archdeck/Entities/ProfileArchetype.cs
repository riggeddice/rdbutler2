﻿using Commons.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace Archdeck.Entities
{
    public class ProfileArchetype
    {
        public ProfileArchetype(string name, string actions, string hunger, string howActs, 
            string resources, string whatis, string tags, string examples)
        {
            this.Name = name;
            this.Actions = actions;
            this.Hunger = hunger;
            this.HowActs = howActs;
            this.Resources = resources;
            this.Whatis = whatis;
            this.Tags = tags;
            this.Examples = examples;
        }

        public string Name { get; }
        public string Actions { get; }
        public string Hunger { get; }
        public string HowActs { get; }
        public string Resources { get; }
        public string Whatis { get; }
        public string Tags { get; }
        public string Examples { get; }

        public override string ToString()
        {
            return $"Name: {Name}";
        }

        public override int GetHashCode() => HashCode.Combine(
            Name, Actions, Hunger, HowActs, Resources,
            Whatis, Tags, Examples);

        public override bool Equals(object obj)
        {
            if (!(obj is ProfileArchetype other)) return false;

            if (Name != other.Name) return false;
            if (Actions != other.Actions) return false;
            if (Hunger != other.Hunger) return false;
            if (HowActs != other.HowActs) return false;
            if (Resources != other.Resources) return false;
            if (Whatis != other.Whatis) return false;
            if (Tags != other.Tags) return false;
            if (Examples != other.Examples) return false;
            return true;
        }
    }


    public static class ProfileArchetypeFactory
    {
        public static ProfileArchetype[] Create(FileRecord[] fileRecords)
        {
            List<ProfileArchetype> archetypes = new List<ProfileArchetype>();
            foreach (FileRecord record in fileRecords)
            {
                archetypes.Add(Create(record));
            }
            return archetypes.ToArray();
        }

        public static ProfileArchetype Create(FileRecord fileRecord)
        {
            var name = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractParamFromFrontMatter("title"), fileRecord.Content);
            var actions = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Co robi? (3)"), fileRecord.Content);
            var hunger = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Czego chce? (3)"), fileRecord.Content);
            var howActs = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Jak działa? (3)"), fileRecord.Content);
            var resources = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Zasoby i otoczenie (3)"), fileRecord.Content);
            var whatis = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Co to za archetyp"), fileRecord.Content);
            var tags = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Tagi i przykłady"), fileRecord.Content);
            var examples = TextParserEngine.SafelyExtractSingleCapturedElement(RegexFacade.ExtractHeaderSection(3, 3, "Przykładowe działania"), fileRecord.Content);

            return new ProfileArchetype(name, actions, hunger, howActs, resources, whatis, tags, examples);
        }
    }

}

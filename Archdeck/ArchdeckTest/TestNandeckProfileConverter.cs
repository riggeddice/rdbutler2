using Archdeck.Actions;
using Archdeck.Entities;
using Commons.Tools;
using NUnit.Framework;

namespace ArchdeckTests
{
    public class TestNandeckProfileConverter
    {
        [Test]
        public void TestHappyPathForSingleProfileFront()
        {
            // Expected
            string expected = @"""Inspirator"",""- zauracza i porywa za sobą tłum\13\- kontroluje uwagę i emocje\13\- manipuluje urokiem i aktorstwem"",""TAK: łączyć ludzi wokół Sprawy\13\TAK: dawać siłę i nadzieję\13\TAK: długoterminowych efektów\13\NIE: śmieszność i utrata twarzy\13\NIE: działać samotnie, bez wsparcia"",""TAK: przekonywanie i wpływ\13\TAK: naturalnie przejmuje dowodzenie\13\TAK: dziełami sztuki czy osobiście\13\NIE: działanie w ukryciu, dyskretne\13\NIE: działanie wbrew swej Sprawie"",""- akcesoria do występów\13\- grupa głęboko oddanych osób\13\- sprawa, której jest oddany\13\- reputacja i pozycja; wolno więcej""";

            // Given
            FileRecord record = FileOps.ReadFile(@"TestData/1911-inspirator.md");
            ProfileArchetype inspirator = ProfileArchetypeFactory.Create(record);
            ProfileCardFront front = ProfileCardFrontFactory.Create(inspirator);

            // When
            var actual = NandeckProfileConverter.Convert(front);

            // Then
            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
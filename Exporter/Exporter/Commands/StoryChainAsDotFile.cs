﻿using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using Exporter.CommandSupport.StoryChainAsDotFile;
using Exporter.Infrastructure;

using System.Threading.Tasks;

namespace Exporter.Commands
{
    public class StoryChainAsDotFile
    {
        public async Task Execute(IRepository repository, IOutputFileSaver outputFileSaver)
        {
            // 1. Get StoryInfos - all relevant info should be located there
            AggregatedStory[] stories = await repository.GetAllAggregatedStories();

            // 2. Construct a Graphviz string
            string graphviz = new StoryInfosToGraphviz().Convert(stories);

            // 3. Save to HDD
            outputFileSaver.PersistStoryChain(graphviz);

            // 4. -> this is manual from cmdline. Convert the output to svg.
            // dot storyChain.graphviz -Ooutput -Tsvg
        }
    }
}

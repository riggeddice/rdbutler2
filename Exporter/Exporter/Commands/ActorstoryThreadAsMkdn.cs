﻿using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using Exporter.CommandSupport.ThresholdActorstoryAsMkdn;
using Exporter.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.Commands
{
    public class ActorstoryThreadAsMkdn
    {
        public async Task Execute(IRepository repository, IOutputFileSaver outputSaver)
        {
            // 1. Get the Actors and subset them to only those Actors we want

            ActorBriefRecord[] actorBriefs = await repository.GetActorBriefs(new Dictionary<string, string>());
            string[] actorUids = actorBriefs.Select(a => a.Link).ToArray();
            AggregatedActor[] aggregatedActors = await repository.GetActorsByUids(actorUids);

            // 2. Having all Actors we need to select only those who were present in more Stories than the threshold.

            int threshold = 5;
            AggregatedActor[] targetActors = aggregatedActors.Where(a => a.Merits.Length >= threshold).ToArray();

            // 3. Get all the StoryInfos, LocationRecords and Merits - we will need a lot of information from those

            AggregatedStory[] allAggregatedStories = await repository.GetAllAggregatedStories();
            LocationRecord[] allLocationRecords = await repository.GetLocationRecords();
            AggregatedMerit[] allMerits = await repository.GetAllAggregatedMerits();

            // 4. Having inputs, construct actorstories.

            (string, string)[] actorstoryMkdns = new ActorstoryFromInputs().ConvertMany(
                targetActors, allAggregatedStories, allLocationRecords, allMerits);

            // 5. Save the files on HDD in a path taken from the configuration.
            outputSaver.PersistActorstoryMkdns(actorstoryMkdns);
        }
    }
}

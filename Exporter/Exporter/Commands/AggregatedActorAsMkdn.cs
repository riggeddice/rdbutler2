﻿using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using Exporter.CommandSupport.AggregatedActorAsMkdn;
using Exporter.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.Commands
{
    public class AggregatedActorAsMkdn
    {
        /// <summary>
        /// I have two possible approaches to do it. The one simpler in terms of an algorithm (but more memory intensive) is to do it in batch.
        /// That is, read ALL stuff and operate on ALL stuff. Obviously, this will fail when the amount of data exceeds the critical number, 
        /// but that is a problem for another day.
        /// </summary>
        public async Task Execute(IRepository repository, IOutputFileSaver outputSaver)
        {
            // 1. Get AggregatedActorDto[] : this represents all the info we have on an Actor. Both from ActorSheet and from autogeneration.

            ActorBriefRecord[] actorBriefs = await repository.GetActorBriefs(new Dictionary<string, string>());
            string[] actorUids = actorBriefs.Select(a => a.Link).ToArray();
            AggregatedActor[] aggregatedActors = await repository.GetActorsByUids(actorUids);

            // 2. AggregatedActorDto[] --> MarkdownString[] where each single string represents one AggregatedActorDto
            (string, string)[] aggregatedActorUidsMkdns = aggregatedActors.Select(
                aActor => AggregatedActorToMarkdownConverter.Convert(aActor)).ToArray();

            // 3. Save the files on HDD in a path taken from the configuration.
            outputSaver.PersistAggregatedActorMkdns(aggregatedActorUidsMkdns);
        }
    }
}

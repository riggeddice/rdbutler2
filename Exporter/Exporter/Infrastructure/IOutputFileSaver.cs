﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.Infrastructure
{
    public interface IOutputFileSaver
    {
        void PersistActorstoryMkdns((string, string)[] actorstoryUidMkdns);
        void PersistAggregatedActorMkdns((string, string)[] aggregatedActorUidsMkdns);
        void PersistWorldMapGraphviz(string graphviz);
        void PersistStoryChain(string graphviz);
        void PersistWorldMapMkdn(string markdown);

    }
}

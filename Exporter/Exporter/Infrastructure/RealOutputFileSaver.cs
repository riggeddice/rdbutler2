﻿using Commons.Config;
using Commons.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.Infrastructure
{
    public class RealOutputFileSaver : IOutputFileSaver
    {

        public void PersistActorstoryMkdns((string, string)[] actorstoryUidMkdns)
        {
            Persist_IdContent_Pairs(actorstoryUidMkdns, 
                GetDirPathAndCreateDirIfNeeded(new AppConfigProvider().GetPathToThreadOutputFolder()));
        }

        public void PersistAggregatedActorMkdns((string, string)[] aggregatedActorUidMkdns)
        {
            Persist_IdContent_Pairs(aggregatedActorUidMkdns, 
                GetDirPathAndCreateDirIfNeeded(new AppConfigProvider().GetPathToActorOutputFolder()));
        }


        public void PersistWorldMapMkdn(string markdown)
        {
            string directory = GetGeneralOutputDirPathAndCreateDir();
            SaveFile(markdown, directory + "/" + "worldMap.md");
        }

        public void PersistWorldMapGraphviz(string graphviz)
        {
            string directory = GetGeneralOutputDirPathAndCreateDir();
            SaveFile(graphviz, directory + "/" + "worldMap.graphviz");
        }

        public void PersistStoryChain(string graphviz)
        {
            string directory = GetGeneralOutputDirPathAndCreateDir();
            SaveFile(graphviz, directory + "/" + "storyChain.graphviz");
        }

        private string GetGeneralOutputDirPathAndCreateDir()
        {
            string directory = new AppConfigProvider().GetPathToGeneralOutputFolder();
            Directory.CreateDirectory(directory);   // if does exist, this line effectively does nothing
            return directory;
        }

        // PRIVATE:

        private string GetDirPathAndCreateDirIfNeeded(string directory)
        {
            Directory.CreateDirectory(directory);   // if does exist, this line effectively does nothing
            return directory;
        }

        private void SaveFile(string content, string filePath)
        {
            FileOps.SaveFile(content, filePath);
        }

        private void Persist_IdContent_Pairs((string, string)[] idContentPairs, string directory)
        {
            foreach (var idContentPair in idContentPairs)
            {
                string path = directory + "/" + idContentPair.Item1 + ".md";
                string content = idContentPair.Item2;

                SaveFile(content, path);
            }
        }
    }
}

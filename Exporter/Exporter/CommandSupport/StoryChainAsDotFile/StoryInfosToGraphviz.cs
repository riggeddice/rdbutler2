﻿using Commons.CoreEntities;
using Commons.Tools;
using csdot;
using csdot.Attributes.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.StoryChainAsDotFile
{
    public class StoryInfosToGraphviz
    {
        public string Convert(AggregatedStory[] stories)
        {
            // Overall comment: all the submethods in this Convert(...) method could have been public.
            // Then I could create proper, simple tests instead of scary E2E ones.
            // We live, we learn, right? ;-)

            Graph graph = CreateDirectedLRGraph();
            List<StoryInfoGraphvizNode> nodes = CreateProperlyLabeledNodes(stories);
            List<Edge> edges = CreateEdges(nodes);

            // Add the components to the graph
            graph.AddElements(nodes.ToArray());
            graph.AddElements(edges.ToArray());

            return ChangeNodeLabel.TextTypeToHtmlType(graph.ElementToString());
        }

        private static List<Edge> CreateEdges(List<StoryInfoGraphvizNode> nodes)
        {
            List<Edge> edges = new();
            foreach (var node in nodes)
            {
                // We can have more than one Story in the past, but every Story can exist only once in the node list.
                // Also, we do not want to add self to previous node list.
                List<StoryInfoGraphvizNode> previousNodes = new();
                foreach (string previousStory in node.PreviousStoryIds)
                {
                    var previousNode = nodes.Where(n => n.StoryUid == previousStory).FirstOrDefault();
                    if (previousNode != null && previousNode.StoryUid != node.StoryUid)
                        previousNodes.Add(previousNode);
                }

                // Having the list of previous Stories, we can now draw an Edge between the current one and the previous one.
                foreach (var previous in previousNodes)
                {
                    Edge edge = new Edge();
                    Transition t1 = new Transition(previous, EdgeOp.directed);
                    Transition t2 = new Transition(node, EdgeOp.unspecified);
                    edge.Transition = new List<Transition> { t1, t2 };
                    edges.Add(edge);
                }
            }

            return edges;
        }

        private static List<StoryInfoGraphvizNode> CreateProperlyLabeledNodes(AggregatedStory[] stories)
        {
            // HTML-styled rect nodes. But the library does not give me HTML features, so we will do a Sick Regex Hack later.

            List<StoryInfoGraphvizNode> nodes = new();
            foreach (var info in stories)
            {
                StoryInfoGraphvizNode node = new(info.StoryUid.ToGraphvizIdFormat());
                node.Attribute.shape.Value = "rect";
                node.Attribute.label.Value = StoryInfoNodeLabelMaker.MakeFrom(info);
                node.StoryUid = info.StoryUid;
                node.PreviousStoryIds = info.PreviousStories;

                nodes.Add(node);
            }

            return nodes;
        }

        private static Graph CreateDirectedLRGraph()
        {
            // Graph parameters - directed graph with LR approach.

            Graph graph = new Graph("story_chain");
            graph.type = "digraph";
            graph.Attribute.rankdir.Value = Rankdir.LR;
            return graph;
        }
    }
}

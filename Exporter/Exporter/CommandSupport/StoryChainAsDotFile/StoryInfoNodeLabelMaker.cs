﻿using Commons.CoreEntities;
using Commons.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.StoryChainAsDotFile
{
    public static class StoryInfoNodeLabelMaker
    {
        public static string MakeFrom(AggregatedStory story)
        {
            string gms = string.Join(", ", story.Gms);
            string players = string.Join(", ", story.Players);
            string actors = string.Join(", ", story.PlayerActors);
            
            string uidPart = SplitTo80(("id: " + story.StoryUid));
            string peoplePart = SplitTo80(($"gm: {gms} | players: {players}"));
            string actorsPart = SplitTo80(($"actors: {actors}"));
            string summary = SplitTo80(story.Summary);

            // Here, apply general formatting:
            string result = $"<b><font point-size='20'>{uidPart}</font></b>\\n\\n" +
                $"{peoplePart}\\n" +
                $"{actorsPart}\\n\\n" +
                $"{summary}\\n\\n" +
                $"time: {story.StartDate} - {story.EndDate}";

            return result.EscapeToGraphvizHtml();
        }

        /// <summary>
        /// This method takes the string and splits it to as-close-as-80-characters as possible, searching for the
        /// last space before the mark of 80 characters.
        /// </summary>
        public static string SplitTo80(string toSplit)
        {
            int howManySplits = toSplit.Length / 80;

            if (howManySplits == 0)
                return toSplit;

            List<int> splitIndexes = new List<int>();
            for(int i = 1; i< howManySplits+1; i++)
            {
                int splitIndex = toSplit.LastIndexOfAny(new char[] { ' ', '\n' }, i * 80-1);
                splitIndexes.Add(splitIndex);
            }

            var cloned = new string(toSplit).ToCharArray();
            foreach(int index in splitIndexes)
            {
                cloned[index] = '\n';
            }

            return new string(cloned);
        }
    }
}

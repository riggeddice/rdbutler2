﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.StoryChainAsDotFile
{
    public static class ChangeNodeLabel
    {
        /// <summary>
        /// In short: change all quotations in a label to angular brackets to enable html formatting.
        /// Graphviz uses label = "xxx" to denote TEXT label. But if I use label = <xxx> then I
        /// enable HTML labels - so I can use things like <<b>cats rule</b> everything>, which would
        /// not be possible in textual form. But csdot library does not allow me to specify html label.
        /// So what I do - I hack the output of csdot using regular expressions. Of course.
        /// </summary>
        public static string TextTypeToHtmlType(string graph)
        {
            Regex regex = new Regex(@"label = ""(.+?[^""^\\])""");
            return regex.Replace(graph, @"label = <$1>");
        }
    }
}

﻿using csdot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.StoryChainAsDotFile
{
    public class StoryInfoGraphvizNode : Node
    {
        public StoryInfoGraphvizNode(string i_id) : base(i_id) { }

        public string StoryUid { get; set; }
        public string[] PreviousStoryIds { get; set; }

    }
}

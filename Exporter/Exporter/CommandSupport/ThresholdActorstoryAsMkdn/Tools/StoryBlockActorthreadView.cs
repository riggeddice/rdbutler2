﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.ThresholdActorstoryAsMkdn.LocationComponent
{
    public class StoryBlockActorthreadView
    {
        public StoryBlockActorthreadView(string storyUid, string storyTitle, string startDate, string endDate, string summary, 
            string actorDeed, string[] actorProgressions, string[] actorsPresent)
        {
            StoryUid = storyUid;
            StoryTitle = storyTitle;
            StartDate = startDate;
            EndDate = endDate;
            Summary = summary;
            TargetActorDeed = actorDeed;
            TargetActorProgressions = actorProgressions;
            AllActorsPresent = actorsPresent;
        }

        public string StoryUid { get; init; }
        public string StoryTitle { get; init; }
        public string StartDate { get; init; }
        public string EndDate { get; init; }
        public string Summary { get; init; }
        public string TargetActorDeed { get; init; }
        public string[] TargetActorProgressions { get; init; }
        public string[] AllActorsPresent { get; init; }

        public override int GetHashCode() => HashCode.Combine(StoryUid, StoryTitle, StartDate, EndDate, Summary, TargetActorDeed);
        public override bool Equals(object obj)
        {
            if (obj is not StoryBlockActorthreadView other) return false;

            if (StoryUid != other.StoryUid) return false;
            if (StoryTitle != other.StoryTitle) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;
            if (Summary != other.Summary) return false;
            if (TargetActorDeed != other.TargetActorDeed) return false;
            if (Enumerable.SequenceEqual(TargetActorProgressions, other.TargetActorProgressions) == false) return false;
            if (Enumerable.SequenceEqual(AllActorsPresent, other.AllActorsPresent) == false) return false;

            return true;
        }

        public override string ToString()
        {
            return $"{StoryUid} - {StoryTitle} @ {StartDate} - {EndDate} with Deed: {TargetActorDeed} " +
                $"having {TargetActorProgressions.Length} Progressions and {AllActorsPresent.Length} Actors present.";
        }
    }

    public static class StoryBlockActorthreadViewFactory
    {
        public static StoryBlockActorthreadView[] CreateViewForActor(AggregatedActor actor, AggregatedStory[] relevantStories, AggregatedMerit[] allMerits)
        {
            List<StoryBlockActorthreadView> blocks = new ();
            foreach(var storyInfo in relevantStories)
            {
                blocks.Add(CreateSingleStoryBlockForActorView(storyInfo, actor, allMerits));
            }

            return blocks.OrderByDescending(b => b.EndDate).ToArray();
        }

        public static StoryBlockActorthreadView CreateSingleStoryBlockForActorView(AggregatedStory story, AggregatedActor actor, AggregatedMerit[] allMerits)
        {
            string storyUid = story.StoryUid;
            string storyTitle = story.StoryTitle;
            string startDate = story.StartDate;
            string endDate = story.EndDate;
            string summary = story.Summary;
            
            string actorDeed;
            AggregatedMerit[] merits = actor.Merits.Where(m => m.OriginUid == storyUid).ToArray();
            if (merits.Length == 0) actorDeed = "AKTOR NIEOBECNY NA SESJI";
            else actorDeed = merits.Select(m => m.Deed).FirstOrDefault();

            string[] actorProgressions = actor.Progressions.Where(p => p.OriginUid == storyUid).Select(p => p.Deed).ToArray();
            string[] actorsPresent = allMerits.Where(m => m.OriginUid == storyUid).Select(m => m.Actor).OrderBy(a => a).ToArray();

            return new StoryBlockActorthreadView(
                storyUid: storyUid,
                storyTitle: storyTitle,
                startDate: startDate,
                endDate: endDate,
                summary: summary,
                actorDeed: actorDeed,
                actorProgressions: actorProgressions,
                actorsPresent: actorsPresent
                );
        }
    }
}

﻿using Commons.CoreEntities;
using Exporter.CommandSupport.ThresholdActorstoryAsMkdn.LocationComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.ThresholdActorstoryAsMkdn.Tools
{
    public static class ActorstoryMkdnConverter
    {
        public static string Convert(string actorUid, string actorName, 
            StoryBlockActorthreadView[] storyBlocks, AtarRecord[] atars, LocationActorPresence[] visitedLocationActorPresences)
        {

            // 2. Construct Primary section: name, potentially threads

            string markdown = $"# {actorName}\n## Identyfikator\n\nId: {actorUid}\n\n";

            // 3. Construct Story section: (story - storyuid, start - end dates, story summary,
            //    THIS actor deed - progression - plans, other actors met there)
            markdown += ConstructStorySection(storyBlocks);

            // 4. Construct Worldliness section: AtARM and visited WorldMap
            markdown += ConstructWordlinessSection(atars, visitedLocationActorPresences);

            return markdown;
        }

        private static string ConstructWordlinessSection(AtarRecord[] atars, LocationActorPresence[] actorPresences)
        {
            string markdown = "## Sekcja Światowości\n\n";
            
            // WorldMap
            markdown += "### Zwiedzony świat\n\n";
            markdown += "1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)\n\n";
            markdown += CreateVisitedWorldMapSection(actorPresences);

            // Atar
            markdown += "### Relacje Aktor - Aktor\n\n";
            markdown += "| Z kim | Intensywność | Opowieści |\n";
            markdown += "| ---- | ---- | ---- |\n";
            markdown += CreateAtarSection(atars);


            return markdown;
        }

        private static string CreateVisitedWorldMapSection(LocationActorPresence[] locationActorPresence)
        {
            var sortedLocationRecords = locationActorPresence.OrderBy(a => a.Path).ToArray();

            List<string> presences = new();
            foreach (var record in sortedLocationRecords)
            {
                string displayedLocationName = "1. " + record.Name;

                int padding = 4 * (record.Path.Where(c => c == '|').Count() - 1);
                
                string padded = displayedLocationName.PadLeft(padding + displayedLocationName.Length, ' ');
                padded += $"    : {record.Intensity}, @: {record.LastVisitedDate}";

                presences.Add(padded);
            }

            return string.Join(Environment.NewLine, presences) + "\n\n";
        }

        private static string CreateAtarSection(AtarRecord[] actorToActorRelations)
        {
            if (actorToActorRelations.Length == 0) return string.Empty;

            var sorted = actorToActorRelations.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor);

            string[] atarStrings = sorted.Select(r =>
                $"| {r.RelevantActor,-20} | {r.Intensity} | (({string.Join("; ", r.StoryUids)})) |"
            ).ToArray();

            return string.Join("\n", atarStrings);
        }


        private static string ConstructStorySection(StoryBlockActorthreadView[] storyBlocks)
        {
            string markdown = "## Sekcja Opowieści\n\n";

            var sortedBlocks = storyBlocks.OrderByDescending(s => s.EndDate).ToArray();
            int storyCounter = sortedBlocks.Length;

            foreach (StoryBlockActorthreadView storyBlock in sortedBlocks)
            {
                markdown += $"### {storyBlock.StoryTitle}\n\n";
                
                markdown += $"* **uid:** {storyBlock.StoryUid}, _numer względny_: {storyCounter}\n";
                markdown += $"* **daty:** {storyBlock.StartDate} - {storyBlock.EndDate}\n";
                markdown += $"* **obecni:** " + string.Join(", ", storyBlock.AllActorsPresent) + "\n";

                markdown += "\n";
                
                markdown += $"Streszczenie:\n\n{storyBlock.Summary}\n\n";
                
                markdown += $"Aktor w Opowieści:\n";
                markdown += "\n";

                markdown += $"* Dokonanie:\n    * {storyBlock.TargetActorDeed}\n";
                if (storyBlock.TargetActorProgressions.Length == 0) { }
                else if (storyBlock.TargetActorProgressions.Length == 1)
                {
                    markdown += $"* Progresja:\n    * {storyBlock.TargetActorProgressions[0]}\n";
                }
                else
                {
                    markdown += $"* Progresja:\n";
                    foreach (string progression in storyBlock.TargetActorProgressions)
                    {
                        markdown += $"    * {progression}\n";
                    }
                }
                
                markdown += "\n\n";

                storyCounter--;
            }

            return markdown;
        }
    }
}

﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.ThresholdActorstoryAsMkdn.Tools
{
    public static class StoryUidSelector
    {
        public static string[] SelectAllRelevantToActor(AggregatedMerit[] actorMerits, AcdeorPlus[] actorProgressions)
        {
            string[] meritStoryUids = actorMerits.Select(s => s.OriginUid).ToArray();
            string[] progressionStoryUids = actorProgressions.Select(s => s.OriginUid).ToArray();
            string[] storyUids = meritStoryUids.Concat(progressionStoryUids).Distinct().ToArray();

            return storyUids;
        }
    }
}

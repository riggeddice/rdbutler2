﻿using Commons.CoreEntities;
using Exporter.CommandSupport.ThresholdActorstoryAsMkdn.LocationComponent;
using Exporter.CommandSupport.ThresholdActorstoryAsMkdn.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.ThresholdActorstoryAsMkdn
{
    public class ActorstoryFromInputs
    {
        /// <returns>pairs: (string id, string content)[]</returns>
        public (string, string)[] ConvertMany(AggregatedActor[] targetActors, 
            AggregatedStory[] allStories, 
            LocationRecord[] allLocationRecords,
            AggregatedMerit[] allMerits)
        {
            List<(string, string)> idActorMkdns = new ();
            foreach(var actor in targetActors)
            {
                idActorMkdns.Add(ConvertSingle(actor, allStories, allLocationRecords, allMerits));
            }
            
            return idActorMkdns.ToArray();
        }

        /// <returns>pairs: (string id, string content)</returns>
        public (string, string) ConvertSingle(AggregatedActor actor,
            AggregatedStory[] allStories, 
            LocationRecord[] allLocationRecords,
            AggregatedMerit[] allMerits)
        {
            // 1. Filter only relevant storyInfos (extracting and unique-ing the crossection between Merits and Progression) and locationRecords
            string[] allRelevantStoryUids = StoryUidSelector.SelectAllRelevantToActor(actor.Merits, actor.Progressions);
            AggregatedStory[] relevantStories = allStories.Where(s => allRelevantStoryUids.Contains(s.StoryUid)).ToArray();

            string[] storyUidsRelevantToLocations = actor.Merits.Select(s => s.OriginUid).ToArray();
            LocationRecord[] relevantLocationRecords = 
                LocationRecordDtoOps.SelectRecordsFilteringByStoryUids(storyUidsRelevantToLocations, allLocationRecords);

            // 2. Construct basics: name (actor), (threads (storyinfo) OPTIONAL)
            string actorUid = actor.Uid;
            string actorName = actor.Name;

            // 3. Construct BLOCK FOR EACH STORY (story - storyuid (storyinfo), start - end dates (storyinfo), story summary (storyinfo),
            //    THIS actor deed - progression - plans (actor), other actors met there (storyinfo))
            StoryBlockActorthreadView[] storyBlocks = 
                StoryBlockActorthreadViewFactory.CreateViewForActor(actor, relevantStories, allMerits);

            // 4. Get BLOCK AtARM (actor) and visited WorldMap (locationActorPresences)

            AtarRecord[] atars = actor.ActorToActorRelations;
            LocationActorPresence[] visitedLocationActorPresences = 
                LocationActorPresenceFactory.CreateVisitedWorldMap(relevantLocationRecords);

            // 5. Format everything -> mkdn
            string mkdn = ActorstoryMkdnConverter.Convert(actorUid, actorName, storyBlocks, atars, visitedLocationActorPresences);

            // 6. return mkdn
            return (actorUid, mkdn);
        }
    }
}

﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exporter.CommandSupport.WorldMapAsMkdn
{
    public class LocationRecordsToMkdnMap
    {
        public string Convert(LocationRecord[] records)
        {
            // In order to properly render the map, I need to sort the LocationRecords by path so they are in proper sequence.
            // Reasoning: shape of LocationRecord.Path: Świat|Primus|Sektor Astoriański|Astoria|Sojusz Letejski, SW|Granica Anomalii|.
            // So every record will be next to its parent record.
            LocationRecord[] sorted = records.OrderBy(r => r.Path).ToArray();

            // Having that we can extract all important information - name and depth.
            List<string> names = new();
            foreach (var record in sorted)
            {
                string displayedLocationName = "1. " + record.Name;
                string path = MakeSurePathFinishesWithPipe(record);

                // Therefore we know that the last symbol in path will always be the pipe ("|") symbol: so we subtract 1 from total pipe count.
                int padding = 4 * (path.Where(c => c == '|').Count() - 1);

                string padded = displayedLocationName.PadLeft(padding + displayedLocationName.Length, ' ');
                names.Add(padded);
            }

            return string.Join(Environment.NewLine, names);
        }

        private static string MakeSurePathFinishesWithPipe(LocationRecord record)
        {
            // Typical record.Path example: Świat|Primus|Sektor Astoriański|Astoria|Sojusz Letejski, SW|Granica Anomalii|
            // The last symbol should be the pipe symbol ("|"). In case it is not, we will force it to be one for further calculations:
            if (record.Path.Last() != '|')
                return record.Path + "|";
            else
                return record.Path;
        }
    }
}
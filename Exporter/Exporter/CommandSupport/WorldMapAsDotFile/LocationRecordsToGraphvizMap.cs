﻿using Commons.CoreEntities;
using Commons.Tools;
using csdot;
using csdot.Attributes.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.WorldMapAsDotFile
{
    public class LocationRecordsToGraphvizMap
    {
        public string Convert(LocationRecord[] recordsWithDistinctPath, LocationRecord[] allRecords)
        {
            Graph graph = CreateUndirectedLRGraph();
            LocationGraphvizNode[] uniqueNodes = CreatePrettyProperlyLabeledNodes(recordsWithDistinctPath, allRecords);
            List<Edge> edges = CreateEdgesProperlyLinkingNodesWithParents(uniqueNodes);

            // Add the components to the graph
            graph.AddElements(uniqueNodes.ToArray());
            graph.AddElements(edges.ToArray());

            return graph.ElementToString();
        }

        private static List<Edge> CreateEdgesProperlyLinkingNodesWithParents(LocationGraphvizNode[] uniqueNodes)
        {
            // Now time for transitions and edges - find the PARENT and link to it.
            List<Edge> edges = new();
            foreach (var node in uniqueNodes)
            {
                if (node.Path != "Świat|")
                {
                    string parentPath = node.Path.Substring(0, node.Path.TrimEnd('|').LastIndexOf('|') + 1);
                    LocationGraphvizNode parent = uniqueNodes.Where(n => n.Path == parentPath).First();

                    Edge edge = new Edge();

                    Transition t1 = new Transition(parent, EdgeOp.undirected);
                    Transition t2 = new Transition(node, EdgeOp.unspecified);

                    edge.Transition = new List<Transition> { t1, t2 };
                    edges.Add(edge);
                }
            }

            return edges;
        }

        private static LocationGraphvizNode[] CreatePrettyProperlyLabeledNodes(LocationRecord[] recordsWithDistinctPath, LocationRecord[] allRecords)
        {
            List<LocationGraphvizNode> nodes = new();
            foreach (var record in recordsWithDistinctPath)
            {
                LocationGraphvizNode node = new(record.Path.ToGraphvizIdFormat());
                node.Attribute.label.Value = record.Name;
                node.Path = record.Path;

                string[] relatedStories = allRecords.Where(r => r.Path == node.Path).Select(r => r.OriginatingStory).ToArray();
                node.Stories = relatedStories;

                nodes.Add(node);
            }

            // Prepare nodes for display
            LocationGraphvizNode[] uniqueNodes = nodes
                .GroupBy(n => n.Path)
                .Select(n => n.First())
                .Distinct()
                .Select(n => LocationNodeAugmenter.AddIntensityAsColorScheme(n))
                .Select(n => LocationNodeAugmenter.AddStoryListAsTooltip(n))
                .ToArray();
            return uniqueNodes;
        }

        private static Graph CreateUndirectedLRGraph()
        {
            Graph graph = new Graph("world_map");
            graph.type = "graph";
            graph.Attribute.rankdir.Value = Rankdir.LR;
            return graph;
        }
    }
}

﻿using csdot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.WorldMapAsDotFile
{
    /// <summary>
    /// This inheritance is the best solution to find to which node I should connect parent LocationRecord.
    /// Reasoning: I want all the Node behaviors and methods and I need one more field - Path (so Parent Location).
    /// Inheritance is the superior option here over composition or design patterns.
    /// </summary>
    public class LocationGraphvizNode : Node
    {
        public LocationGraphvizNode(string i_id) : base(i_id) { }

        /// <summary>
        /// The Path - unique identification of this node. "World|XXX|YYY|This|"
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The id of all the Stories where the representing Node is located. 
        /// </summary>
        public string[] Stories { get; set; }
    }
}

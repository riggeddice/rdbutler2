using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using Exporter.Commands;
using Exporter.Infrastructure;
using ExporterTest._FakeData;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExporterTest
{
    public class TestOutputAggregatedActorAsMkdn
    {
        IRepository _emptyRepo;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var emptyRepositoryMock = new Mock<IRepository>();
            emptyRepositoryMock.Setup(_ => _.GetActorBriefs(new Dictionary<string, string>())).Returns(Task.FromResult(Array.Empty<ActorBriefRecord>()));
            _emptyRepo = emptyRepositoryMock.Object;

            var singleActorRepositoryMock = new Mock<IRepository>();
            singleActorRepositoryMock.Setup(_ => _.GetActorBriefs(new Dictionary<string, string>())).Returns(
                Task.FromResult(new[] { FakeDataFactory.SingleActor_MartynHiwasser_id_001__ActorBrief() })
                );
            singleActorRepositoryMock.Setup(_ => _.GetActorByUid(It.IsAny<string>())).Returns(
                Task.FromResult(FakeDataFactory.SingleActor_MartynHiwasser_id_001__AggregatedActor()));
        }

        [Test]
        public async Task For_empty_repository_application_does_not_crash_and_saves_nothing()
        {
            // Given
            IRepository repository = _emptyRepo;

            var fileSaverMock = new Mock<IOutputFileSaver>();
            IOutputFileSaver fileSaver = fileSaverMock.Object;

            // When
            await new AggregatedActorAsMkdn().Execute(repository, fileSaver);

            // Then
            fileSaverMock.Verify(f => f.PersistAggregatedActorMkdns(Array.Empty<(string, string)>()), Times.Once);
        }
    }
}